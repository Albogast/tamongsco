<?php
namespace App\Imports;
use App\Models\EventAttendee;
use Auth;
use App\Models\Events;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class MemberImport implements ToModel, WithHeadingRow
{
    /**
     * @param array $row
     * */
    public function model(array $row)
    {
      //  dd($row);
        return new EventAttendee([
            'name'  => $row['name'],
            'phone' => !empty($row['phone']) ? validate_phone_number($row['phone'])[1] : '0',
            'school_id' => $row['school'],
            'title' => $row['title'],
            'email' => $row['email'],
            'event_id' => Events::orderBy('id', 'DESC')->first()->id,
            'user_id' => Auth::User()->id
        ]);
    }
}

