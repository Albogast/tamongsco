<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PortalPart extends Model {

    /**
     * Generated
     */
    protected $table = 'portal_parts';
    protected $fillable = ['id', 'name', 'status', 'created_at', 'updated_at'];
    
        public function subparts() {
            return $this->hasMany(\App\Models\SupPart::class, 'part_id', 'id');
        }

}
