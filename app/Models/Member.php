<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Member extends Model {

	protected $table = 'admin.tamongsco_contacts';

    /**
     * The attributes that are mass assignable.
    */
    protected $fillable = [
        'name',
        'email',
        'phone',
        'school_id',
        'user_id',
        'schools',
        'title',
        'created_at',
        'updated_at',
    ];
 
    /**
     * The attributes that should be cast to native types.
     */
    protected $casts = [
        'created_at' => 'datetime',
    ];

    public function users() {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }

    public function school() {
        return $this->belongsTo(\App\Models\School::class, 'school_id', 'id')->withDefault(['name' => 'Not Defined']);
    }

}
