<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentType extends Model {

    /**
     * Generated
     */

    protected $table = 'constant.payment_types';
    protected $fillable = ['id', 'name'];



}
