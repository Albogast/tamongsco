<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;
	protected $table = 'admin.tamongsco_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'phone',
        'address',
        'role_id',
        'usertype',
        'national_id',
        'photo',
        'status',
        'created_by',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function users() {
        return $this->hasMany(\App\Models\User::class, 'user_id', 'id');
    }

    public function role() {
        return $this->belongsTo(\App\Models\Role::class, 'role_id', 'id')->withDefault(['name' => 'Not Defined']);
    }

    public function country() {
        return $this->belongsTo(\App\Models\Country::class, 'national_id', 'id')->withDefault(['name' => 'Not Defined']);
    }
}
