<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Task
 */
class Sms extends Model {
    
        /**
         * Generated
         */

        protected $table = 'admin.sms';
        protected $primaryKey = 'sms_id';
        protected $fillable = ['sms_id', 'body', 'user_id', 'status', 'return_code', 'phone_number', 'created_at', 'sent_from', 'type', 'table', 'priority', 'opened'];
    
    public function member() {
        return $this->belongsTo(\App\Models\Member::class, 'user_id', 'id')->withDefault(['name' => 'Not allocated']);
    }

    public function user() {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }

}
