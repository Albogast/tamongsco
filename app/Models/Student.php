<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model {

    /**
     * Generated
     */
    protected $table = 'student';
    public $timestamps=false;
    protected $primaryKey = 'student_id';
    protected $fillable = ['student_id', 'name', 'dob', 'sex', 'email', 'phone', 'address', 'classesID', 'sectionID','roll', 'create_date','created_at', 'photo', 'year', 'username', 'password', 'usertype', 'parent_type', 'academic_year_id', 'status', 'health', 'health_other', 'religion_id', 'parent_type_id', 'health_condition_id', 'city_id', 'status_id', 'physical_condition_id', 'health_insurance_id', 'birth_certificate_number', 'distance_from_school', 'jod', 'joining_status', 'email_valid', 'remember', 'number', 'government_number','country_id','index','school_id','head_teacher_name','school_phone_number','lat','lng'];

    public function classes() {
        return $this->belongsTo(\App\Model\Classes::class, 'classesID', 'classesID');
    }

    public function city() {
        return $this->belongsTo(\App\Model\ReferCity::class, 'city_id', 'id')->withDefault(['city'=>'undefined']);
    }
    public function healthInsurance() {
        return $this->belongsTo(\App\Model\HealthInsurance::class, 'health_insurance_id', 'id');
    }

    public function physicalCondition() {
        return $this->belongsTo(\App\Model\PhysicalCondition::class, 'physical_condition_id', 'id');
    }

    public function section() {
        return $this->belongsTo(\App\Model\Section::class, 'sectionID', 'sectionID')->withDefault(['section'=>'Not allocated']);
    }

    public function academicYear() {
        return $this->belongsTo(\App\Model\AcademicYear::class, 'academic_year_id', 'id');
    }

    public function studentStatus() {
        return $this->belongsTo(\App\Model\StudentStatus::class, 'status_id', 'id')->withDefault(['reason'=>'Active']);
    }

    public function referReligion() {
        return $this->belongsTo(\App\Model\ReferReligion::class, 'religion_id', 'id')->withDefault(['religion'=>'unknown']);
    }

    public function referParentType() {
        return $this->belongsTo(\App\Model\ReferParentType::class, 'parent_type_id', 'id');
    }

    public function studentsApplications() {
        return $this->belongsToMany(\App\Model\Student::class, 'application', 'student_id', 'student_id');
    }

    public function feesInstallmentsDiscount() {
        return $this->belongsToMany(\App\Model\FeesInstallment::class, 'discount_fees_installments', 'student_id', 'fees_installment_id');
    }

    public function exams() {
        return $this->belongsToMany(\App\Model\Exam::class, 'exam_special_cases', 'student_id', 'exam_id');
    }

    public function teachers() {
        return $this->belongsToMany(\App\Model\Teacher::class, 'general_character_assessment', 'student_id', 'class_teacher_id');
    }

    public function hostels() {
        return $this->belongsToMany(\App\Model\Hostel::class, 'hmembers', 'student_id', 'hostel_id');
    }

    public function absentReasons() {
        return $this->belongsToMany(\App\Model\AbsentReason::class, 'sattendances', 'student_id', 'absent_reason_id');
    }

    public function feesInstallmentsUsubscription() {
        return $this->belongsToMany(\App\Model\FeesInstallment::class, 'student_fees_installments_unsubscriptions', 'student_id', 'fees_installment_id');
    }

    public function sponsors() {
        return $this->belongsToMany(\App\Model\Sponsor::class, 'student_sponsors', 'student_id', 'sponsor_id');
    }

    public function parents() {
        return $this->belongsToMany(\App\Model\Parents::class, 'student_parents', 'student_id', 'parent_id');
    }

    public function invoices() {
        return $this->hasMany(\App\Model\Invoice::class, 'student_id', 'student_id');
    }

    public function applications() {
        return $this->hasMany(\App\Model\Application::class, 'student_id', 'student_id');
    }

    public function advancePayments() {
        return $this->hasMany(\App\Model\AdvancePayment::class, 'student_id', 'student_id');
    }

    public function studentCharacters() {
        return $this->hasMany(\App\Model\StudentCharacter::class, 'student_id', 'student_id');
    }

    public function discountFeesInstallments() {
        return $this->hasMany(\App\Model\DiscountFeesInstallment::class, 'student_id', 'student_id');
    }

    public function subjectStudents() {
        return $this->hasMany(\App\Model\SubjectStudent::class, 'student_id', 'student_id');
    }

    public function dueAmounts() {
        return $this->hasMany(\App\Model\DueAmount::class, 'student_id', 'student_id');
    }

    public function studentArchives() {
        return $this->hasMany(\App\Model\StudentArchive::class, 'student_id', 'student_id');
    }

    public function examSpecialCases() {
        return $this->hasMany(\App\Model\ExamSpecialCase::class, 'student_id', 'student_id');
    }

    public function generalCharacterAssessments() {
        return $this->hasMany(\App\Model\GeneralCharacterAssessment::class, 'student_id', 'student_id');
    }

    public function hmembers() {
        return $this->hasMany(\App\Model\Hmember::class, 'student_id', 'student_id');
    }

    public function marks() {
        return $this->hasMany(\App\Model\Mark::class, 'student_id', 'student_id');
    }

    public function sattendances() {
        return $this->hasMany(\App\Model\Sattendance::class, 'student_id', 'student_id');
    }

    public function specialPromotions() {
        return $this->hasMany(\App\Model\SpecialPromotion::class, 'student_id', 'student_id');
    }
    public function subjectMarks() {
        return $this->hasMany(\App\Model\SubjectMark::class, 'student_id', 'student_id');
    }

    public function studentFeesInstallmentsUnsubscriptions() {
        return $this->hasMany(\App\Model\StudentFeesInstallmentsUnsubscription::class, 'student_id', 'student_id');
    }

    public function syllabusStudentBenchmarkings() {
        return $this->hasMany(\App\Model\SyllabusStudentBenchmarking::class, 'student_id', 'student_id');
    }

    public function payments() {
        return $this->hasMany(\App\Model\Payment::class, 'student_id', 'student_id');
    }

    public function studentSponsors() {
        return $this->hasMany(\App\Model\StudentSponsor::class, 'student_id', 'student_id');
    }

    public function studentOthers() {
        return $this->hasMany(\App\Model\StudentOther::class, 'student_id', 'student_id');
    }

    public function studentParents() {
        return $this->hasMany(\App\Model\StudentParent::class, 'student_id', 'student_id');
    }

    public function tmembers() {
        return $this->hasMany(\App\Model\Tmember::class, 'student_id', 'student_id');
    }

    public function studentAddresses() {
        return $this->hasMany(\App\Model\StudentAddress::class, 'student_id', 'student_id');
    }

    public function country() {
        return $this->hasOne(\App\Model\ReferCountry::class, 'id', 'country_id')->withDefault(['country' => 'Unknown']);
    }
      public function lmember() {
        return $this->hasOne(\App\Model\Lmember::class, 'student_id', 'student_id');
    }

     public function diary() {
        return $this->hasMany(\App\Model\Diary::class, 'student_id', 'student_id');
    }
      public function admission() {
        return $this->hasMany(\App\Model\Admission::class, 'student_id', 'student_id');
    }
}
