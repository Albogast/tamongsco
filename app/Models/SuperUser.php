<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SuperUser extends Model {

    /**
     * Generated
     */
    protected $table = 'users';
    protected $fillable = ['id', 'firstname', 'middlename', 'lastname', 'email', 'password', 'role_id', 'type', 'name', 'remember_token', 'dp', 'phone', 'town', 'created_by', 'photo','about','salary','sex','skills','marital','date_of_birth','personal_email','tshirt_size','joining_date','contract_end_date','academic_certificates','medical_report','driving_license','valid_passport','next_kin','personal_email','employment_category','national_id','position','company_file_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function name() {
        return $this->attributes['firstname'] . ' ' . $this->attributes['lastname'];
    }

    public function location() {
        return $this->hasMany('App\Model\Location');
    }

    public function uattendance() {
        return $this->hasMany(\App\Models\Uattendance::class, 'user_id', 'id');
    }

}



