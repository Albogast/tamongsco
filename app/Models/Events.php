<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Task
 *
 * @author hp
 */
class Events extends Model {
   
    //put your code here
    protected $table = 'tamongsco_events';
    protected $fillable = ['id','title', 'note', 'location', 'event_date', 'start_time', 'end_time', 'status', 'user_id', 'created_at', 'updated_at','link'];

    public function user() {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id')->withDefault(['name' => 'User Not allocated']);
    }
    public function members() {
        return $this->hasMany(\App\Models\EventAttendee::class, 'event_id', 'id');
    }

}
