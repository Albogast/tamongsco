<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class School extends Model {

    /**
     * Generated
     */
    protected $table = 'admin.schools_all';
    protected $fillable = ['id','name', 'region', 'ward', 'district', 'ownership','type','student','schema_name','region_id','latitude','langitude','number','status'];
    public function users() {
        return $this->hasManyThrough(\App\Models\UsersSchool::class,\App\Models\User::class);
    }
    
    public function regions() {
        return $this->belongsTo(\App\Models\Region::class, 'region_id', 'id')->withDefault(['name' => 'Not Defined']);
    }

    public function contacts() {
        return $this->hasMany(\App\Models\Member::class);
    }
}
