<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Region extends Model {

    /**
     * Generated
     */
    protected $table = 'admin.regions';
    protected $fillable = ['id', 'name', 'created_at','updated_at'];

    public function schools() {
        return $this->hasMany(\App\Models\School::class, 'region_id', 'id');
    }


}
