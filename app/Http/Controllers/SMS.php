<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;

class SMS extends Admin_Controller {

    public $patterns = array(
        '/#name/i', '/#username/i', '/#default_password/i', '/#email/i', '/#phone/i', '/#role/i', '/#student_name/i', '/#invoice/i', '/#balance/i', '/#student_username/i'
    );

     private $schema_name;
    function __construct() {
        parent::__construct();
    }
   

    public function inbox()
    {
        $this->data['schema_name'] = $schema_name = request()->segment(3);
        $this->data['id'] = $id = request()->segment(4);
        $id  =  empty($id) ? 2 : $id;
          if($schema_name){
            $this->data['users'] = $users= DB::table($schema_name.'.users')->where(DB::raw('lower(usertype)'), '!=', 'student')->latest()->get();

           $sql = 'SELECT a.message, a.created_at as date,a."from",a.id, b.name,b.photo, b."table"
           from '.$schema_name. '.reply_sms a join '.$schema_name. '.users b on (b.id=a.user_id and b."table"=a."table")  where a.from not in(\'+255655406004\',\'+255754406004\')';
           $this->data['reply_sms']  = DB::select($sql);
          }
        return view('communications.inbox', $this->data);
    }

   
      public function search() {
        $this->data['schema'] = $schema = request()->segment(3);
        $tag = request('wd');
        $users = DB::select("select * from $schema.users where lower(name) like '%" . strtolower($tag) . "%' and usertype !='Student' ");
        foreach ($users as $user) {
            echo '<a href="#" id="' . $user->id . '" data-table="' . $user->table . '" class="user_message" style="background: #CCC" onmousedown="user_messages()">
                  <div class="profile_details" ><div id="div' . $user->id . '"  class="well profile_view"  >  <div class="col-sm-12">  <h4 class="brief"><i>Role: ' . $user->usertype . '</i></h4> <div class="left col-xs-7">  <h2>' . $user->name . '</h2> <ul class="list-unstyled"><li><i class="fa fa-phone"></i> Phone #: ' . $user->phone . '</li> </ul> </div><div class="right col-xs-5 text-center"> <img src="' . base_url("storage/uploads/images/defualt.png") . '" alt="" class="img-circle img-responsive"></div></div></div> </div></a>';
        }
    }



      public function destroy()
       {
        $id = request('id');
        $schema_name = request('schema');
        $reply = DB::table($schema_name.'.reply_sms')->where('id',$id)->delete();
      //  $reply->delete();
        return 1;
    }


      public function reply() {
        $message = request('content');
        $phone_number = request('phone');
        $schema_name = request('schema');
        if ($message != '' && $phone_number != '' && strlen($phone_number) > 6) {
            $this->send_sms($schema_name,$phone_number, $message);
            echo json_encode(array('success' => 1));
        } else {
            echo json_encode(array('success' => 0));
        }
    }



     public function create($content = null, $phone = null) {
        $message = $content == 'SMS' ? request('content') : $content;
        $phone_number = $phone == 'create' ? request('data_id') : $phone;
        $schema_name =  request('schema_name');
        dd($schema_name);

        if ($message != '' && $phone_number != '' && strlen($phone_number) > 6) {
            $user = \App\Model\User::where('phone', 'ilike', $phone_number)->first();

            if (!empty($user)) {
                $user_info = $user->userInfo(DB::table($user->table));
                $patterns = array(
                    '/#name/', '/#username/', '/#default_password/'
                );

                if (preg_match('/default_password/', $message)) {
                    //reset password to default to this user
                    $pass = rand(1, 999) . substr(str_shuffle('ABCDEFGHJKLMNPQRSTUVWXYZ'), 0, 3) . rand(1, 999);
                    $password = bcrypt($pass);
                    DB::table($user->table)->where('username', $user_info->username)->update(['password' => $password, 'default_password' => $pass]);
                } else {
                    $pass = NULL;
                }

                $replacements = array(
                    $user->name, $user->username, $pass
                );

                $sms = preg_replace($patterns, $replacements, $message);

                $this->send_sms($phone_number, $sms);
                return '<li><img src="' . base_url("storage/uploads/images/" . $this->data['siteinfos']->photo) . '" class="avatar" alt="Avatar"><div class="message_date"> <h3 class="date text-info">' . timeAgo(date('d M Y H:i:s')) . '</h3></div><div class="message_wrapper"><h4 class="heading">' . $this->data['siteinfos']->name . '</h4><blockquote class="message">' . $sms . '</blockquote><br></div></li>';
            } else {
                return '<div class="alert alert-warning">Message failed to be sent. Phone number is not valid</div>';
            }
        }
    }



    public function template()
    {    
          $this->data['schema_name'] = $schema_name = request()->segment(3);
          if($schema_name){
            $this->data['mailandsmstemplates'] =  DB::table($schema_name.'.mailandsmstemplate')->orderBy('status', 'desc')->get();
          }
         // $this->data["subview"] = "mailandsmstemplate/index";
          return view('communications.templates',$this->data);
    }


    public function addtemplate(){
            $this->data['schema_name'] =  request()->segment(3);

            if($_POST){
                   $validateData = request()->validate([
                        'name' => 'required',
                        'template' => 'required'
                   ]);
                $schema_name =request('schema_name');
                DB::table($schema_name.'.mailandsmstemplate')->insert(request()->except('schema_name','_token'));
                redirect(url("sms/template/".$schema_name))->with("success","Added successfully");
            }
            return view("communications/addtemplate",$this->data);
        } 


        public function templateview(){
            $this->data['schema_name'] = $schema_name =  request()->segment(3);
            $this->data['id'] = $id =  request()->segment(4);
            if((int) $id){
              $this->data['mailandsmstemplate'] = DB::table($schema_name.'.mailandsmstemplate')->where('id', $id)->first();
              return view("communications.viewtemplate", $this->data);
            }
        }

         public function templatedelete() {
            $schema_name =  request()->segment(3);
            $id =  request()->segment(4);
            if ((int) $id) {
                 DB::table($schema_name.'.mailandsmstemplate')->where('id', $id)->delete();
                return redirect("sms/template/".$schema_name)->with('success','Deleted siccessfully');
            } else {
                return redirect("sms/template/".$schema_name);
            }
       }

         public function schedule() {
              $this->data['schema_name'] = $schema_name =  request()->segment(3);
              
          //  $this->data['classlevels'] = $this->classlevel_m->get_order_by_classlevel();
                if($schema_name){
                      $this->data['reminders'] = DB::table($schema_name.'.reminders')->latest()->get();
                }
                $this->data["subview"] = "communications/schedule/reminder_index";
                return view($this->data["subview"], $this->data);
            
         }

      public function add_schedule() {
       $this->data['schema_name'] = $schema_name =  request()->segment(3);
    
        $this->data['usertypes'] = DB::table($schema_name.'.role')->get();
        $this->data['templates'] = DB::table($schema_name.'.mailandsmstemplate')->get();
        if ($_POST) {
            
            if (in_array("0", request('user_id'))) {
                $users_id = array();
                $users = DB::table($schema_name.'.users')->where('role_id', request('usertype_id'))->get();
                foreach ($users as $user) {
                    $users_id[] = $user->id;
                }
                exit;
                $clean_ids = preg_replace('/[^0-9,.]/', '', $users_id);

                $user_id = implode(',', $clean_ids);
            } else {

                $clean_ids = preg_replace('/[^0-9,.]/', '', request('user_id'));

                $user_id = implode(',', $clean_ids);
            }
            $arr = [
                'user_id' => $user_id,
                'role_id' => request('usertype_id'),
                'date' => date('Y-m-d h:i', strtotime(request('date'))),
                'time' => date('Y-m-d h:i', strtotime(request('time'))),
                'mailandsmstemplate_id' => (int) request('template_id'),
                'message' => request('message'),
                'last_schedule_date' => request('last_date'),
                'title' => request('title'),
                'is_repeated' => request('is_repeated'),
              //  'days' => request('date') == Null ? implode(',', request('days')) : ''
            ];
            DB::table($schema_name.'.reminders')->insert($arr);
            return redirect('sms/schedule/'.$schema_name)->with('success', 'Added successfully');
        } else {
            $this->data["subview"] = "communications/schedule/schedule_reminder_index";
            return view($this->data["subview"], $this->data);
        }
    }

    function getTemplateContent() {
        $id = (int) request('templateID');
        $template = DB::table('public.mailandsmstemplate')->where('id', $id)->first();
        echo $template->template;
    }

      public function CallUsers() {
        $schema_name = request('schema_name');
        $usertype_id = request('usertype_id');

        $users = DB::select('select * from ' . $schema_name.'.users where role_id=' . $usertype_id . '');

        if (empty($users)) {
            echo '0';
        } else {
            echo "<option value='" . 0 . "'>All</option>";

            foreach ($users as $user) {
                echo "<option value='" . $user->id . "'>" . $user->name . "</option>";
            }
        }
    }



      public function deleteReminder() {
       $this->data['schema_name'] = $schema_name =  request()->segment(3);
       $this->data['id'] = $id =  request()->segment(4);

        $p = (int) $id > 0 ? DB::table($schema_name.'.reminders')->where('id', $id)->delete() : 0;
        return redirect()->back()->with('success', (int) $p == 0 ? 'Reminder failed to be Deleted' : 'Added successfully');
    }


     public function sentItems() {
        $this->data['schema_name'] = $schema_name =  request()->segment(3);
        $id = $this->data['set'] = (int) request()->segment(4);
        $type = $this->data['type'] = (int) clean_htmlentities(request()->segment(5));
        $other=(int) clean_htmlentities(request()->segment(6));

        if($schema_name){
        $this->data['sent_sms'] = DB::table($schema_name.'.sms')->where('status', 1)->count();
        $this->data['quick_sent_sms'] = DB::table($schema_name.'.sms')->where('type', 1)->count();
        $schema = strtolower(str_replace('.', null, $schema_name));
        $this->data['pending_sms'] = DB::table($schema_name.'.sms')->where('status', 0)->count();
        
        $this->data['sent_email'] = DB::table($schema_name.'.email')->where('status', 1)->count();
        $this->data['pending_email'] = DB::table($schema_name.'.email')->where('status', 0)->count();
        
        if ($schema_name && $type >= 0 && $id >= 0) {
            $table = $type == 1 ? $schema_name.'.sms' : $schema_name.'.email';
            if ($type == 1) {
                
                switch ($id) {
                    // case 3:
                    //     //delivered sms
                    //     $this->data['smss'] = DB::select("select * from $schema_name.pending_sms where lower(sms_id) like '%$schema%' and status=3 ");
                    //     break;
                    // case 4:
                    //     //failed sms
                    //     $this->data['smss'] = DB::select("select * from $schema_name.pending_sms where lower(sms_id) like '%$schema%' and status=4 ");
                    //     break;
                    // case 7:
                    //     //active in phone sms
                    //     $this->data['smss'] = DB::select("select * from $schema_name.pending_sms where lower(sms_id) like '%$schema%' and status=1 ");
                    //     break;
                    case 0:
                        //pending sms
                        $this->data['smss'] = DB::table($schema_name.'.sms')->where('status', 0)->orderBy('created_at', 'DESC')->paginate();
                        break;
                    default:
                        //pending sms
                        $this->data['smss'] =(int) $other==1? DB::table($schema_name.'.sms')->where('type',1)->paginate() : DB::table($table)->where('status', $id)->orderBy('created_at', 'DESC')->paginate();
                        break;
                }
            } else {
             
                $this->data['smss'] = DB::table($table)->where('status', $id)->orderBy('created_at', 'DESC')->paginate();
            }
            $this->data["subview"] = "communications/summaryreport";
            return view($this->data["subview"], $this->data);
          }
        }    else {
             $this->data["subview"] = "communications/summaryreport";
             return view($this->data["subview"], $this->data);
        }
    }



        public function delete() {
        $this->data['schema_name'] = $schema_name =  request()->segment(3);
         
        $id = clean_htmlentities(request()->segment(4));
        $type = (int) clean_htmlentities(request()->segment(5));
        $table = $type == 1 ? $schema_name.'.sms' : $schema_name.'.email';
        $_id = $type == 1 ? 'sms_id' : 'email_id';
        if ($id == 'bulk') {
            $seg = clean_htmlentities(request()->segment(6));
            if ($seg == 'all') {
                DB::table($table)->where('status', 0)->delete();
            } else {
                $ids = explode(',', trim($seg, ','));
                DB::table($table)->whereIn($_id, $ids)->delete();
            }
            return redirect()->back()->with('success', 'success');
        } else {
            $fid = (int) $id;
            DB::table($table)->where($_id, $fid)->delete();
            return redirect()->back()->with('success', 'success');
        }
    }

         public function composeEmail(){
            //    $this->data['schema_name'] = $schema_name =  request()->segment(3);
               
                $this->data['sms_keys'] = DB::table('public.sms_keys')->get();
                $this->data['templates'] = DB::table('public.mailandsmstemplate')->where('status', '1')->get();

            //     if($schema_name){
            //       $this->data['usertypes'] = DB::select('select name as type, id from ' . $schema_name.'.role');
            //     }
                if($_POST){
                 
               /*     "schema_name" => array:2 [▼
                    0 => "shulesoft"
                    1 => "goldland"
                  ]
                  "sms_keys_id" => array:1 [▼
                    0 => "phone-sms"
                  ]
                  "firstCriteria" => "00"
                  "custom_numbers" => null
                  "criteria" => "0"
                  "teachersCriteria" => null
                  "amount" => null
                  "template" => "1"
                  "message" => "Hello #name, your email address is #email and registered phone number is #phone . If you need to know your role, then its #role"
                  "_token" => "US8mvPrbdj9fz6yNPX6fcGcjvwjGPieULJTBJAIE"
                    */
                if(request('usertype') == '00'){
                    $table = "'parent'";
                }elseif(request('usertype') == '01'){
                    $table = "'teacher'";
                }elseif(request('usertype') == '02'){
                    $table = "'user'";
                }else{

                }
                
                if(request('schema_name')[0] == 'all'){
                    $schema_name  = available_schemas();
                }else{
                    $schema_name = request('schema_name');
                }

                 $patterns = array(
                    '/#name/i', '/#username/i', '/#default_password/i', '/#email/i', '/#phone/i', '/#role/i', '/#student_username/i',
                  );
            
                foreach($schema_name  as $schema){
                    $thus = "'$schema'";
                    $users = DB::select('SELECT name,phone,usertype,email,username,id,default_password FROM admin.all_users WHERE status=1 AND "table"='.$table.' and schema_name = '.$thus);
                 if(isset($users) && count($users) > 0) {
                 foreach ($users as $user) {

                     $message  = request('message');
                    if (preg_match('/default_password/i', $message)) {
                   //reset password to default to this user
                   $pass = rand(1, 999) . substr(str_shuffle('ABCDEFGHJKLMNPQRSTUVWXYZ'), 0, 3) . rand(1, 999);
                   $password = bcrypt($pass);
                   $table_column = $user->table == 'student' ? 'student_id' : $user->table . 'ID';

                     DB::table($user->table)->where($table_column, $user->id)->update(['password' => $password, 'default_password' => $pass]);
                    } else {
                        $pass = '';
                    }

                  //  $message = str_replace('#name', $user->name, request('message'));
                     
                    $replacements = array(
                        $user->name, $user->username, $pass, $user->email, $user->phone, $user->usertype
                    );

                      $message = $this->getCleanSms($replacements, $message);
                
                      $this->send_sms($schema,$user->phone, $message);
                     }
                   }
                 } 
               }

                $this->data["custom_sms"] = 1;
                $this->data["email"] = 0;
                $this->data["sms"] = 0;
                $this->data["sms_parent"] = 2;
                $this->data["subview"] = "communications/compose";
                return view( $this->data["subview"], $this->data);
         }


       public function getCleanSms($replacements, $message, $pattern = null) {
        /*
         *  $message=hello #name, unahitajika mjini #town
         * $sms=preg_replace(array('//#namei','/#town/i'), array('Juma', 'Mwanza'), $message);
         */

         $sms = preg_replace($pattern != null ? $pattern : $this->patterns, $replacements, $message);
         if (preg_match('/#/', $sms)) {
            //try to replace that character
            return preg_replace('/\#[a-zA-Z]+/i', '', $sms);
          } else {
            return $sms;
         }
    }




       public function fcomposeEmail() {
        $this->data['schema_name'] = $schema_name =  request()->segment(3);
         

        $this->data['templates'] = DB::table('mailandsmstemplate')->where('status', '1')->get();
        $this->data['sms_keys'] = DB::table('sms_keys')->get();
            //independent of the area, we need to show usertypes here
            $this->data['usertypes'] = DB::select('select name as type, id from ' . set_schema_name() . 'role');
            /* Start For SMS */
            $sms_user = request("sms_user");
            if ($sms_user != 'select') {
                $this->data['sms_templates'] = DB::table('mailandsmstemplate')->where('type', 'SMS')->get();
                $this->data['sms_user'] = request("sms_user");
            } else {
                $this->data['sms_templates'] = 'empty';
                $this->data['sms_user'] = 'select';
            }
            $this->data['sms_templateID'] = 'select';
            /* End For SMS */

             
            if($schema_name){

            $this->data['classes'] = DB::table($schema_name.'.classes')->get();
            $this->data['schema'] = $schema = str_replace('.', '', $schema_name);
            $types = $this->db->query("SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema='" . $schema . "' AND table_name='student'")->result();
            $haystack = array("health_other", "academic_year_id", "created_at", "usertype", "password", "username", "year", "photo", "paidamount", 'section', "totalamount", "transport", "roll", "sectionID", "classesID", "phone", "status", "updated_at", "health", "student_id", "create_date", "email", "dob", "name", "parent_type_id", "religion", "nationality", "address");
            foreach ($types as $key => $type) {
                if (in_array($type->column_name, $haystack)) {
                    unset($types[$key]);
                }
            }
            $this->data['student_types'] = $types;
            $this->data['payment_status'] = array(0 => 'pending invoice', 1 => 'With Discount', 2 => 'With Due Amount');
            $this->data['parents'] = DB::table($schema_name.'.parent')->where('status', 1)->get();
            $this->data['teachers'] = DB::table($schema_name.'.teacher')->where('status', 1)->get();

            $this->data['teachers'] = DB::table($schema_name.'.fees')->whereNotIn('id', [1000, 2000])->get();
    
            /* End from parent_sms() function */

            if ($_POST) {
                $this->validate(request(), [
                    'message' => 'required',
                    'user' => 'required',
                    strtolower(request('type')) == 'email' ? "'email_subject'=>'required'" : ''
                        ], $this->custom_validation_message);
                $subject = request('email_subject');
                $message = request('message');
        
                $allusers = DB::table($schema_name.'.users')::whereIn('role_id', request('user'))->where('status', 1)->get();
                $patterns = array(
                    '/#name/i', '/#username/i', '/#default_password/i', '/#email/i', '/#phone/i', '/#role/i', '/#student_username/i',
                );


                foreach ($allusers as $user) {

                    if (preg_match('/default_password/i', $message)) {
                        //reset password to default to this user
                        $pass = rand(1, 999) . substr(str_shuffle('ABCDEFGHJKLMNPQRSTUVWXYZ'), 0, 3) . rand(1, 999);
                        $password = bcrypt($pass);
                        $table_column = $user->table == 'student' ? 'student_id' : $user->table . 'ID';

                        DB::table($user->table)->where($table_column, $user->id)->update(['password' => $password, 'default_password' => $pass]);
                    } else {
                        $pass = '';
                    }
                   
                    $replacements = array(
                        $user->name, $user->username, $pass, $user->email, $user->phone, $user->usertype
                    );
                    $sms = $this->getCleanSms($replacements, $message);
                  
                  //  $this->send_sms($user->phone, $sms);
                }

                $array = array(
                    'users' => implode(',', request('user')),
                    'type' => request('type'),
                    'message' => $message,
                    'year' => date('Y')
                );
                $this->db->insert('mailandsms', $array);
            
                return redirect(url("sms/index"));
            } 
            
            else {
                $this->data["custom_sms"] = 1;
                $this->data["email"] = 0;
                $this->data["sms"] = 0;
                $this->data["sms_parent"] = 2;
                $this->data["subview"] = "communications/compose";
                return view( $this->data["subview"], $this->data);
            }
        }
 
    }

}
