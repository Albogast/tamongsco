<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use DB;
use Auth;
use Image;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\UsersImport;
use App\Imports\MemberImport;

class Users extends Admin_Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $group = \App\Models\GroupUser::where('user_id', Auth::User()->id)->first();
        $this->data['users'] = \App\Models\User::get();
        return view('dashboard.user', $this->data);
        
    }

    public function create() {
        $users = User::where('id', Auth::user()->id)->get();
        $this->data['roles'] = DB::table('roles')->get();
        $this->data['countries'] = DB::table('constant.refer_countries')->get();
        if($_POST){
        
           $user = User::create(array_merge(request()->all(), ['password' => bcrypt(request('email')), 'created_by' => Auth::user()->id]));
            if (!empty(request('photo'))) {
                $filename = 'portal'.time() . rand(11, 8844) . '.' . request()->file('photo')->guessExtension();
                Image::make(request()->file('photo'))->resize(232, 285)->save('storage/uploads/images/' . $filename);
                User::where('id', $user->id)->update(['photo' => $filename]);
            }

            $this->sendEmailAndSms($user);
            return redirect('users/index')->with('success', 'User ' . $user->name . ' created successfully');
        }
        return view('dashboard.add_user', $this->data);
    }

    
    public function addMember() {
        $users = User::where('id', Auth::user()->id)->get();
        $this->data['roles'] = DB::table('roles')->get();
        $this->data['regions'] =  \App\Models\Region::where('country_id', 1)->get();        
        if($_POST){
            $user = \App\Models\Member::create([
                'name' => request('name'),
                'email' => request('email'),
                'phone' => request('phone'),
                'school_id' => request('school_id'),
                'user_id' => Auth::user()->id,
                'title' => request('title')
            ]);
            return redirect()->back()->with('success', 'user recorded successfully');
            $this->sendEmailAndSms($user);
            return redirect('users/members')->with('success', 'User ' . $user->name . ' created successfully');
        }
        return view('dashboard.add_member', $this->data);
    }

    
    public function sendEmailAndSms($requests, $content = null) {
        $request = (object) $requests;
        $message = $content == null ? 'Hello ' . $request->name . ' You have been added in  TAMONGSCO Portal. You can login for Administration of schools with username ' . $request->email . ' and password ' . $request->email : $content;
        \DB::table('public.sms')->insert([
            'body' => $message,
            'user_id' => 1,
            'phone_number' => $request->phone,
            'table' => 'setting'
        ]);
        \DB::table('public.email')->insert([
            'body' => $message,
            'subject' => 'ShuleSoft Administration Credentials',
            'user_id' => 1,
            'email' => $request->email,
            'table' => 'setting'
        ]);
    }

    public function userinfo(){
        $id = request()->segment(3);
            $this->data['staff_id'] = (int)$id > 0 ? $id : Auth::user()->id;
            $this->data['messages'] = \App\Models\SMS::where('user_id', $this->data['staff_id'])->get();
            $this->data['staff'] = \App\Models\User::where('id', $this->data['staff_id'])->first();
        return view('dashboard.profile', $this->data);
    } 

  
    public function members() {
        $this->data['home'] = 'Teachers';
        $id = request()->segment(3);
        $this->data['regions'] = \App\Models\Region::where('country_id', 1)->get();
        $this->data['teachers'] = (int)$id > 0 ?  \App\Models\Member::whereIn('school_id', \App\Models\School::where('region_id', $id)->where('ownership', '<>', 'Government')->get(['id']))->get() : \App\Models\Member::get();

        return view('dashboard.users', $this->data);
    }

      
    public function deletemember() {
        $id = request()->segment(3);
         \App\Models\Member::where('id', $id)->delete();
         return redirect()->back()->with('success', 'School staff deleted.');

    }



    public function user() {
        $this->data['home'] = 'Staffs';
        if(isset($schema)){
            $this->data['staffs'] = $this->load_all_users($schema, null,null,'Staffs');
            $this->data['school_name']  = $this->school_name($schema);
            $this->data['all']  =  $this->count_staff($schema);
            $this->data['male'] = $this->count_staff($schema,'Male');
            $this->data['female'] = $this->count_staff($schema,'Female'); 
        } 
        return view('users.staffs', $this->data);
    }

    public function report($id)
    {
        return view('user.profile', [
            'user' => User::findOrFail($id)
        ]);
    }


    public function teacherprofile(){
        if(request()->segment(3) > 0){
            $id = request()->segment(3);
            $this->data['teacher'] =   \App\Models\Member::where('id', $id)->first();
            $this->data['messages'] =  \App\Models\Sms::where('user_id', $id)->orderBy('sms_id', 'DESC')->get();
            return view('dashboard.teacher_profile', $this->data);
        }
    } 

    public function profile() {
        $school = $this->data['schema'] = request()->segment(3);
        $id = request()->segment(4);
        $this->data['shulesoft_users'] = \App\Models\User::where('status', 1)->where('role_id', '<>', 7)->get();
        $is_client = 0;
        if ($school == 'school') {
            $id = request()->segment(4);
            $this->data['client_id'] = $id;
            $this->data['school'] = \collect(DB::select('select name as sname, name,schema_name, region , ward, district as address  from admin.schools where id=' . $id))->first();
        } else {
            $is_client = 1;
            $this->data['school'] = DB::table($school . '.setting')->first();
            $this->data['levels'] = DB::table($school . '.classlevel')->get();
            $client = \App\Models\Client::where('username', $school)->first();
            if (empty($client)) {
                $client = \App\Models\Client::create(['name' => $this->data['school']->sname, 'email' => $this->data['school']->email, 'phone' => $this->data['school']->phone, 'address' => $this->data['school']->address, 'username' => $school, 'created_at' => date('Y-m-d H:i:s')]);
            }
            $this->data['client_id'] = $client->id;

            $this->data['top_users'] = DB::select('select count(*), user_id,a."table",b.name,b.usertype from ' . $school . '.log a join ' . $school . '.users b on (a.user_id=b.id and a."table"=b."table") where user_id is not null group by user_id,a."table",b.name,b.usertype order by count desc limit 5');
        }
        $this->data['profile'] = \App\Models\ClientSchool::where('client_id', $client->id)->first();
        $setting = "'setting'";
        $this->data['list_users'] = DB::select('SELECT count(*) as total, a."table" from ' . $school . '.users a where status=1 AND "table" !='.$setting.' group by a."table" order by "table" desc limit 5');

        $this->data['is_client'] = $is_client;

        $year = \App\Models\AccountYear::where('name', date('Y'))->first();
        $this->data['invoices'] = \App\Models\Invoice::where('client_id', $client->id)->where('account_year_id', $year->id)->get();
        
            return view('users/profile', $this->data);
        }


        public function viewMessage(){

            $this->data['messages'] = \App\Models\Sms::latest()->get();
            return view('dashboard.view_message',$this->data);
        }


      public function message()
      {  
          if($_POST){
               //Send SMS to Family Member
               if(request('region_id') != '' && request('region_id')[0] == 'all'){
                $users = \App\Models\Member::get();
            }elseif(request('region_id') != ''){
               // $users = \App\Models\Member::whereIn('school_id', request('region_id'))->get();
                $users = \App\Models\Member::whereIn('school_id', \App\Models\School::whereIn('region_id', request('region_id'))->where('ownership', '<>', 'Government')->get(['id']))->get();
            }
            $message = request('body');
                if (count($users) > 0 && strlen($message) > 15) {  
                    foreach($users as $user){ 
                       if($user->phone != ''){
                        $body = $message;
                        $data = ['subject' => empty($user->name) ? $user->name.' New Message' : ' New Message', 'sent_from' => 'phone', 'user_id' => isset($user->id) ? $user->id : 1, 'phone' => $user->phone, 'body' => isset($body) ? $body : 'TAMONGSCO New Message', 'status' => 0];
                       // dd($data);
                        \App\Models\Sms::create($data);
                    }
                }
                return redirect()->back()->with('success', 'Congraturations ' . count($users) . ' Messages Sent Successfully');
            }
        }
           $this->data['parts']  = \App\Models\Region::where('country_id', 1)->latest()->get();
           $this->data['Permissionsgroup'] = \App\Models\PermissionGroup::get();
           return view('dashboard.message',$this->data);
      }
 
     public function permissions()
     {  
          $id = request()->segment(3);
          $this->data['set']  = (int) $id;
          $this->data['roles']  = \App\Models\Role::latest()->get();
          $this->data['permission']  = \App\Models\Permission::get();
          $this->data['Permissionsgroup'] = \App\Models\PermissionGroup::get();
          $this->data['user_roles'] = DB::table('admin.portal_roles')->get();
          return view('operations.permission',$this->data);
     }

     public function uploadMembers() 
     {  
         Excel::import(new UsersImport, request()->file('call_file'));
        
         return redirect()->back()->with('success', 'All Call Histories Uploaded Successfully!');
     }
     
     public function uploadEvents() 
     {  
         Excel::import(new MemberImport, request()->file('call_file'));
        
         return redirect()->back()->with('success', 'All Call Histories Uploaded Successfully!');
     }

    public function storepermission() {
        $role = new \App\Models\PermissionRole();
        $role->permission_id = request('perm_id');
        $role->role_id = request('role_id');
        $role->created_by = \Auth::user()->id;
        $role->save();
        echo 'Permission created successfully';
    }

   public function removepermission() {
        $permission_id = request('perm_id');
        $role_id = request('role_id');
        \App\Models\PermissionRole::where(['permission_id' => $permission_id, 'role_id' => $role_id])->delete();
        echo 'success';
  }


  public function  storeroles(){
    if($_POST){
         $validateData = request()->validate([
           'name' => 'required|max:255',
           'description' => 'required|max:255',
       ]);

        $role = new \App\Models\Role();
        $role->name = request('name');
        $role->description = request('description');
        $role->created_by = Auth::user()->id;
        $role->save();
        return redirect()->back()->with('success', 'Role created successfully');
     }
  }


}