<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Vendor extends Admin_Controller {

    /**
     * -----------------------------------------
     * 
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     * 
     * 
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     * 
     * 
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    public $schema;

    function __construct() {
        parent::__construct();
    }

    public function index() {
        if($_POST){
            $schema_name = request('schema');
            $this->data['vendors'] = \DB::table('admin.vendors')->where('schema_name', $schema_name)->get();
        return view('account.vendor.custom_vendor',$this->data); 
        }else{
            return view('account.vendor.index');
        }
    }

    protected function rules() {
        return $this->validate(request(), [
                    'name' => 'required|max:255',
                    'phone_number' => 'required|numeric|min:1',
                    'location' => 'required|max:200',
                    'country_id' => 'required',
                    'city' => 'required',
                    "items" => "required",
                        ], $this->custom_validation_message);
    }

    public function add() {
        if (can_access('add_vendor')) {
            if ($_POST) {
                $this->rules();
                $url = '';
                if(request()->file("attachment") != ''){
                    $url = $this->saveFile(request()->file("attachment"), 'shulesoft');
                }
                $vendor = \App\Model\Vendor::create(array_merge(request()->all(), [
                            'created_by' => session('id'),
                            'created_by_table' => session('table'),
                            'schema_name' => $this->schema,
                            'attachment' => $url
                ]));
                foreach (request('items') as $value) {
                    \App\Model\VendorProductRegister::create([
                        'vendor_id' => $vendor->id, 'product_register_id' => $value
                    ]);
                }
                $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                return redirect(base_url("vendor/index"));
            } else {
                $this->data["subview"] = "vendor/add";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function edit() {
        if (can_access('edit_vendor')) {
            $id = clean_htmlentities(($this->uri->segment(3)));
            if ((int) $id) {
                $this->data['vendor'] = \App\Model\Vendor::find($id);
                if ($this->data['vendor']) {
                    if ($_POST) {
                        $this->rules();
                        $url = '';
                        if(request()->file("attachment") != ''){
                            $url = $this->saveFile(request()->file("attachment"), 'shulesoft');
                        }
                        $this->data['vendor']->update(array_merge(request()->all(), [
                            'created_by' => session('id'),
                            'created_by_table' => session('table'),
                            'schema_name' => $this->schema,
                            'attachment' => $url
                        ]));
                        foreach (request('items') as $value) {
                            $p = \App\Model\VendorProductRegister::where('vendor_id', $this->data['vendor']->id)->where('product_register_id', $value)->first();
                            if (empty($p)) {
                                \App\Model\VendorProductRegister::create([
                                    'vendor_id' => $this->data['vendor']->id, 'product_register_id' => $value
                                ]);
                            }
                        }

                        $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                        return redirect(base_url("vendor/index"));
                    } else {
                        $this->data["subview"] = "vendor/edit";
                        $this->load->view('_layout_main', $this->data);
                    }
                } else {
                    $this->data["subview"] = "error";
                    $this->load->view('_layout_main', $this->data);
                }
            } else {
                $this->data["subview"] = "error";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function delete() {
        if (can_access('delete_vendor')) {
            $id = clean_htmlentities(($this->uri->segment(3)));
            if ((int) $id) {
                \App\Model\Vendor::find($id)->delete();
                $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                return redirect(base_url("vendor/index"));
            } else {
                return redirect(base_url("vendor/index"));
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function show() {
        $id = clean_htmlentities(($this->uri->segment(3)));
        if ((int) $id) {
            $this->data["vendor"] = \App\Model\Vendor::find($id);
            if ($this->data["vendor"]) {
                $this->data["subview"] = "vendor/view";
                $this->load->view('_layout_main', $this->data);
            } else {
                $this->data["subview"] = "error";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

}
