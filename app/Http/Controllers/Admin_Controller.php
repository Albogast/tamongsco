<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Auth;
use DB;
use Excel;
use App;
use Image;
use Carbon\Carbon;

class Admin_Controller extends Controller
{
    

    function __construct() {
        $this->middleware('auth');        
        $this->data["all_users"] = \App\Models\User::get();
    }
  
    public function load_school($id){
        $group = \App\Models\GroupUser::where('user_id', $id)->first();
        return  \App\Models\Client::whereIn('id', \App\Models\ClientGroup::where('group_id', $group->group_id)->get(['client_id']))->get();
    }
   
    public function load_schema($id){
        $group = \App\Models\GroupUser::where('user_id', $id)->first();
        return  \App\Models\Client::whereIn('id', \App\Models\ClientGroup::where('group_id', \App\Models\GroupUser::where('user_id', $id)->first()->group_id)->get(['client_id']))->get(['username']);
    }


    public function available_schemas(){
        $allschemas = array();
        $values = $this->load_schema(Auth::User()->id);
            for ($i =0;$i < count($values); $i++){
              array_push($allschemas, $values[$i]->username);
         }
         return $allschemas;
     }

    public function load_users($id){
        $setting="'setting'";
        $group = \App\Models\GroupUser::where('user_id', $id)->first();
        $schema = \App\Models\Client::whereIn('id', \App\Models\ClientGroup::where('group_id', \App\Models\GroupUser::where('user_id', $id)->first()->group_id)->get(['client_id']))->get(['username']);
        return DB::SELECT('SELECT count(id),"table" from admin.all_users where status=1 and "table" != '.$setting.' and schema_name in (SELECT username from admin.clients where id in (select client_id from admin.client_groups where group_id='.$group->id.')) GROUP BY "table"');
    }
    
    public function school_name($schema=null){
        return \App\Models\Client::where('username',$schema=null)->first()->name;
    }

    public function class_name($schema=null,$class_id=null){
        return DB::table($schema.'.classes')->where('classesID',$class_id)->first()->classes;
    }

    public function  get_year($schema,$id){
        return DB::table($schema.'.academic_year')->where('id',$id)->first()->name; 
    }

    public function get_single_student($schema,$student_id){
        return DB::table($schema.'.student')->where('student_id',$student_id)->first();
    }
   

    
    function getStudentByYearClass($class_id, $academic_year_id, $section_id = NULL, $schema=null) {
        $where = ($section_id == NULL || (int) $section_id == 0 ) ? '' :
                ' AND a.section_id=' . $section_id;

        $sql = 'select a.*,b.name,b.photo,b.roll,c.section, c."classesID" from ' . $schema . '.student_archive a JOIN ' . '  ' . $schema . '.student b ON '
                . '      b."student_id"= a.student_id JOIN ' . $schema . '.section c
ON  c."sectionID"=a."section_id" WHERE b.status=1 AND '
                . '      a.academic_year_id=' . $academic_year_id . '  AND '
                . '      a.section_id IN (SELECT "sectionID" FROM ' . $schema . '.section WHERE "classesID"=' . $class_id . ') ' . $where;
        return DB::select($sql);
    }

      public function student_archive($schema,$student_id){
        return DB::table($schema.'.student_archive')
        ->select('student_archive.*','classes.classes as classes_name','section.section as section_name','academic_year.name as year')
        ->join($schema.'.section',$schema.'.section.sectionID', '=', $schema.'.student_archive.section_id')
        ->join($schema.'.classes',$schema.'.classes.classesID', '=', $schema.'.section.classesID')
        ->join($schema.'.academic_year',$schema.'.academic_year.id', '=', $schema.'.student_archive.academic_year_id')
        ->where($schema.'.student_archive.student_id', $student_id)->get();
      }

      public function load_schools($schema=null){
        return \App\Models\Client::where('username',$schema=null)->first();
    }

    function createDate($date, $format = 'm-d-Y', $return = 'Y-m-d') {
        return date('Y-m-d', strtotime($date));
    }

    public function load_levelstudent($schema,$year_id,$classlevel_id){
        return DB::SELECT('SELECT C."classesID", s.section_id, t.name, B.classes,C.section,count(A.student_id) as total from '.$schema.'.student A 
        join '.$schema.'.student_archive S on s.student_id = A.student_id 
        join '.$schema.'.section C on S.section_id = C."sectionID" 
        join '.$schema.'.classes B on B."classesID" = C."classesID" 
        join '.$schema.'.teacher t on B."teacherID" = t."teacherID" 
        join '.$schema.'.academic_year D on S.academic_year_id = D.id 
        where B.classlevel_id = '.$classlevel_id.' and A."academic_year_id" = '.$year_id.' and s.status = 1 GROUP BY 
        C."classesID", s.section_id, t.name, B.classes,C.section ORDER BY "classesID"');
    }

    public function load_all_levelstudent(){
        return DB::table('admin.all_student')
          ->join('admin.all_classes','admin.all_student.classesID','=','admin.all_classes.classesID')
          ->join('constant.refer_classes','constant.refer_classes.id','=','admin.all_classes.refer_class_id')
          ->select('constant.refer_classes.id','admin.all_classes.classesID','constant.refer_classes.name',
          DB::raw('count(admin.all_student.student_id) as total'),
           'admin.all_student.schema_name')
          ->whereIn('admin.all_student.schema_name',$this->available_schemas())
          ->where('admin.all_student.status','<>',0)
          ->groupBy('constant.refer_classes.id','constant.refer_classes.name','admin.all_student.classesID',
          'admin.all_classes.classesID','admin.all_student.schema_name')->get();
    }

  
    public function count_all_students($schema,$class_id,$year_id){
        if($class_id == null){
           return DB::table($schema.'.student')->where('academic_year_id',$year_id)->where("status", '<>', 0)->count();
           }
           return DB::table($schema.'.student')->where('classesID',$class_id)->where('academic_year_id',$year_id)->where("status", '<>', 0)->count();
       }
   

    public function count_male_students($schema,$class_id,$year_id){
        if($class_id ==null){
        return DB::table($schema.'.student')->where('academic_year_id',$year_id)->where("status", '<>', 0)->where('sex','LIKE','%M%')->count();
        }
        return DB::table($schema.'.student')->where('classesID',$class_id)->where('academic_year_id',$year_id)->where("status", '<>', 0)->where('sex','LIKE','%M%')->count();
    }

    public function count_female_students($schema,$class_id,$year_id){
        if($class_id == null){
            return  DB::table($schema.'.student')->where('academic_year_id',$year_id)->where("status", '<>', 0)->where('sex','LIKE','%F%')->count(); 
        }
        return  DB::table($schema.'.student')->where('classesID',$class_id)->where('academic_year_id',$year_id)->where("status", '<>', 0)->where('sex','LIKE','%F%')->count();     
    }
    
    public function find_total_sex($schema,$class_id,$year_id){
        if($class_id == null){
            return  DB::table($schema.'.student')->where('academic_year_id',$year_id)->where("status", '<>', 0)->where('sex','LIKE','%F%')->count(); 
        }
        return  DB::table($schema.'.student')->where('classesID',$class_id)->where('academic_year_id',$year_id)->where("status", '<>', 0)->where('sex','LIKE','%F%')->count();     
    }


        public function ajaxTable($table, $columns, $custom_sql = null, $order_name = null, $count = null) {
            ## Read value
        if (isset($_POST) && request()->ajax() == true) {
            $draw = $_POST['draw'];
            $row = $_POST['start'];
            $rowperpage = $_POST['length']; // Rows display per page
            $columnIndex = $_POST['order'][0]['column']; // Column index
            $columnName = $_POST['columns'][$columnIndex]['data']; // Column name
            $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
            $searchValue = $_POST['search']['value']; // Search value
              ## Search 
            $searchQuery = " ";
            if ($searchValue != '') {
                $searchQuery = " and ( ";
                $list = '';
                foreach ($columns as $column) {
                    $list .= 'lower(' . $column . "::text) like '%" . strtolower($searchValue) . "%' or ";
                }
                $searchQuery = $searchQuery . rtrim($list, 'or ') . ' )';
            }

                ## Total number of records without filtering
                // $sel = DB::select("select count(*) as allcount from employee");
                ## Total number of record with filtering
                ## Fetch records
            $columnName = strlen($columnName) < 1 ? '1' : $columnName;
            $total_records = 0;
            if (strlen($custom_sql) < 2) {
             // strlen($searchQuery); exit;
                $sel = \collect(DB::select("select count(*) as count from " . $table . " WHERE true " . $searchQuery))->first();
                $total_records = $sel->count;
                $empQuery = "select * from " . $table . " WHERE true " . $searchQuery . " order by \"" . $columnName . "\" " . $columnSortOrder . " offset  " . $row . " limit " . $rowperpage;
            } else {
                $empQuery = $custom_sql . " " . $searchQuery . " order by \"" . $columnName . "\" " . $columnSortOrder . " offset  " . $row . " limit " . $rowperpage;
                $total_records = $count == null ? count(DB::select($custom_sql)) : $count;
            }
            $empRecords = DB::select($empQuery);

             ## Response
            $response = array(
                "draw" => intval($draw),
                "iTotalRecords" => $total_records,
                "iTotalDisplayRecords" => $total_records,
                "aaData" => $empRecords
            );
            return json_encode($response);
        }
    }



        public function send_sms($phone_number, $message, $priority = 1, $sms_content_id = 1) {       
         
                //skip default teacher               
            if (!preg_match('/655406004/i', $phone_number)) {
                $sms_id = DB::table('admin.sms')->insertGetId(array(
                    'phone_number' => $phone_number,
                    'body' => $message,
                    'user_id' => $user->id,
                    'status'=> 0,
                    'table' => $user->table,
                    'sms_content_id' => 1,
                    'sent_from' => 'phonesms',
                    'subject' => request('email_subject'),
                    'type' => $type, 
                    'priority' => $priority, 
                    'sms_keys_id' => 2), 'sms_id');    
                    }
            }

    
    }
