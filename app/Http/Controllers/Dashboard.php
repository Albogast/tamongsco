<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use DB;

class Dashboard extends Admin_Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $this->data['users'] = \App\Models\Sms::count();
        $this->data['schools'] = \App\Models\School::count();
        $this->data['our_schools'] = \App\Models\Member::count();
        $this->data['events'] = \App\Models\Events::get();
        $this->data['regions'] = \App\Models\Region::where('country_id', 1)->orderBy('id', 'ASC')->limit('13')->get();
        $this->data['all_regions'] = \App\Models\Region::where('country_id', 1)->orderBy('id', 'DESC')->limit('13')->get();  
        $this->data['list_users'] = DB::table('portal_users')->get();
        return view('dashboard.index', $this->data);
    }

    public function show($id)
    {
        return view('user.profile', ['user' => User::findOrFail($id)]);
    }

    public function usage() {
        $this->data['home'] = 'Home';
        $id = request()->segment(3);
        $this->data['school'] = \App\Models\School::where('id', $id)->first();
        $this->data['user'] = $user =  \App\Models\User::where('id', Auth::User()->id)->first();
        $this->data['users'] =  \App\Models\User::get();
        $this->data['clients'] = \App\Models\Member::whereIn('school_id', \App\Models\School::where('id', $id)->where('ownership', '<>', 'Government')->get(['id']))->get();
        return view('dashboard.usage', $this->data);
    }

    public function school() {
        $this->data['home'] = 'Home';
        $id = request()->segment(3);
        $ward = request()->segment(4);
        $this->data['user'] = $user =  \App\Models\User::where('id', Auth::User()->id)->first();
        $this->data['regions'] =  \App\Models\Region::where('country_id', 1)->get();
        $this->data['schools'] = (int)$id > 0 ? \App\Models\School::where('region_id', $id)->where('ownership', '<>', 'Government')->get() : \App\Models\School::where('ownership', '<>', 'Government')->get();
        (int)$id > 0 && $ward != '' ? $this->data['schools'] = \App\Models\School::where('district', $ward)->where('ownership', '<>', 'Government')->get() : $this->data['schools'] = \App\Models\School::where('region_id', $id)->where('ownership', '<>', 'Government')->get();
        $this->data['region'] = (int)$id > 0 ? \App\Models\Region::where('country_id', 1)->where('id', $id)->first() : [];
        $this->data['districts'] = (int)$id > 0 ? DB::SELECT("SELECT count(id) as total, district FROM schools_all WHERE region_id=".$id." AND ownership != 'Government' GROUP BY district ORDER BY count(id) DESC") : [];
        return view('dashboard.school', $this->data);
    }
    public function docs() {
        $this->data['parts'] = DB::table('portal_parts')->get();
        return view('dashboard.index', $this->data);
    }

    public function help() {
        $this->data['set'] = request()->segment(3);

        $this->data['parts'] = \App\Models\Task::where('user_id', Auth::User()->id)->get();
        if ($_POST) {
            $array = [
                'body' => request('body'),
                'user_id' => Auth::User()->id,
                'client_id' => \App\Models\Client::where('username', request('schema_name'))->first()->id,
                'subpart_id' => request('subpart_id'),
                'priority' => 1
            ];
          //  dd($array);
            \App\Models\Task::create($array);
        }
        return view('dashboard.support', $this->data);
    }

    public function all_parts() {
        $part_id = request('id');
        if (!empty($part_id)) {
            $parts = DB::table('portal_subparts')->where('part_id', $part_id)->get();
                echo "<option value='0'>select here</option>";
                foreach ($parts as $part) {
                    echo '<option value=' . $part->id . '>' . $part->name . '</option>';
                }
            }else{
                echo '0';
            }
        }
 
        public function loadSchool() {
            $id = request('id');
            if ((int)$id > 0) {
                $parts =  (int) $id > 0 ? \App\Models\School::where('region_id', $id)->where('ownership', '<>', 'Government')->get() : \App\Models\School::where('ownership', '<>', 'Government')->get();
                    echo "<option value=''>select school here</option>";
                    foreach ($parts as $part) {
                        echo '<option value=' . $part->id . '>' . $part->name .' - ' .strtoupper($part->type). '</option>';
                    }
                }else{
                    echo '0';
                }
            }

        public function addEvent() {
            if ($_POST) {
                $filename = null;
               
            if (!empty(request('attach'))) {
                $file = request()->file('attach');
                $filename = 'tangmosco_'.time() . rand(11, 8894) . '.' . $file->guessExtension();
                $filePath = base_path() . '/storage/uploads/';
                $file->move($filePath, $filename);
            }
                $array = [
                    'title' => request('title'),
                    'note' => request('note'),
                    'event_date' => request('event_date'),
                    'start_time' => request('start_time'),
                    'end_time' => request('end_time'),
                    'location' => request('location'),
                    'link' => request('meeting_link'),
                    'user_id' => Auth::user()->id,
                    'attach' => $filename
                ];
              //  dd($array);
                $minute = \App\Models\Events::create($array);
                return redirect('dashboard/events')->with('success', request('title') . ' added successfully');
            }
            $this->data['users'] = \App\Models\User::all();
            return view('event.add_event', $this->data);
        }
    
        public function Events() {
            /**
             * add option for someone to write an attendance and upload via excel in case you visit TAMONGSCO 
             */
            $id = request()->segment(3);
            if ((int) $id > 0) {
    
                if ($_POST) {
    
                    $body = request('message');
                    $sms = request('sms');
                    $email = request('email');
                    $events = \App\Models\EventAttendee::where('event_id', $id)->get();
                    $workshop = \App\Models\Events::where('id', $id)->first();
                    foreach ($events as $event) {
                        if ($event->email != '' && (int) $email > 0) {
                            $message = '<h4>Dear ' . $event->name . '</h4>'
                                    . '<h4>I trust this email finds you well.</h4>'
                                    . '<h4>' . $body . '</h4>'
                                    . '<p><br>Looking forward to hearing your contribution in the discussion.</p>'
                                    . '<br>'
                                    . '<p>Thanks and regards,</p>'
                                    . '<p><b>Shulesoft Team</b></p>'
                                    . '<p>Call: +255 655 406 004 </p>';
                            $this->send_email($event->email, 'ShuleSoft Webinar on ' . $workshop->title, $message);
                        }
                        if ($event->phone != '' && (int) $sms > 0) {
                            $message1 = 'Dear ' . $event->name . '.'
                                    . chr(10) . $body
                                    . chr(10)
                                    . chr(10) . 'TAMONGSCO'
                                    . chr(10) . 'Call: '.Auth::User()->phone;
                            $sql = "insert into admin.sms (body,user_id, type,phone_number, sent_from) values ('$message1', 1, '0', '$event->phone', 'phonesms')";
                            $chatId = str_ireplace('+', '', $event->phone) . '@c.us';
                         //   $this->sendMessage($chatId, $message1);
    
                            DB::statement($sql);
                        }
                    }
                    return redirect()->back()->with('success', 'Message Sent Successfully to ' . count($events) . ' Attendees.');
                }
                $this->data['event'] = \App\Models\Events::where('id', $id)->first();
                return view('event.view_event', $this->data);
            }
            $this->data['events'] = \App\Models\Events::orderBy('id', 'DESC')->get();
            return view('event.events', $this->data);
        }
    
        public function deleteUser() {
            $id = request()->segment(3);
            \App\Models\EventAttendee::findOrFail($id)->delete();
            return redirect()->back()->with('success', 'success');
        }

        public function addSchool() {
            if ($_POST) {
                $filename = null;
            
              //  dd(request()->except('_token'));
                $minute = \App\Models\School::create(request()->except('_token'));
                return redirect()->back()->with('success', request('name') . ' added successfully');
            }
            $this->data['regions'] = \App\Models\Region::where('country_id', 1)->get();
            return view('dashboard.add_school', $this->data);
        }
}
