
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-list"></i> panel_title') ?></h3>


        <ol class="breadcrumb">
            <li><a href="<?= url("dashboard/index") ?>"><i class="fa fa-laptop"></i> menu_dashboard') ?></a></li>
            <li><a href="<?= url("characters/index") ?>"></i> menu_characters') ?></a></li>
            <li class="active">menu_edit') ?> menu_characters') ?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-8">
                <form class="form-horizontal" role="form" method="post">

                    <?php
                    if (form_error($errors, 'character_category_id'))
                        echo "<div class='form-group has-error' >";
                    else
                        echo "<div class='form-group' >";
                    ?>
                    <label for="character_category_id" class="col-sm-2 control-label">
                        character_category") ?>
                    </label>
                    <div class="col-sm-6">

                        <select name="character_category_id" id="character_category_id" class="form-control">
                            <option value="<?php echo $characters->character_category_id; ?>"><?php echo $characters->character_category; ?></option>
                            <?php
                            foreach ($character_categories as $character_category) {
                                echo '<option value="' . $character_category->id . '">' . $character_category->character_category . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <span class="col-sm-4 control-label">
<?php echo form_error($errors, 'character_category_id'); ?>
                    </span>
            </div>

            <?php
            if (form_error($errors, 'code'))
                echo "<div class='form-group has-error' >";
            else
                echo "<div class='form-group' >";
            ?>
            <label for="classes" class="col-sm-2 control-label">
character_code") ?>
            </label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="code" name="code" placeholder="character code" value="<?= set_value('code', $characters->code) ?>" >
            </div>
            <span class="col-sm-4 control-label">
<?php echo form_error($errors, 'code'); ?>
            </span>

        </div>

        <?php
        if (form_error($errors, 'description'))
            echo "<div class='form-group has-error' >";
        else
            echo "<div class='form-group' >";
        ?>
        <label for="classes" class="col-sm-2 control-label">
character") ?>
        </label>
        <div class="col-sm-6">
            <input type="text" class="form-control" id="description" name="description" placeholder="character category" value="<?= old('description', $characters->description) ?>" >
        </div>
        <span class="col-sm-4 control-label">
<?php echo form_error($errors, 'description'); ?>
        </span>
    </div>
  <?php 
                        if(form_error($errors,'position')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="classes" class="col-sm-2 control-label">
                            <?=$data->lang->line("position")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="position" name="position" placeholder="<?=$data->lang->line("position")?>" value="<?=old('position',$characters->position)?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error($errors,'position'); ?>
                        </span>
                    </div>
    <?php
    if (form_error($errors, 'class_id'))
        echo "<div class='form-group has-error' >";
    else
        echo "<div class='form-group' >";
    ?>
    <label for="classes" class="col-sm-2 control-label">
        menu_classes") ?>
    </label>
    <div class="col-sm-6">
        <?php
        $array = array("0" => $data->lang->line("all"));
        $classes = \App\Model\Classes::all();
        if (!empty($classes)) {
            foreach ($classes as $class) {
                $array[$class->classesID] = $class->classes;
            }
        }

        $class_ids = \App\Model\CharacterClass::where('character_id', $characters->id)->get(['class_id']);
        $ar = [];
        foreach ($class_ids as $id) {
            array_push($ar, $id->class_id);
        }
        echo form_dropdown("classes[]", $array, old("classes", $ar), "id='classes'  class='form-control select2_multiple' multiple");
        ?>
    </div>
    <span class="col-sm-4 control-label">
<?php echo form_error($errors, 'description'); ?>
    </span>
</div>
<div class="form-group">
    <div class="col-sm-offset-2 col-sm-6">
        <input type="submit" class="btn btn-success btn-block" value="save") ?>" >
    </div>
</div>

<?= csrf_field() ?>
</form>


</div>
</div>
</div>
</div>
