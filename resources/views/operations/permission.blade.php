@extends('layouts.app')
@section('content')

<div class="page-body">
<div class="row">
 <div class="col-sm-12">


        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                <div class="card-header">
    
                <div class="form-group has-success row">
                    <div class="col-sm-5">
                        <label class="col-form-label" for="inputSuccess1">Select user Role
                        </label>
                    </div>
                    <div class="col-sm-7">
                        <select class="form-control"  id='permission'>
                        <option></option>
                        <?php foreach ($user_roles as $u_role) { ?>
                        <option value="<?= $u_role->id ?>" <?= (int) request('id') > 0 && request('id') == $u_role->id ? 'selected' : '' ?> ><?= $u_role->name  ?></option>
                        <?php } ?>
                    </select>
                    </div>
                </div>

                    </div>
                    <div class="card-block accordion-block">
                        <div id="accordion" role="tablist" aria-multiselectable="true">

                        @foreach ($Permissionsgroup as $key => $group)
                            <div class="accordion-panel">
                                <div class="accordion-heading" role="tab" id="headingOne">
                                    <h3 class="card-title accordion-title">
                                    <a class="accordion-msg" data-toggle="collapse" data-parent="#accordion" href="#collapseOne_{{ $group->id }}" aria-expanded="true" aria-controls="collapseOne">
                                        <?= $group->name ?>
                                    </a>
                                </h3>
                                </div>
                                <div id="collapseOne_{{ $group->id }}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">

                                    @foreach($group->permissions()->get() as $sub)
                                    <div class="accordion-content accordion-desc">
                                        <p>
                                            <?php
                                            $check = \App\Models\PermissionRole::where('role_id', $set)->where('permission_id', $sub->id)->first();
                                                    !empty($check) ? $checked = 'checked' : $checked = '';
                                                ?>
                                            <div class="checkbox-fade fade-in-primary">
                                                <label>
                                                <input type="checkbox" {{ $checked }} value="{{ $sub->id }}" id="permission_{{ $sub->id }}" onclick="submit_role(this)">
                                                <span class="cr">
                                                    <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                                                </span>
                                                <span> {{ $sub->description }}</span>
                                            </label>
                                        </div>

                                        
                                        </p>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            @endforeach
                        
                
                        </div>
                    </div>
                </div>
            </div>
        
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-header-text">Single Open Accordion</h5>
                    </div>
                    <div class="card-block accordion-block">
                        <div class="accordion-box" id="single-open">
                            <a class="accordion-msg">Lorem Message 1</a>
                            <div class="accordion-desc">
                                <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has
                                
                                </p>
                            </div>
                        
                        </div>
                    </div>
                </div>
            </div>
        
        </div>







     



  </div>
 </div>
</div>

<script type="text/javascript">

$('#permission').change(function(event) {
    var id = $(this).val();
    if (id === '') {} else {
        window.location.href = '<?= url('users/permissions') ?>/' + id;
    }
});


function submit_role(permission) {
    var perm_id = permission.value;
    var role = '<?=$set?>';
    if(!permission.checked){
        var url_obj = "<?= url('users/removepermission') ?>";
    } else {
        var url_obj = "<?= url('users/storepermission') ?>";
    }
    $.ajax({
        url: url_obj,
        method: 'post',
        data: { "_token": "{{ csrf_token() }}", perm_id: perm_id,role_id: role},
        success: function(data) {
           // alert(data);
        }
    });
}

</script>

@endsection