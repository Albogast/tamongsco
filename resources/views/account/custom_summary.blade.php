@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>
   
<div class="page-body">
    <!-- Server Side Processing table start -->
    <div class="card">
        <div class="card-block">
                
            <form style="" class="form-horizontal" role="form" method="post">
                <div class="row">
                   <div class="col-sm-12 col-xl-3 m-b-30">
                     <h4 class="sub-title">School</h4>
                       <select id="schema" name="schema" class="form-control form-control-primary" required>
                           <option value="" disabled selected>Select school</option>
                           <option value="allSchema">All schools</option>
                               @foreach (load_schemas() as $school)
                                   <option value="{{$school->username}}">{{$school->username}}</option>
                               @endforeach
                       </select>
                   </div>
              
                   <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form1">
                       <h4 class="sub-title">Start Date</h4>
                       <div class=" col-xs-12">
                           <input type="date" required="true" class="form-control calendar" id="from_date" name="from_date" value="" autocomplete="off">
                       </div>
                   </div>
                 
                   <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form">
                       <h4 class="sub-title">End Date</h4>
                       <div class=" col-xs-12">
                           <input type="date" required="true" class="form-control calendar" id="to_date" name="to_date" value="" autocomplete="off">
                       </div>
                   </div>

                   <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form">
                       <h4 class="sub-title"><br> </h4>
                       <div class=" col-xs-12">
                           <input type="submit" class="btn btn-success submit_btn_custom" value="Submit">
                       </div>
                   </div>
                    
                 <?= csrf_field() ?>
               </form>
        </div>
        <div class="col-sm-6  offset-sm-2 list-group">
            <div class="list-group-item">
                <table class="table  nowrap table-md">
                    <thead>
                        <tr>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Transaction Type</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?= $from ?></td>
                            <td><?= $to ?></td>
                            <td><?= $type ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div> <br>
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Expenses</a>
                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Summary</a>
                
                </div>
            </nav>
            <br>
          <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                <div class="table-responsive dt-responsive">
                    <table id="dom-jqry" class="table table-striped table-bordered dataTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Date</th>
                                <th>Payment Method</th>
                                @if($type == 'Expense')
                                    <th>Voucher No</th>
                                @else
                                    <th>Receipt No</th>
                                @endif
                                <th>Amount</th>
                                <th>Bank</th>
                                <th>Transaction ID</th>
                                <th>Recorded By</th>         
                                <th>Date Recorded</th>
                                <th class="">Action</th>
                            </tr>
                        </thead>
                                <!-- body start -->
                                <tbody>
                                    <?php
                                    $total_amount = 0;
                                    $full_paid = 0;
                                    $all_ids = '';
                                    if (isset($transactions) && !empty($transactions)) {
                                        $i = 1;
                                        foreach ($transactions as $transaction) {
                                            ?>
                                            <tr>
                                                <td data-title="Number">
                                                    <?php echo $i; ?>
                                                </td>
                                                <td data-title="student_invoice">
                                                    <?php
                                                    if ($report_type == 1) {
                                                        echo $transaction->expense;
                                                    } else if ($report_type == 2) {
                                                        echo $transaction->student->name;
                                                    } else if ($report_type == 3) {
                                                        echo '';
                                                    }
                                                    ?>

                                                </td>
                                                <td data-title="invoice_role">
                                                    {{-- <?php echo $report_type == 2 ? 'Invoice Payments' : \DB::table($schema_name.'.refer_expense')->where('id',$transaction->refer_expense_id)->first()->name; ?> --}}
                                                </td>

                                                <td data-title="invoice number">
                                                    <?php echo date('d M Y', strtotime($report_type == 3 ? $transaction->date : $transaction->date)); ?>
                                                </td>
                                                <td data-title="invoice amount">
                                                    <?php
                                                    echo $report_type == 2 ? $transaction->paymentType->name :
                                                            $transaction->payment_method;
                                                    ?>
                                                </td>
                                             
                                            @if($type == 'Expense')
                                                <?php   $school = $siteinfos;  ?>                                                                                  
                                                <td><b><?=substr($school->sname,0,2)?>/<?=date('Y', strtotime($transaction->date))?>/<?=$transaction->voucher_no?></b></td>
                                            @else
                                                <td data-title="Receipt No">
                                                    @if($schema_name =='elshaddai.')
                                                    <span  value="text" contenteditable="true" class="form-control mark" payment_id="<?= $transaction->id ?>" student_id="<?= $transaction->student_id ?>"  data-title="" >
                                                    @endif
                
                                                    <?php
                                                    if($schema_name=='elshaddai.'){
                                                                echo $transaction->receipt_no;
                                                    }else{
                                                    echo $transaction->id;     
                                                    }
                                                    ?>
                                                    </span>
                                                </td>
                                            @endif
                                                <td data-title="paid_invoice_amount">
                                                    <?php
                                                    $am = $transaction->amount;
                                                    $total_amount += $am;
                                                    echo money($am);
                                                    ?>
                                                </td>
                                                <td data-title="unpaid_invoice_amount">
                                                    <?php
                                                    $bank = \DB::table($schema.'.bank_accounts')->where('id',$transaction->bank_account_id)->first();
                                                    echo  isset($bank)?$bank->name:'';
                                                    ?>
                                                </td>
                                                <td data-title="invoice_status">
                                                    <?php
                                                    echo $transaction->transaction_id;
                                                    ?>
                                                </td>

                                                <td data-title="actions">
                                                    <?php
                                                    if ($report_type <> 2) {
                                                        $user = DB::table($schema_name.'.users')->where('id', $report_type == 3 ? $transaction->user_id : $transaction->userID)->where('table', $transaction->created_by_table)->first();
                                                        echo !empty($user)? $user->name : $transaction->payer_name;
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    echo date('d M Y h:m', strtotime($transaction->created_at));
                                                    ?>
                                                </td>
                                                @if($type == 'Expense')
                                                <td>
                                                    <?php echo btn_add_pdf('accounts/voucher/'.$schema_name.'/' . $transaction->expenseID.'/', 'Payment Voucher'); ?> 
                                               </td>
                                                @endif
                                                @if($type == 'Revenues') 
                                                <td><a href="{{url('revenue/receipt/' . $transaction->id .'/'.$schema_name. '/')}}" class="btn btn-primary btn-sm">Receipt</a></td>
                                                @endif
                                                @if($type == 'Payments')
                                                    @if($transaction->id)
                                                       <td><a href="{{url('accounts/current_receipt/'.$transaction->id .'/'.$schema_name.'/')}}" class="btn btn-success btn-sm">Receipt</a></td>
                                                    @else
                                                       <td>-</td>
                                                    @endif
                                                @endif
                                            </tr>
                                            <?php
                                            $i++;
                                        }
                                    }
                                    ?>
                                </tbody>
                                <!-- body end -->
                        <tfoot>
                            <tr>
                                <td colspan="6">Total</td>
                                <td><?= money($total_amount) ?></td>
                                <td colspan="5"></td>
                            </tr>
                        </tfoot>
                    </table>
                </div> <!-- End datatable -->
             </div>
             <!-- summary-->
                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <h5> Summary </h5> 
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th> Total </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td></td>
                                <td><?= money($total_amount) ?></td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="row">             
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h6>Transaction By Month</h6>
                                    <div class="clearfix"></div>
                                </div>
                                    <?php
                                    //$insight = new \App\Http\Controllers\Insight();
                                    //$sql_ = 'select sum(amount) as count, to_char(date,\'Mon\')  as month from '.strtolower($type).' where date between \''.$from.'\' and \''.$to.'\' group by to_char(date,\'Mon\') order by EXTRACT(MONTH FROM to_date(to_char(date,\'Mon\'), \'Mon\'))';
                                // echo  $insight->createChartBySql($sql_, 'month', 'Overall Transactions', 'line', false);
                                    ?>
                            </div>
                        </div>
                    </div>
                </div> <!-- End summary -->
        </div>
   </div>

@endsection