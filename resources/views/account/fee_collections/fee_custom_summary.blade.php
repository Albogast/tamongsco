@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>
   
<div class="page-body">
    <!-- Server Side Processing table start -->
    <div class="card">
        <div class="card-block">
            <form style="" class="form-horizontal" role="form" method="post">
                <div class="row">
                    <div class="col-sm-12 col-xl-3 m-b-30">
                          <h4 class="sub-title">School</h4>
                          <select id="schema" name="schema" class="form-control form-control-primary">
                                <option value="" disabled selected>Select school</option>
                                  @foreach (load_schemas() as $school)
                                      <option value="{{$school->username}}">{{$school->username}}</option>
                                  @endforeach
                          </select>
                      </div>
                   
                      <div class="col-sm-12 col-xl-3 m-b-30">
                        <h4 class="sub-title">Fee Name</h4>
                        <select id="schema_fee_id" name="fee_id" class="form-control form-control-primary">
                                
                        </select>
                    </div>
    
                      <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form1">
                        <h4 class="sub-title">Start Date</h4>
                        <div class=" col-xs-12">
                          <input type="date" required="true" class="form-control calendar" id="from_date" name="from_date" value="" autocomplete="off">
                        </div>
                    </div>
                      
                      <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form">
                          <h4 class="sub-title">End Date</h4>
                          <div class=" col-xs-12">
                            <input type="date" required="true" class="form-control calendar" id="to_date" name="to_date" value="" autocomplete="off">
                          </div>
                      </div>
    
                      <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form">
                        <div class=" col-xs-12">
                            <input type="submit" class="btn btn-success submit_btn_custom" value="Submit">
                        </div>
                        </div>
                         
                      <?= csrf_field() ?>
                  </form>
        </div>
        <div class="col-sm-6  offset-sm-2 list-group">
            <div class="list-group-item">
                <table class="table  nowrap table-md">
                    <thead>
                        <tr>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Transaction Type</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?= $from ?></td>
                            <td><?= $to ?></td>
                            <td><?= $type ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div> <br>
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Expenses</a>
                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Summary</a>
                
                </div>
            </nav>
            <br>
          <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                <div class="table-responsive dt-responsive">
                    <table id="dom-jqry" class="table table-striped table-bordered dataTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Date</th>
                                <th>Payment Method</th>
                                <th>Receipt No</th>
                                <th>Amount</th>
                                <th>Bank</th>
                                <th>Transaction ID</th>      
                                <th>Date Recorded</th>
                                <th class="">Action</th>
                            </tr>
                        </thead>
                                <!-- body start -->
                                <tbody>
                                    <?php
                                    $total_amount = 0;
                                    $full_paid = 0;
                                    $all_ids = '';
                                    $i = 1;?>
                                    @if (isset($transactions) && !empty($transactions))
                                       
                                        @foreach ($transactions as $transaction)
                                        
                                            <tr>
                                                <td data-title="Number">
                                                    <?php echo $i; ?>
                                                </td>
                                                <td data-title="student_invoice">
                                                    <?php
                                                     $student = \DB::table($schema_name.'.student')->where('student_id',$transaction->student_id)->first();
                                                      echo isset($student)? $student->name : '';
                                                    ?>                                        
                                                </td>
                                                <td data-title="invoice_rol">
                                                    <?php echo 'Fee Payments'; ?>
                                                </td>

                                                <td data-title="transaction_date">
                                                    <?php echo date('d M Y', strtotime($transaction->date)); ?>
                                                </td>
                                                <td data-title="invoice amount">
                                                    <?php 
                                                       echo isset($transaction->name)? $transaction->name:'';
                                                    ?>
                                                </td>
                                        
                                                <td data-title="Receipt No">
                                                    @if($schema_name =='elshaddai.')
                                                    <span  value="text" contenteditable="true" class="form-control mark" payment_id="<?= $transaction->id ?>" student_id="<?= $transaction->student_id ?>"  data-title="" >
                                                    @endif
                
                                                    <?php
                                                    if($schema_name=='elshaddai.'){
                                                                echo $transaction->receipt_no;
                                                    }else{
                                                    echo $transaction->payment_id;     
                                                    }
                                                    ?>
                                                    </span>
                                                </td>
                                    
                                                <td data-title="paid_invoice_amount">
                                                    <?php
                                                        $am = $transaction->amount;
                                                        $total_amount += $am;
                                                        echo money($am);
                                                    ?>
                                                </td>
                                                <td data-title="unpaid_invoice_amount">
                                                    <?php
                                                    $bank = \DB::table($schema_name.'.bank_accounts')->where('id',$transaction->bank_account_id)->first();
                                                    echo  isset($bank)?$bank->name:'';
                                                    ?>
                                                </td>
                                                <td data-title="invoice_status">
                                                    <?php
                                                        $payment_details = \DB::table($schema_name.'.payments')->where('id',$transaction->payment_id)->first();  
                                                        echo isset($payment_details->transaction_id)?$payment_details->transaction_id:'';
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                        echo date('d M Y h:m', strtotime($payment_details->created_at));
                                                    ?>
                                                </td>
                                                <td><a href="<?= url('invoices/current_receipt/'.$transaction->payment_id)?>" class="btn btn-success btn-sm">Receipt</a></td>
                                            </tr>
                                            <?php
                                            $i++; ?>
                                        @endforeach
                                    @endif
                                    
                                </tbody>
                                <!-- body end -->
                        <tfoot>
                            <tr>
                                <td colspan="6">Total</td>
                                <td><?= money($total_amount) ?></td>
                                <td colspan="5"></td>
                            </tr>
                        </tfoot>
                    </table>
                </div> <!-- End datatable -->
             </div>
             <!-- summary-->
                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <h5> Summary </h5> 
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th> Total </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td></td>
                                <td><?= money($total_amount) ?></td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="row">             
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h6>Transaction By Month</h6>
                                    <div class="clearfix"></div>
                                </div>
                                    <?php
                                    //$insight = new \App\Http\Controllers\Insight();
                                    //$sql_ = 'select sum(amount) as count, to_char(date,\'Mon\')  as month from '.strtolower($type).' where date between \''.$from.'\' and \''.$to.'\' group by to_char(date,\'Mon\') order by EXTRACT(MONTH FROM to_date(to_char(date,\'Mon\'), \'Mon\'))';
                                // echo  $insight->createChartBySql($sql_, 'month', 'Overall Transactions', 'line', false);
                                    ?>
                            </div>
                        </div>
                    </div>
                </div> <!-- End summary -->
        </div>
   </div>

   <script type="text/javascript">
    $('#schema').change(function (event) {
    var schema_name = $(this).val();
    if (schema_name === 'allSchema') {
   
    } else {
        $.ajax({ 
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '<?= url('Accounts/getFees') ?>',
            data: {schema_name: schema_name},
            dataType: "html", 
            cache: false,
            success: function (data) { 
            $('#schema_fee_id').html(data);
            }
        });

    }
});
</script>

@endsection