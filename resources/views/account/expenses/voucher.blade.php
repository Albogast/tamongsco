@extends('layouts.app')
@section('content')
<div class="x_content" >
    <div class="well"  id="well">
        <div class="row">
            <div class="col-sm-12">
                <button class="btn btn-xs btn-success" onclick="javascript:printDiv('printablediv')"><span class="fa fa-print">Print </button>
                <a class="btn btn-xs btn-danger" href="{{url('expense/index/' . isset($voucher->refer_expense_id) . "/")}}"><i class="fa fa-edit"></i> Return Back</a>
            </div>
        </div>
    </div>
    <div class="row">
            <?php
            /**
             * @author Ephraim Swilla <ephraim@inetstz.com>
             */
            $school = $siteinfos;
            ?>
                <?php if (isset($voucher) && !empty($voucher)) { ?>
                    <div id="printablediv">
                    <div class="col-sm-12" style=" min-width: 100%; padding-left: 10%; padding-right: 10%;">                    
                     <?php
                        $array = array(
                            "src" => url('storage/uploads/images/' . $siteinfos->photo),
                            'width' => '126em',
                            'height' => '126em',
                            'class' => 'img-rounded',
                            'style' => 'margin-left:2em'
                        );
                        ?>
                        <div class="">
                            <table class=" table-striped center" style="margin: 1px 2px 1px 0px;">
                                <thead>
                                    <tr>
                                        <th class="col-md-2" style="padding-bottom:0px">
                                            <?= img($array); ?>
                                        </th>
                                        <th class="col-md-8 text-center letterhead" style="margin: 1% 0 0 16%; padding-top: 2%; ">

                                            <h2 style="font-weight: bolder !important; font-size: 24px; text-transform: uppercase;"><?= $siteinfos->sname ?></h2>
                                            <h4><?= 'P.O. BOX ' . $siteinfos->box . ', ' . ucfirst($siteinfos->address) ?></h4>
                                            <h3>Phone : <?= str_replace(',', ' / ', $siteinfos->phone) ?></h3>
                                            <h3>Email :<?= $siteinfos->email ?></h3>
                                            <h3>Website : <?= $siteinfos->website ?></h3>
                                        </th>
                                        <th class="col-md-2" style="padding-bottom:0px"></th>

                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <hr/>

                        <div class="row text-center">
                            <div class="table-responsive">
                                <table class="table">
                                    <tr>
                                    <h1 align="centre" style="text-decoration: underline"><?= strtoupper(isset($voucher->payment_method) ? $voucher->payment_method : '' ) . '  ' ?> PAYMENT VOUCHER</h1>
                                </tr>
                                </table>
                            </div>
                        </div>
                        <div  class="row">

                            <div class="table-responsive">
                                <table class="table" style="width: 100%">
                                    <thead>
                                        <tr style="font-size: 20px;">
                                        <?php if($schema_name == 'elshaddai.'){  ?>
                                            <th>Voucher No:&nbsp;&nbsp;&nbsp;<b><?= date('m', strtotime($voucher->date))?>/<?= $voucher->voucher > 9 ? $voucher->voucher : '0'.$voucher->voucher ?> of <?= date('Y', strtotime($voucher->date))?></b></th>  <th style="text-align: right;">Date: &nbsp;&nbsp;&nbsp;<b><?= date("d/m/Y", strtotime($voucher->date)); ?></b></th>
                                        <?php }else{  ?>
                                                <th>Voucher No: &nbsp;&nbsp;&nbsp;<b><?= substr($school->sname, 0, 2) ?>/<?= date('Y', strtotime($voucher->date)) ?>/<?= $voucher->voucher_no ?></b></th> <th style="text-align: right;">Date: &nbsp;&nbsp;&nbsp; Date:<b><?= $voucher->date; ?></b></th>
                                            <?php } ?>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>

                        <?php if (isset($productexpenses) && count($productexpenses)>0) { ?>
                            <div class="row text-center">
                                    <p>1.Pay the under mentioned Amount to</p>
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <th>No.</th>
                                                <th>Item Name</th>
                                                <th>Quantity</th>
                                                <th>Sub-amount</th>
                                            </tr>
                                            <?php
                                            $i = 1;
                                            $sum = 0;
                                           
                                            foreach ($productexpenses as $cart) {
                                                $sum += $cart->amount;
                                                ?>
                                                <tr>
                                                    <th><?= $i++ ?></th>
                                                    <th><?= $cart->productQuantity->name ?></th>
                                                    <th><?php echo $cart->quantity . ' - ' . $cart->productQuantity->metric->abbreviation; ?></th>
                                                    <th><?= money($cart->amount) ?></th>
                                                </tr>
                                                <?php } ?>
                                                 <tr>
                                                    <th colspan="3">Payment Details for - <?php echo $cart->productQuantity->referExpense->name; ?> <span style="float: right">Total Amount</span></th>
                                                    <th><?= money($sum) ?>/=</th>
                                                </tr>

                                                <tr style="text-transform: uppercase ; text-align:center;">
                                                    <td colspan="4">Amount In Words:<?= number_to_words($sum) ?> only.</td>
                                                </tr>
                                            <thead>
                                                <tr>
                                                    <th colspan="4">Name of the Recipient/Vendor - <b> <?= $cart->vendor->name ?></b> (<?= $cart->vendor->phone_number ?>) <br>Address: -  </th>
                                                </tr>
                                            </thead>
                                            <tr>
                                                <th>Note:</th>
                                                <th colspan="3"><?= $cart->expenses->note ?></th>
                                            </tr>                         
                                 </tbody>
                            </table>
                         </div>
                    <?php } else {
                            ?>
                            <div class="row text-center">
                                <div class="col-lg-12 table">
                                    <p style="float: left;" >1.Pay the under mentioned Amount to</p>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th colspan="4">Name of the Recipient:<b><?= $voucher->recipient ?></b><br>
                                                    Address:</th>
                                            </tr>
                                        </thead>
                                        <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th style="width: 59%">Payment Details for <b><?= strtoupper(isset($voucher->refer_expense_id) ? \DB::table($schema_name.'.refer_expense')->where('id',$voucher->refer_expense_id)->first()->name : '' ) . '  ' ?></b></th>
                                                <th>Subtotal</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td><?= $voucher->note ?></td>
                                                <td><?= money($voucher->amount) ?></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3"><center>Amount In Words:- <?php echo strtoupper(number_to_words($voucher->amount)) . ' ONLY'; ?></center></td>
                                         </tr>\
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.col -->
                            </div>
                        <?php } ?>
                        <div class="row">

                            <p>2.Prepared By: </p>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Name:&nbsp;<b><?= $voucher->payer_name ?></b><br>
                                        </th>
                                        <th>Signature:-------------------------------------</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                        <div class="row">
                            <p>3. I certify that the above mentioned amount is correct and I authorise payment</p>
                            <table class="table" border="0" cellspacing="0" cellpadding="0">
                                <thead>
                                    <tr>
                                        <th>Name of Officer
                                        </th>
                                        <th>Designation</th>
                                        <th>Signature</th>
                                        <th>Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr><td>-------------------------------</td>
                                        <td>----------------------------</td>
                                        <td>------------------------</td>
                                        <td>-------/--------/-----------------</td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                            <?php if ($schema_name == 'stpeterclaver.') { ?>

                            <div class="row">

                                <p>4. I Approve that the above mentioned amount is correct and I authorise payment</p>
                                <table class="table" border="0" cellspacing="0" cellpadding="0">
                                    <thead>
                                        <tr>
                                            <th>Name of Officer
                                            </th>
                                            <th>Designation</th>
                                            <th>Signature</th>
                                            <th>Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>-----------------------------</td>
                                            <td>--------------------------</td>
                                            <td>-------------------------</td>
                                            <td>---------/---------/--------------</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">

                                <p>5. Received the amount stated above</p>
                                <table class="table" border="0" cellspacing="0" cellpadding="0">
                                    <thead>
                                        <tr>

                                            <th>---------------------------</th>
                                            <th >-----------------------------</th>


                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Signature</td>
                                            <td>Date</td>
                                        </tr>
                                    </tbody>
                                </table>


                            </div>
                            <?php } else { ?>
                            <div class="row">
                                <p>4. Received the amount stated above</p>
                                <table class="table" border="0" cellspacing="0" cellpadding="0">
                                    <thead>
                                        <tr>
                                            <th>---------------------------</th>
                                            <th >-----------------------------</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Signature</td>
                                            <td>Date</td>
                                        </tr>
                                    </tbody>
                                </table>
                                </div>
                    <?php } ?>
                    </div>
                </div>
            </div>
            </div>
                    <?php
                } else {


                    echo'There is an issue in this receipt';
                }
                ?>

                <br><br>
            </div>
        </div>

        <!-- /MAIL LIST -->
    </div>
</div>
<script language="javascript" type="text/javascript">
    function printDiv(divID) {
        if (document.all) {
            document.all.well.style.display = 'none';
            document.all.topnav.style.display = 'none';
            window.print();
            document.all.well.style.visibility = 'visible';
            document.all.topnav.style.visibility = 'visible';
        } else {
            document.getElementById('well').style.display = 'none';
            document.getElementById('topnav').style.display = 'none';
            window.print();
            $('#well,#topnav').show();
        }
    }
</script>
@endsection
