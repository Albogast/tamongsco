
@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>

<div class="box">
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">

                    {{-- <a class="btn btn-success" href="<?php echo url('inventory/add') ?>">
                        <i class="fa fa-plus"></i> 
                        Add New Item
                    </a> --}}
                    <br>
                    <br>
                <div class="col-sm-12">

                    <div class="nav-tabs-custom">

                        <div class="tab-content">
                            <div id="all" class="tab-pane active">
                                <div id="hide-table">
                                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                                        <thead>
                                            <tr>
                                                <th class="col-sm-1">#</th>
                                                <th class="col-sm-2">Item Name</th>
                                                <th class="col-sm-1">Category</th>
                                                <th class="col-sm-1">Alert Quantity</th>
                                                <th class="col-sm-2">Remained Quantity</th>
                                                <th class="col-sm-2">Account Group</th>
                                                <th class="col-sm-2">Description</th>
                                                <th class="col-sm-2">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (!empty($items)) {
                                                $i = 1;
                                                foreach ($items as $item) {
                                                    ?>
                                                    <tr>
                                                        <td data-title="#">
                                                            <?php echo $i; ?>
                                                        </td>

                                                        <td data-title="Name">
                                                            <?php
                                                            echo $item->name
                                                            ?>
                                                        </td>
                                                        <td data-title="Category">
                                                            <?php
                                                            echo $item->category
                                                            ?>
                                                        </td>
                                                        <td data-title="alert quantity">
                                                            <?php echo $item->alert_quantity . ' - ' . $item->metrics; ?>
                                                        </td>
                                                        <td data-title="Remain quantity">
                                                            <?php echo $item->remain_quantity . ' - ' . $item->metrics; ?>
                                                        </td>  
                                                        <td data-title="Account group">
                                                            <?php echo $item->inventory; ?>
                                                        </td> 

                                                        <td data-title="Description">
                                                            <?php echo $item->note; ?>
                                                        </td>
                                                        <td data-title="Action">
                                                            {{-- <?php
                                                            echo btn_edit('inventory/edit_product/' . $item->id . "/" . $set, 'Edit');
                                                            echo btn_delete('inventory/delete/' . $item->id . "/" . $set, 'Delete');
                                                            ?> --}}
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $i++;
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div> <!-- nav-tabs-custom -->
                </div>

            </div> <!-- col-sm-12 -->
        </div><!-- row -->
    </div><!-- Body -->
</div><!-- /.box -->
@endsection
<script type="text/javascript">
    $('#classesID').change(function () {
        var classesID = $(this).val();
        if (classesID == 0) {
            $('#hide-table').hide();
            $('.nav-tabs-custom').hide();
        } else {
            $.ajax({
                type: 'POST',
                url: "<?= url('student/student_list') ?>",
                data: "id=" + classesID,
                dataType: "html",
                success: function (data) {
                    window.location.href = data;
                }
            });
        }
    });
</script>