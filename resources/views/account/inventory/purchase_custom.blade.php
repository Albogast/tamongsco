@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>
<div class="box">
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                {{-- <h5>
                    <a class="btn btn-success" href="<?php echo url('inventory/add_purchase') ?>">
                        <i class="fa fa-plus"></i> 
                       Add purchase
                    </a>
                </h5> --}}
                <br><br>

                <div class="col-sm-12">
                    <form style="" class="form-horizontal" role="form" method="post">
                        <div class="row">
                            <div class="col-sm-12 col-xl-3 m-b-30">
                                  <h4 class="sub-title">School</h4>
                                  <select id="schema" name="schema" class="form-control form-control-primary">
                                      <option value="" disabled selected>Select school</option>
                                          @foreach (load_schemas() as $school)
                                              <option value="{{$school->username}}">{{$school->username}}</option>
                                          @endforeach
                                  </select>
                              </div>
                           
                              <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form1">
                                  <h4 class="sub-title">Start Date</h4>
                                  <div class=" col-xs-12">
                                    <input type="date" required="true" class="form-control calendar" id="from_date" name="from_date" value="" autocomplete="off">
                                  </div>
                              </div>
                              
                              <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form">
                                  <h4 class="sub-title">End Date</h4>
                                  <div class=" col-xs-12">
                                    <input type="date" required="true" class="form-control calendar" id="to_date" name="to_date" value="" autocomplete="off">
                                  </div>
                              </div>
            
                              <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form">
                                <h4 class="sub-title"><br> </h4>
                                <div class=" col-xs-12">
                                    <input type="submit" class="btn btn-success submit_btn_custom" value="Submit">
                                </div>
                                </div>
                                 
                              <?= csrf_field() ?>
                          </form>
                          <div class="table-responsive dt-responsive">
                            <table id="dom-jqry" class="table table-striped table-bordered dataTable">
                                        <thead>
                                            <tr>
                                                <th class="col-sm-1">#</th>
                                                <th class="col-sm-2">Item Name</th>
                                                <th class="col-sm-1">Quantity</th>
                                                <th class="col-sm-1">Total</th>
                                                <th class="col-sm-2">Amount</th>
                                                {{-- <th class="col-sm-1">Action</th>            --}}
                                            </tr>
                                        </thead>
                                        <?php
                                        if (isset($items) && sizeof($items) >0) {
                                             ?>
                                        <tbody>
                                            <?php
                                                $i = 1;
                                                $amounts = 0;
                                                foreach ($items as $item) {
                                                    $amounts += $item->amount;
                                                    ?>
                                                    <tr>
                                                        <td data-title="#">
                                                            <?php echo $i; ?>
                                                        </td>

                                                        <td data-title="Name">
                                                            <?php 
                                                            echo $item->name;
                                                            ?>
                                                        </td>

                                                        <td data-title="Quantity">
                                                            <?php echo $item->quantity; ?>
                                                        </td>                     
                                                        <td data-title="Total">
                                                        <?php
                                                             echo $item->total; 
                                                        ?>
                                                        </td>
                                                        <td data-title="Amount">
                                                            <?php
                                                            if(isset($item->amount)){
                                                            echo money($item->amount);
                                                        }
                                                            ?>
                                                        </td>  
                                                        
                                                        {{-- <td data-title="Action">
                                                            <?php
                                                                echo btn_view('inventory/purchase/' . $item->product_alert_id.'/'.$schema_name, 'View');
                                                            
                                                            ?>
                                                        </td> --}}
                                                    </tr>
                                                    <?php
                                                    $i++;
                                                }
                                            ?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                        <td></td>
                                        <td colspan="3">Total</td>
                                        <td  colspan="2"><?php echo money($amounts); ?></td>
                                        </tr>
                                        <tfoot>
                                            <?php } ?>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div> 
                </div>
</div><!-- /.box -->
@endsection
