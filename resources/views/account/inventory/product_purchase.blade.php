@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>
<div class="page-body">
    <!-- Server Side Processing table start -->
    <div class="card">
        <div class="card-block">
            <form style="" class="form-horizontal" role="form" method="post">
                <div class="row">
                    <div class="col-sm-12 col-xl-3 m-b-30">
                          <h4 class="sub-title">School</h4>
                          <select id="schema" name="schema" class="form-control form-control-primary">
                                <option value="" disabled selected>Select school</option>
                                  @foreach (load_schemas() as $school)
                                      <option value="{{$school->username}}">{{$school->username}}</option>
                                  @endforeach
                          </select>
                      </div>
    
                      <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form1">
                        <h4 class="sub-title">Start Date</h4>
                        <div class=" col-xs-12">
                          <input type="date" required="true" class="form-control calendar" id="from_date" name="from_date" value="" autocomplete="off">
                        </div>
                    </div>
                      
                      <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form">
                          <h4 class="sub-title">End Date</h4>
                          <div class=" col-xs-12">
                            <input type="date" required="true" class="form-control calendar" id="to_date" name="to_date" value="" autocomplete="off">
                          </div>
                      </div>
    
                      <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form">
                        <div class=" col-xs-12">
                            <input type="submit" class="btn btn-success submit_btn_custom" value="Submit">
                        </div>
                        </div>
                         
                      <?= csrf_field() ?>
                  </form>
        </div>
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Purchase</a>
               
            </nav>
            <br>
          <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                <div class="table-responsive dt-responsive">
                    <table id="dom-jqry" class="table table-striped table-bordered dataTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Date</th>
                                <th>Payment Method</th>
                                <th>Receipt No</th>
                                <th>Amount</th>
                                <th>Bank</th>
                                <th>Transaction ID</th>      
                                <th>Date Recorded</th>
                                {{-- <th class="">Action</th> --}}
                            </tr>
                        </thead>
                                <!-- body start -->
                                <tbody>
                                    <?php
                                        $qnt = 0;
                                        $amounts = 0;
                                  
                                    if (isset($items) && sizeof($items) >0) {
                                        $i = 1;
                                        foreach ($items as $item) {
                                            ?>
                                            <tr>
                                                <td data-title="#">
                                                    <?php echo $i; ?>
                                                </td>

                                                <td data-title="Item Name">
                                                    <?php 
                                                    if(isset($item->name)){
                                                    echo $item->name;
                                                    }
                                                    ?>
                                                </td>
                                                <td data-title="Vendor">
                                                     <?php
                                                     $schema_name = request()->segment(4);
                                                        $vendor = \DB::table($schema_name.'.vendors as a')->where('a.id',$item->vendor_id)->first();
                                                        echo isset($vendor->name)?$vendor->name:'';
                                                    ?> 
                                                </td>
                                            
                                                <td data-title="Quantity">
                                                    <?php 
                                                    echo $item->quantity;
                                                    $qnt += $item->quantity;
                                                     ?>
                                                </td>                     
                                                <td data-title="Amount">
                                                    <?php
                                                    if(isset($item->amount)){
                                                    echo money($item->amount);
                                                    $amounts += $item->amount;
                                                }
                                                    ?>
                                                </td>  
                                                <td data-title="Payer Name">
                                                <?php   
                                                // if(isset($item->expenses->paymentType->name)){   
                                                //     echo $item->expenses->paymentType->name;
                                                //  }
                                                  ?>
                                                </td>

                                                <td data-title="Transaction Id">
                                                    <?php 
                                                    // if(isset($item->expenses->transaction_id)){   
                                                    // echo $item->expenses->transaction_id; 
                                                    // }
                                                    ?>
                                                </td>
                                                <td data-title="Date">
                                                <?php if(isset($item->date)){  
                                                     echo $item->date; 
                                                }
                                                ?>
                                                </td>
                                                {{-- <td data-title="Action">
                                                    <?php
                                                        echo btn_edit('inventory/edit_inventory/' . $item->expense_id, 'Edit');
                                                        echo btn_delete('inventory/delete_inventory/' . $item->id, 'Delete');
                                                    ?>
                                                </td> --}}
                                            </tr>
                                <?php
                                        $i++;
                                        }
                                    }
                                    ?>
                                </tbody>
                                <!-- body end -->
                        <tfoot>
                            <tr>
                                <td colspan="6">Total</td>
                                <td><?= money($amounts) ?></td>
                                <td colspan="5"></td>
                            </tr>
                        </tfoot>
                    </table>
                </div> <!-- End datatable -->
             </div>
        </div>
   </div>

@endsection