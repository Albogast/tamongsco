
@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>
<div class="page-body">
    <div class="card">
        <div class="card-block">
                    <table id="dom-jqry" class="table table-striped table-bordered dataTable">
                        <thead>
                            <tr>
                                <th class="col-lg-2">#</th>
                                <th class="col-lg-2">Vendor Name</th>
                                 <th class="col-lg-2">Phone Number</th>
                                <th class="col-lg-2">Email</th>
                                <th class="col-lg-2">Location</th>
                                 <th class="col-lg-2">Status</th>
                                {{-- <th class="col-lg-2">Action</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($vendors)) {$i = 1; foreach($vendors as $vendor) { ?>
                                <tr>
                                    <td data-title="#">
                                        <?php echo $i; ?>
                                    </td>
                                    <td data-title="Name">
                                        <?php echo $vendor->name; ?>
                                    </td>
                                     <td data-title="phone">
                                        <?php echo $vendor->phone_number; ?>
                                    </td>
                                    <td data-title="email">
                                        <?php echo $vendor->email; ?>
                                    </td>
                                   
                                    <td data-title="location">
                                        <?php echo $vendor->location; ?>
                                    </td>
                                     <td data-title="status">
                                        <?php echo $vendor->status==1 ? '<b class="label label-success">Verified</b>':'<b class="label label-default">Not Verified</b>'; ?>
                                    </td>
                                    {{-- <td data-title="Action">
					                    <?php echo btn_view('vendor/show/'.$vendor->id,'View') ?>
                                        <?php echo btn_edit('vendor/edit/'.$vendor->id, 'Edit') ?>
                                        <?php echo ((int) $vendor->status==0) ? btn_delete('vendor/delete/'.$vendor->id,'Delete'):''; ?>
                                    </td> --}}
                                </tr>
                            <?php $i++; }} ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
@endsection