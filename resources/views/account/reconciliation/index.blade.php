@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>
   
<div class="page-body">
    <!-- Server Side Processing table start -->
    <div class="card">
      <div class="card-block">

          <form style="" class="form-horizontal" role="form" method="post">
            <div class="row">
                <div class="col-sm-12 col-xl-3 m-b-30">
                      <h4 class="sub-title">School</h4>
                      <select id="schema" name="schema" class="form-control form-control-primary">
                            <option value="" disabled selected>Select school</option>
                              @foreach (load_schemas() as $school)
                                  <option value="{{$school->username}}">{{$school->username}}</option>
                              @endforeach
                      </select>
                  </div>
               
                  <div class="col-sm-12 col-xl-3 m-b-30">
                    <h4 class="sub-title">Bank Name</h4>
                    <select id="schema_bank_id" name="bank_id" class="form-control form-control-primary">
                            
                    </select>
                 </div>

                  <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form1">
                    <h4 class="sub-title">From Date</h4>
                    <div class=" col-xs-12">
                      <input type="date" required="true" class="form-control calendar" id="from" name="from" value="" autocomplete="off">
                    </div>
                </div>
                  
                  <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form">
                      <h4 class="sub-title">To Date</h4>
                      <div class=" col-xs-12">
                        <input type="date" required="true" class="form-control calendar" id="to" name="to" value="" autocomplete="off">
                      </div>
                  </div>
                  
                  <div class="col-sm-12 col-xl-3 m-b-30">
                    <h4 class="sub-title">Transaction Type</h4>
                    <select name="method" class="form-control">
                      <option selected disabled>Select Transaction</option>
                      <option value="All">All</option>
                      <option value="revenue">Revenue</option>
                      <option value="payment">Payments (From Invoice) </option>
                      <option value="current_assets">Current Assets</option>
                      <option value="liability">Liability</option>
                      <option value="capital">Capital</option>
                      <option value="fixed_assets">Fixed Assets</option>
                    </select> 
                 </div>
                  <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form">
                    <div class=" col-xs-12">
                        <input type="submit" class="btn btn-success submit_btn_custom" value="Submit">
                    </div>
                    </div>
                     
                  <?= csrf_field() ?>
              </form>
          </div>

            <script type="text/javascript">
                    $('#schema').change(function (event) {
                    var schema_name = $(this).val();
                    if (schema_name === 'allSchema') {
                   
                    } else {
                        $.ajax({ 
                            type: 'POST',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: '<?= url('paymentController/getSchemaBanks') ?>',
                            data: {schema_name: schema_name},
                            dataType: "html", 
                            cache: false,
                            success: function (data) { 
                            $('#schema_bank_id').html(data);
                            }
                        });
            
                    }
                });
            </script>

@endsection