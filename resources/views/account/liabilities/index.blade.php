@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>
   
<div class="page-body">
    <!-- Server Side Processing table start -->
    <div class="card">
        <div class="card-block">

        <form style="" class="form-horizontal" role="form" method="post">
            <div class="row">
                <div class="col-sm-12 col-xl-4 m-b-30">
                      <h4 class="sub-title">School</h4>
                      <select id="schema" class="form-control form-control-primary">
                          <option value="">Select school</option>
                              
                      </select>
                  </div>
               
                  <div class="col-sm-12 col-xl-4 m-b-30" id="hide-form1">
                      <h4 class="sub-title">Start Date</h4>
                      <select name="select" id="academic_year_id1" class="form-control form-control-primary">

                      </select>
                  </div>
                  
                  <div class="col-sm-12 col-xl-4 m-b-30" id="hide-form">
                      <h4 class="sub-title">End Date</h4>
                      <select name="select" id="academic_year_id" class="form-control form-control-primary">
                         
                      </select>
                  </div>
                  
                  <?= csrf_field() ?>
              </form>
            </div>

            <script type="text/javascript">
                $('#schema').change(function (event) {
                     var schema = $(this).val();
                     if (schema === 'All') {
                         window.location.href = "<?= url('Users/all_students/all') ?>";
                       } else {
                         $.ajax({ 
                             type: 'POST',
                             headers: {
                                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                             },
                             url: '<?= url('Users/getClasses') ?>',
                             data: {schema: schema},
                             dataType: "html", 
                             cache: false,
                             success: function (data) { 
                                $('#classes_id').html(data);
                             }
                         });
             
                     }
                 });
         
                 $('#classes_id').change(function(event) {
                 var schema_name = $('#schema').val();
                 var classesID = $(this).val();
                 if (classesID === '0') {
                     $('#academic_year_id').val(0);
                     $('#report_filter_div').hide();
                 } else {
                     $('#report_filter_div').show();
                     $.ajax({
                         type: 'POST',
                         url: "<?= url('exam/get_academic_years') ?>",
                         data: "_token=" + "{{ csrf_token() }}" + "&id=" + classesID + "&schema_name=" + schema_name,
                         dataType: "html",
                         success: function(data) {
                             $('#academic_year_id').html(data);
                         }
                     });
                 }
             });
         
                 $('#academic_year_id').change(function () {
                 var academic_year_id = $(this).val();
                 var schema = $('#schema').val();
                 var classes_id = $('#classes_id').val();
                 if (classes_id == 0 || academic_year_id == 0) {
                     // $('#hide-table').hide();
                     // $('.nav-tabs-custom').hide();
                 } else {
                     window.location.href = "<?= url('Users/student') ?>/" + schema + '/' + academic_year_id + '/' + classes_id;
                 }
             });
         
             </script>
         

@endsection