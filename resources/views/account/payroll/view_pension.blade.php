@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>
   
<div class="page-body">
    <!-- Server Side Processing table start -->
    <div class="card">
        <div class="card-block">
                
            <form style="" class="form-horizontal" role="form" method="post">
                <div class="row">
                   <div class="col-sm-12 col-xl-3 m-b-30">
                     <h4 class="sub-title">School</h4>
                       <select id="schema" name="schema" class="form-control form-control-primary" required>
                           <option value="" disabled selected>Select school</option>
                               @foreach (load_schemas() as $school)
                                   <option value="{{$school->username}}">{{$school->username}}</option>
                               @endforeach
                       </select>
                   </div>
            
                   <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form">
                       <h4 class="sub-title"><br> </h4>
                       <div class=" col-xs-12">
                           <input type="submit" class="btn btn-success submit_btn_custom" value="Submit">
                       </div>
                   </div>
                    
                 <?= csrf_field() ?>
               </form>
        </div>
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">{{$schema_name}}</a>
                {{-- <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Summary</a> --}}
                
                </div>
            </nav>
            <br>
          <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                <div class="table-responsive dt-responsive">
                    <table id="dom-jqry" class="table table-striped table-bordered dataTable">
                        <thead>
                            <tr>
                                <th class="col-sm-2">#</th>
                                <th class="col-sm-2">Name</th>
                                <th class="col-sm-2">User type</th>
                                <th class="col-sm-2">Email</th>
                                <th class="col-sm-2">Phone Number</th>
                                <?php if ($type == 'pension') { ?>
                                    <th class="col-sm-2">National ID/Check  Number</th>
                                <?php } ?>
                                <?php if ($type == 'allowance' || $type == 'deduction') { ?>
                                    <th class="col-sm-2"><?= (bool) $allowance->is_percentage == false ? 'Amount' : 'Percent'?></th>
                                    <?php
                                }
                                if ($type == 'deduction') {
                                    ?>
                                    <th class="col-sm-2"><?= (bool) $allowance->is_percentage == false ? 'Employer Amount' : 'Employer Percent' ?></th>
                                <?php } ?> 
                                <th class="col-sm-2"><?='Action' ?></th>
                            </tr>
                        </thead>
                                <!-- body start -->
                                <tbody>
                                    <?php
                                    if (!empty($users)) {    
                                        $i = 1;
                                        $allowance_total_amount = 0;
                                        $deduction_total_amount = 0;
                                        foreach ($users as $user) {
                                            $arr = array(
                                                'user_id' => $user->id,
                                                'table' => $user->table
                                            );
                                            ?>
                                            <tr id="std<?= $user->id; ?>">
                                                <td data-title="slno">
                                                    <?php echo $i; ?>
                                                </td>                                                                                                                                                                <!--<td data-title="promotion_photo') ?>">-->
                                           
                                                <td data-title="student_name">
                                                    <?php echo $user->name; ?>
                                                </td>
                                                <td data-title="student_roll">
                                                    <?php echo $user->usertype; ?>
                                                </td>
                                                <td data-title="student_section">
                                                    <?php echo $user->email; ?>
                                                </td>
                                                <td data-title="student_section">
                                                    <?php echo $user->phone; ?>
                                                </td>
                                                <?php if ($type == 'allowance' || $type == 'deduction') { ?>
                                                    <td data-title="amount">
                                                        <?php
                                                        if ($type == 'allowance') {
                                                            $user_allowance = \App\Model\UserAllowance::where('user_id', $user->id)->where('table', $user->table)->where('allowance_id', $set)->first();
                                                            
                                                            $amount = (bool) $allowance->is_percentage == false ?
                                                                    (!empty($user_allowance) && $user_allowance->amount > 0 ? $user_allowance->amount : $allowance->amount) :
                                                                    (!empty($user_allowance)  && (int) $user_allowance->percent > 0 ? $user_allowance->percent : $allowance->percent);
                                                         
                                                           $user_amount= isset($user_allowance->amount)?$user_allowance->amount:0 ;
                                                           $allowance_total_amount  += $user_amount;
                                                                             
                                                            $deadline = !empty($user_allowance)? $user_allowance->deadline : '';
                                                        } else if ($type == 'deduction') {
                                                        
                                                            $user_deduction = \App\Model\UserDeduction::where('user_id', $user->id)->where('table', $user->table)->where('deduction_id', $set)->first();
                                                           
                                                            $amount = (bool) $allowance->is_percentage  == false ?
                                                                    (!empty($user_deduction)  && $user_deduction->amount > 0 ? $user_deduction->amount : $allowance->amount) :
                                                                    (!empty($user_deduction)  && (int) $user_deduction->percent > 0 ? $user_deduction->percent : $allowance->percent);
                                                                  
            
                                                           $user_deduction_amount = isset($user_deduction->amount)? $user_deduction->amount:0;
                                                           $deduction_total_amount += $user_deduction_amount;
                                                            $employer_amount = (bool) $allowance->is_percentage == false ?
                                                                    (!empty($user_deduction)  && $user_deduction->amount > 0 ? $user_deduction->employer_amount : $allowance->employer_amount) :
                                                                    (!empty($user_deduction)  && (int) $user_deduction->percent > 0 ? $user_deduction->employer_percent : $allowance->employer_percent);
                                                                
                                                            $deadline = !empty($user_deduction) ? $user_deduction->deadline : '';
                                                        }
                                                        ?>
            
                                                        <input placeholder="<?= (bool) $allowance->is_percentage == false ? $data->lang->line('amount') : $data->lang->line('percent') ?>" type="number" class="form-control all_deduc" id="amount<?= $user->id . $user->table ?>" name="amount" data-is_percent="<?= (bool) $allowance->is_percentage == false ? 0 : 1 ?>" value="<?= $amount ?>" >
                                                        
                                                    </td>
                                                    <?php
                                                    if ($type == 'deduction') {
                                                        ?>  
                                                        <td>
                                                          
                                                            <input placeholder="<?= (bool) $allowance->is_percentage == false ? $data->lang->line('employer_amount') : $data->lang->line('employer_percent') ?>" type="number" class="form-control all_deduc" id="employer_amount<?= $user->id . $user->table ?>" name="amount" data-is_percent="<?= (bool) $allowance->is_percentage == false ? 0 : 1 ?>" value="<?= $employer_amount ?>" ></td>
                                                    <?php }
                                                }
                                                ?>
                                                    <?php if ($type == 'pension') { ?>
                                                    <td data-title="student_section') ?>">
                                                        <?php
                                                        $user_pension = DB::table('user_pensions')->where('user_id', $user->id)->where('table', $user->table)->where('pension_id', $set)->first();
                                                       
                                                        ?>
                                                        <input type="text" <?php
                                                        if (!empty($user_pension) && in_array($user->id . $user->table, $subscriptions)) {
                                                            echo strlen($user_pension->checknumber) > 1 ? ' value="' . $user_pension->checknumber . '" ' : ' value="' . $user->national_id . '"';
                                                            echo ' pension="' . $user_pension->id . '"';
                                                        } else {
                                                            echo 'disabled';
                                                        }
                                                        ?> class="checknumber" id="<?= $user->id ?>" table="<?= $user->table ?>"/>
                                                        <span id="check<?= $user->id ?><?= $user->table ?>"></span>
                                                    </td>
                                                    <?php } ?>
                                                <td data-title="action">
                                                    <?php
                                                    if (in_array($user->id . $user->table, $subscriptions)) {
                                                        ?>
                                                        <a href="<?= url('payroll/deleteSubscriber/null/?user_id=' . $user->id . '&table=' . $user->table . '&set=' . $set . '&type=' . $type) ?>" class="btn btn-danger btn-xs mrg"><i class="fa fa-trash-o"></i> Remove</a>
                                                    <?php } else { ?>
                                                        <input type="checkbox" value="<?= $user->id; ?>" name="result<?= $user->id; ?>" class="subscribe" id="<?= $user->id ?>" datatype="<?= $type ?>" table="<?= $user->table ?>" class="check<?= $user->id ?>">
                                                     <?php } ?>
                                                </td>
                                            </tr>
                                            <?php
                                            $i++;
                                        }
                                    }
                                    ?>
                                </tbody>
                                <!-- body end -->
                    </table>
                </div> <!-- End datatable -->
             </div>
             <!-- summary-->
                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <h5> Summary </h5> 
                    <table class="table table-bordered">
                        <thead>
                          
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                    <div class="row">             
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h6></h6>
                                    <div class="clearfix"></div>
                                </div>
                                   
                            </div>
                        </div>
                    </div>
                </div> <!-- End summary -->
        </div>
   </div>

@endsection