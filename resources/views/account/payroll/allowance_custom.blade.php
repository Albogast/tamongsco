@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>
   
<div class="page-body">
    <!-- Server Side Processing table start -->
    <div class="card">
        <div class="card-block">
                
            <form style="" class="form-horizontal" role="form" method="post">
                <div class="row">
                   <div class="col-sm-12 col-xl-3 m-b-30">
                     <h4 class="sub-title">School</h4>
                       <select id="schema" name="schema" class="form-control form-control-primary" required>
                           <option value="" disabled selected>Select school</option>
                               @foreach (load_schemas() as $school)
                                   <option value="{{$school->username}}">{{$school->username}}</option>
                               @endforeach
                       </select>
                   </div>
                   <div class="col-sm-12 col-xl-3 m-b-30">
                    <h4 class="sub-title">Category</h4>
                    <select id="deduction_category_id" name="allowance_category_id" class="form-control form-control-primary">
                        <option value="" disabled selected>Select Category</option>
                        <option value="1">Fixed Allowances</option>
                        <option value="2">Monthly Allowances</option>
                    </select>
                </div>
                   <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form">
                       <h4 class="sub-title"><br> </h4>
                       <div class=" col-xs-12">
                           <input type="submit" class="btn btn-success submit_btn_custom" value="Submit">
                       </div>
                   </div>
                    
                 <?= csrf_field() ?>
               </form>
        </div>
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">{{$schema_name}} Allowances</a>
                {{-- <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Summary</a> --}}
                
                </div>
            </nav>
            <br>
          <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                <div class="table-responsive dt-responsive">
                    <table id="dom-jqry" class="table table-striped table-bordered dataTable">
                        <thead>
                            <tr>
                                <th class="col-lg-1">#</th>
                                    <th class="col-lg-2">Name</th>
                                    <th class="col-lg-1">Category</th>
                                    <th class="col-lg-1">Type</th>
                                    <th class="col-lg-1">Pension</th>
                                    <th class="col-lg-1">Amount</th>
                                    <th class="col-lg-1">Percentage</th>
                                    <th class="col-lg-1">Description</th>
                                    <th class="col-lg-2">Action</th>
                            </tr>
                        </thead>
                                <!-- body start -->
                                <tbody>
                                    <?php $i = 1; ?>
                                @foreach ($allowances as $allowance)
                                    <tr>
                                        <td data-title="slno">
                                            <?php echo $i; ?>
                                        </td>
                                        <td data-title="name">
                                            <?php echo $allowance->name; ?>
                                        </td>
                                        <td data-title="name">
                                            <?php echo $allowance->category == 1 ? 'Fixed' : 'Monthly'; ?>
                                        </td>
                                        <td data-title="type">
                                            <?php echo $allowance->type == 0 ? 'Taxable' : 'Non Taxable'; ?>
                                        </td>
                                        <td data-title="pension">
                                            <?php echo $allowance->pension_included == 1 ? 'Included' : 'Non Included'; ?>
                                        </td>
                                        <td data-title="amount">
                                            <?php echo $allowance->amount; ?>
                                        </td>
                                        <td data-title="percentage">
                                            <?php echo (bool) $allowance->is_percentage == false ? 'NONE' : $allowance->percent; ?>
                                        </td>
                                        <td data-title="description">
                                            <?php echo $allowance->description; ?>
                                        </td>
                                        <td data-title="action">
                                            <?php
                                                //  echo '<a  href="' . url("allowance/edit/$allowance->id") . ' " class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> ' . edit . ' </a>'; 
                                            ?>
                                            <?php 
                                            // echo '<a  href="' . url("allowance/delete/$allowance->id") . ' " class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> ' . delete . ' </a>';
                                            
                                            $sub = $category == 1 ? 'subscribe' : 'monthlysubscribe';
                                            ?>
                                            <a href="<?= url('allowance/'.$sub.'/' . $allowance->id .'/'.$schema_name) ?>" class="btn btn-primary btn-xs mrg" ><i class="fa fa-users"></i> members</a>
                                        </td>
                                    </tr>
                                    <?php $i++;?>
                                    @endforeach
                                </tbody>
                                <!-- body end -->
                    </table>
                </div> <!-- End datatable -->
             </div>
             <!-- summary-->
                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <h5> Summary </h5> 
                    <table class="table table-bordered">
                        <thead>
                          
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                    <div class="row">             
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    {{-- <h6>Transaction By Month</h6> --}}
                                    <div class="clearfix"></div>
                                </div>
                                  
                            </div>
                        </div>
                    </div>
                </div> <!-- End summary -->
        </div>
   </div>

@endsection