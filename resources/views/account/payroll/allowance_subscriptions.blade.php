@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>
   
<div class="page-body">
    <!-- Server Side Processing table start -->
    <div class="card">
        <div class="card-block">
                
            <form style="" class="form-horizontal" action="{{url('allowance/index')}}"role="form" method="post">
                <div class="row">
                   <div class="col-sm-12 col-xl-3 m-b-30">
                     <h4 class="sub-title">School</h4>
                       <select id="schema" name="schema" class="form-control form-control-primary" required>
                           <option value="" disabled selected>Select school</option>
                               @foreach (load_schemas() as $school)
                                   <option value="{{$school->username}}">{{$school->username}}</option>
                               @endforeach
                       </select>
                   </div>
                   <div class="col-sm-12 col-xl-3 m-b-30">
                        <h4 class="sub-title">Category</h4>
                        <select id="allowance_category_id" name="allowance_category_id" class="form-control form-control-primary">
                            <option value="" disabled selected>Select Category</option>
                            <option value="1">Fixed Deductions</option>
                            <option value="2">Monthly Deductions</option>
                        </select>
                    </div>
            
                   <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form">
                       <h4 class="sub-title"><br> </h4>
                       <div class=" col-xs-12">
                           <input type="submit" class="btn btn-success submit_btn_custom" value="Submit">
                       </div>
                   </div>
                    
                 <?= csrf_field() ?>
               </form>
        </div>
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">{{$schema_name}} Allowance Subscriptions</a>
                {{-- <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Summary</a> --}}
                
                </div>
            </nav>
            <br>
          <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                <div class="table-responsive dt-responsive">
                    <table id="dom-jqry" class="table table-striped table-bordered dataTable">
                        <thead>
                            <tr>
                            <th class="col-sm-1">#</th>
                            <th class="col-sm-2">Name</th>
                            <th class="col-sm-2">User type</th>
                            <th class="col-sm-2">Email</th>
                            <th class="col-sm-2">Phone Number</th>
                            <th class="col-sm-2">Amount</th>
                            <th class="col-sm-2">Deadline</th>
                            {{-- <th class="col-sm-2">Action</th> --}}
                            </tr>
                        </thead>
                                <!-- body start -->
                            <tbody>
                               
                            </tbody>
                                <!-- body end -->
                                
                    </table>
                </div> <!-- End datatable -->
             </div>
             <!-- summary-->
                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <h5> Summary </h5> 
                    <table class="table table-bordered">
                        <thead>
                          
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                    <div class="row">             
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <div class="clearfix"></div>
                                </div>
                                    <?php
                                    //$insight = new \App\Http\Controllers\Insight();
                                    //$sql_ = 'select sum(amount) as count, to_char(date,\'Mon\')  as month from '.strtolower($type).' where date between \''.$from.'\' and \''.$to.'\' group by to_char(date,\'Mon\') order by EXTRACT(MONTH FROM to_date(to_char(date,\'Mon\'), \'Mon\'))';
                                // echo  $insight->createChartBySql($sql_, 'month', 'Overall Transactions', 'line', false);
                                    ?>
                            </div>
                        </div>
                    </div>
                </div> <!-- End summary -->
        </div>
   </div>

@endsection