@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>
   
<div class="page-body">
    <!-- Server Side Processing table start -->
    <div class="card">
        <div class="card-block">
                
            <form style="" class="form-horizontal" role="form" method="post">
                <div class="row">
                   <div class="col-sm-12 col-xl-3 m-b-30">
                     <h4 class="sub-title">School</h4>
                       <select id="schema" name="schema" class="form-control form-control-primary" required>
                           <option value="" disabled selected>Select school</option>
                               @foreach (load_schemas() as $school)
                                   <option value="{{$school->username}}">{{$school->username}}</option>
                               @endforeach
                       </select>
                   </div>
              
                   <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form">
                       <h4 class="sub-title"><br> </h4>
                       <div class=" col-xs-12">
                           <input type="submit" class="btn btn-success submit_btn_custom" value="Submit">
                       </div>
                   </div>
                    
                 <?= csrf_field() ?>
               </form>
        </div>
         <br>
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Payroll View</a>
                </div>
            </nav>
            <br>
          <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                <div class="table-responsive dt-responsive">
                    <table id="dom-jqry" class="table table-striped table-bordered dataTable">
                        <thead>
                            <th class="col-sm-1">#</th>
                                <th class="col-sm-2">Employee Name</th>
                                <th class="col-sm-1">Basic Pay</th>
                                <th class="col-sm-1">Bank</th>
                                <th class="col-sm-2">Account Number</th>
                                <th class="col-sm-1">Allowance</th>
                                <th class="col-sm-1">Gross Pay<th>
                                <th class="col-sm-1">Pension</th>
                                <th class="col-sm-1">Deduction</th>
                                <th class="col-sm-1">Taxable Amount</th>
                                <th class="col-sm-1">PAYE</th>
                                <th class="col-sm-1">Net Pay</th>
                                <th class="col-sm-1">Date</th>
                                <th class="col-sm-4">Action</th>
                        </thead>
                                <!-- body start -->
                            <tbody>
                                <?php
                                $i = 1;
                                $total_basic_pay = 0;
                                $sum_of_total_allowances = 0;
                                $total_gross_pay = 0;
                                $total_pension = 0;
                                $sum_of_total_deductions = 0;
                                $total_paye = 0;
                                $total_taxable_amount = 0;
                                $total_net_pay = 0;
                                foreach ($salaries as $salary) {
                                    $basic_salary = $salary->basic_pay;
                                    $total_basic_pay += $basic_salary;
                                    $user = \DB::table($schema_name.'.users as a')->where('a.id', $salary->user_id)->where('a.table', $salary->table)->first();
                                    if (!empty($user)) {
                                        $name = $user->name;
                                        $id = $user->id;
                                    } else {
                                     $table=$salary->table=='student' ? 'student_id': $salary->table.'ID';
                                        $us = \DB::table($salary->table)->where($table, $salary->user_id)->first();
                                        if (empty($us)) {
                                            $name = 'Deleted User';
                                            $id = 'removed';
                                        } else {
                                            $name = $us->name;
                                            $id = $us->id_number;
                                        }
                                    }
                                    $user_info_bank = \DB::table($schema_name.'.users as a')->where('a.table', $salary->table)->where('a.id', $salary->user_id)->first();
                                    if (!empty($user_info_bank)) {
                                       // $bank = $user_info_bank->userInfo(DB::table($salary->table));
                                      
                                        if($salary->table=='student'){
                                            $bank = \DB::table($schema_name.'.student as a')->where('a.student_id',$salary->user_id)->first();
                                        }
                                        if($salary->table=='teacher'){
                                            $bank = \DB::table($schema_name.'.teacher as b')->where('b.teacherID',$salary->user_id)->first();
                                        }
                                        if($salary->table=='user'){
                                            $bank = \DB::table($schema_name.'.user as c')->where('c.userID',$salary->user_id)->first();
                                        }
                                        
                                        $bank_name = $bank->bank_name;
                                        $bank_account = $bank->bank_account_number;
                                    } else {
                                        $bank_name = '';
                                        $bank_account = '';
                                    }
                                    ?>
                                    <tr>
                                        <td  data-title="#"><?= $i ?></td>
                                        <td  data-title="Employee Name"><?= $name ?></td>
                                        <td  data-title="Basic Pay"><?= $basic_salary ?></td>
                                        <td  data-title="Bank"><?= $bank_name == '' ? 'null' : $bank_name ?></td>
                                        <td  data-title="Bank Account"><?= $bank_account == '' ? 'null' : $bank_account ?> </td>
                                        <td  data-title="Allowance">
                                            <?php
                                            //calculate user allowances
    
                                            echo money($salary->allowance);
                                            $sum_of_total_allowances += $salary->allowance;
                                            ?>
                                        </td>
                                        <td  data-title="Gross Pay">
                                            <?php
                                            $gross_pay = $basic_salary + $salary->allowance;
                                            echo money($gross_pay);
                                            $total_gross_pay += $gross_pay;
                                            ?> 
                                        </td>
                                        <td  data-title="Pension">  
                                            <?php
                                            //calculate user pension amount
    
                                            echo money($salary->pension_fund);
                                            $total_pension += $salary->pension_fund;
                                            ?>
                                        </td>
                                        <td  data-title="Deduction">
                                            <?php
                                            //calculate user deductions
    
                                            echo money($salary->deduction);
                                            $sum_of_total_deductions += $salary->deduction;
                                            ?> 
                                        </td>
                                        <td  data-title="Taxable amount"> 
                                            <?php
                                            //calculate user taxable amount
                                            $taxable_amount = $gross_pay - $salary->pension_fund;
                                            echo money($taxable_amount);
                                            $total_taxable_amount += $taxable_amount;
                                            ?>  
                                        </td>
                                        <td  data-title="paye">
                                            <?php
                                            //calculate PAYEE
    
                                            echo money($salary->paye);
                                            $total_paye += $salary->paye;
                                            ?> 
                                        </td>
                                        <td  data-title="Net pay">
                                            <?php
                                            $net_pay = $gross_pay - $salary->pension_fund - $salary->deduction - $salary->paye;
                                            echo money($net_pay);
                                            $total_net_pay += $net_pay;
                                            ?>
                                        </td>
                                        <td  data-title="Date">
                                            <?= $set ?> 
                                        </td>
                                        <td  data-title="Action">
                                            <a href="<?= url('payroll/payslip/null/?id=' . $salary->user_id . '&table=' . $salary->table . '&month=' . date('m') . '&set=' . $set) ?>" class="btn btn-success btn-xs mrg" data-placement="top" data-toggle="tooltip" data-original-title="Show Payslip"><i class="fa fa-file"></i>Preview</a>
                                        </td>                 
                                    </tr>
                                    <?php
                                    $i++;
                                }
                             ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td  data-title="Total">Total</td>
                                    <td  data-title=""></td>
                                    <td  data-title="Basic Pay"><?= money($total_basic_pay) ?></td>
                                    <td  data-title=""></td>
                                    <td data-title=""></td>
                                    <td data-title="Allowance"><?= money($sum_of_total_allowances) ?></td>
                                    <td data-title="Gross Pay"><?= money($total_gross_pay) ?></td>
                                    <td data-title="Pension"><?= money($total_pension) ?></td>
                                    <td data-title="Deduction"><?= money($sum_of_total_deductions) ?></td>
                                    <td data-title="#"><?= money($total_taxable_amount) ?></td>
                                    <td data-title="Taxable Amount"><?= money($total_paye) ?></td>
                                    <td data-title="Net Pay"><?= money($total_net_pay) ?></td>
                                    <td></td>
                                    <td data-title="Action"> <a href="<?= url('payroll/summary/null/?set=' . $set . '&month=' . date('M') . '&month=' . date('m')) . '&' . http_build_query(array('basic_pay' => $total_basic_pay, 'allowance' => $sum_of_total_allowances, 'gross_pay' => $total_gross_pay, 'pension' => $total_pension, 'deduction' => $sum_of_total_deductions, 'tax' => $total_taxable_amount, 'paye' => $total_paye, 'net_pay' => $total_net_pay)) ?>" class="btn btn-success btn-xs mrg" data-placement="top" data-toggle="tooltip" data-original-title="Show Payslip"><i class="fa fa-file"></i>Summary</a></td>
                                </tr>
                            </tfoot>
                        <!-- body end -->
                    </table>
                </div> <!-- End datatable -->
             </div>
        </div>
   </div>

@endsection