@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>
   
<div class="page-body">
    <!-- Server Side Processing table start -->
    <div class="card">
        <div class="card-block">
                
            <form style="" class="form-horizontal" role="form" method="post">
                <div class="row">
                   <div class="col-sm-12 col-xl-3 m-b-30">
                     <h4 class="sub-title">School</h4>
                       <select id="schema" name="schema" class="form-control form-control-primary" required>
                           <option value="" disabled selected>Select school</option>
                               @foreach (load_schemas() as $school)
                                   <option value="{{$school->username}}">{{$school->username}}</option>
                               @endforeach
                       </select>
                   </div>
              
                   <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form">
                       <h4 class="sub-title"><br> </h4>
                       <div class=" col-xs-12">
                           <input type="submit" class="btn btn-success submit_btn_custom" value="Submit">
                       </div>
                   </div>
                    
                 <?= csrf_field() ?>
               </form>
        </div>
         <br>
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Payroll Lists</a>
                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Summary</a>
                </div>
            </nav>
            <br>
          <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                <div class="table-responsive dt-responsive">
                    <table id="dom-jqry" class="table table-striped table-bordered dataTable">
                        <thead>
                            <tr>
                                <th class="col-sm-1">#</th>
                                <th class="col-sm-2">Payment Date</th>
                                <th class="col-sm-2">Total Users</th>
                                <th class="col-sm-2">Basic Pay</th>
                                <th class="col-sm-1">Allowance</th>
                                <th class="col-sm-1">Gross Pay</th>
                                <th class="col-sm-1">Pension</th>
                                <th class="col-sm-1">Deduction</th>
                                <th class="col-sm-1">Tax</th>
                                <th class="col-sm-1">PAYE</th>
                                <th class="col-sm-1">Net Payment</th>
                                <th class="col-sm-4">Action</th>
                            </tr>
                        </thead>
                                <!-- body start -->
                                <tbody>
                                    <?php
                                    if (isset($salaries) && !empty($salaries)) {
                                        $i = 1;
                                        foreach ($salaries as $salary) {
                                            ?>
                                            <tr>
                                                <td data-title="#">
                                                    <?php echo $i; ?>
                                                </td>
                                                <td data-title="payment date">
                                                    <?php
                                                    echo date('d M Y', strtotime($salary->payment_date));
                                                    ?>
                                                </td>
                                                <td data-title="total users">
                                                    <?php echo $salary->total_users; ?>
                                                </td>
                                                <td data-title="basic pay">
                                                    <?php echo money($salary->basic_pay); ?>
                                                </td>
                                                <td data-title="allowance">
                                                    <?php
                                                    echo money($salary->allowance);
                                                    ?>

                                                <td data-title="gross pay">
                                                    <?php echo money($salary->gross_pay); ?>
                                                </td>
                                                <td data-title="pension">
                                                    <?php echo money($salary->pension); ?></td>
                                                <td data-title="deduction">
                                                    <?php echo money($salary->deduction); ?></td>
                                                <td data-title="tax">
                                                    <?php echo money($salary->tax); ?></td>
                                                <td data-title="paye">
                                                    <?php echo money($salary->paye); ?></td>
                                                <td data-title="net pay">
                                                    <?php echo money($salary->net_pay); ?>
                                                </td>
                                                <td data-title="Action">
                                                    <?php
                                                    echo '<a  href="' . url("payroll/show/$salary->payment_date/$schema_name") .  '  " class="btn btn-success btn-xs"><i class="fa fa-folder-o"></i> View </a>';

                                                    // echo '<a href="' . url("payroll/delete/$salary->reference/$schema_name") . '  " class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>';
                                                    ?>
                                                </td>
                                            </tr>
                                            <?php
                                            $i++;
                                        }
                                    }
                                    ?>
                                </tbody>
                                <!-- body end -->
                    </table>
                </div> <!-- End datatable -->
             </div>
             <!-- summary-->
                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <h5> Summary </h5> 
                    <script type="text/javascript">
                        $(function () {
                            $('#container').highcharts({
                                title: {
                                    text: 'Payrol Summary'
                                },
                                xAxis: {
                                    categories: [<?php foreach ($salaries as $salary) {  echo '"'.date('M Y', strtotime($salary->payment_date)).'",'; } ?>]
                                },
                                labels: {
                                    items: [{
                                        html: 'Payroll Summary for Basic and net payments',
                                        style: {
                                            left: '50px',
                                            top: '18px',
                                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                                        }
                                    }]
                                },
                                series: [
                                    {
                                    type: 'column',
                                    name: 'Basic Payments',
                                    data: [
                                        <?php foreach ($salaries as $salary) { echo $salary->basic_pay.','; } ?>
                                                ]
                                }, {
                                    type: 'column',
                                    name: 'Net Payments',
                                    data: [
                                        <?php foreach ($salaries as $salary) { echo $salary->net_pay.','; } ?>
                                                ]
                                },  {
                                    type: 'spline',
                                    name: 'Total Users',
                                    data: [<?php foreach ($salaries as $salary) { echo $salary->total_users.','; } ?>],
                                    marker: {
                                        lineWidth: 2,
                                        lineColor: Highcharts.getOptions().colors[3],
                                        fillColor: 'white'
                                    }
                                }, 

                                ]
                            });
                        });

                    </script>
                    <script src="https://code.highcharts.com/highcharts.js"></script>
                    <script src="https://code.highcharts.com/modules/exporting.js"></script>
                    <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div> <!-- End summary -->
        </div>
   </div>

@endsection