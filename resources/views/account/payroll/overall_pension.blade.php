@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>
   
<div class="page-body">
    <!-- Server Side Processing table start -->
    <div class="card">
        <div class="card-block">
            <form style="" class="form-horizontal" role="form" method="post">
                <div class="row">
                    <div class="col-sm-12 col-xl-3 m-b-30">
                          <h4 class="sub-title">School</h4>
                          <select id="schema" name="schema" class="form-control form-control-primary">
                              <option value="" disabled selected>Select school</option>
                                  @foreach (load_schemas() as $school)
                                      <option value="{{$school->username}}">{{$school->username}}</option>
                                  @endforeach
                          </select>
                      </div>
                      <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form">
                        <h4 class="sub-title"><br> </h4>
                        <div class=" col-xs-12">
                            <input type="submit" class="btn btn-success submit_btn_custom" value="Submit">
                        </div>
                        </div>
                         
                      <?= csrf_field() ?>
                  </form>
        </div>   
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">{{$schema_name}}</a>
            </div>
        </nav>
        <br>
      <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
            <div class="table-responsive dt-responsive">
                <table id="dom-jqry" class="table table-striped table-bordered dataTable">
                            <thead>
                                <tr>
                                    <th class="col-sm-1">#</th>
                                    <th class="col-sm-2">Name</th>
                                    <th class="col-sm-2">Employer Percentage Contribution</th>
                                    <th class="col-sm-2">Employee Percentage Contribution</th>
                                    <th class="col-sm-2">Address</th>
                                    <th class="col-sm-2">Members</th>
                                    <th class="col-sm-2">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1;
                                foreach ($pensions as $pension) {
                                    ?>
                                    <tr>
                                        <td data-title="#">
                                            <?php echo $i; ?>
                                        </td>
                                        <td data-title="Name">
                                            <?php echo $pension->name; ?>
                                        </td>
                                        <td data-title="employer_percentage">
                                            <?php echo $pension->employer_percentage; ?> %
                                        </td>
                                        <td data-title="employee_percentage">
                                                <?php echo $pension->employee_percentage; ?>%
                                        </td>
                                        <td data-title="employee_percentage">
                                            <?php echo $pension->address; ?>
                                        </td>
                                        <td data-title="members">
                                            <?php echo \DB::table($schema_name.'.user_pensions as a')->where('a.pension_id',$pension->id)->count(); ?>
                                        </td>
                                       
                                        <td data-title="employee_percentage">
                                            {{--<a href="<?= url('payroll/pension/' . $pension->id.'/'.$schema_name) ?>" class="btn btn-info btn-xs mrg" ><i class="fa fa-users"></i> members</a>
                                             <a href="<?= url('payroll/editPension/' . $pension->id.'/'.$schema_name) ?>" class="btn btn-primary btn-xs mrg" ><i class="fa fa-edit"></i>edit</a>
                                            <a href="<?= url('payroll/deletePension/' . $pension->id.'/'.$schema_name) ?>" class="btn btn-danger btn-xs mrg" ><i class="fa fa-trash-o"></i>delete</a> --}}
                                        </td>
                                    </tr>
                                    <?php $i++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
            </div>
        </div>
    </div>

@endsection