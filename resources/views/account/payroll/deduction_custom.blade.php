@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>
   
<div class="page-body">
    <!-- Server Side Processing table start -->
    <div class="card">
        <div class="card-block">
                
            <form style="" class="form-horizontal" role="form" method="post">
                <div class="row">
                   <div class="col-sm-12 col-xl-3 m-b-30">
                     <h4 class="sub-title">School</h4>
                       <select id="schema" name="schema" class="form-control form-control-primary" required>
                           <option value="" disabled selected>Select school</option>
                               @foreach (load_schemas() as $school)
                                   <option value="{{$school->username}}">{{$school->username}}</option>
                               @endforeach
                       </select>
                   </div>
                   <div class="col-sm-12 col-xl-3 m-b-30">
                    <h4 class="sub-title">Category</h4>
                    <select id="deduction_category_id" name="deduction_category_id" class="form-control form-control-primary">
                        <option value="" disabled selected>Select Category</option>
                        <option value="1">Fixed Deductions</option>
                        <option value="2">Monthly Deductions</option>
                    </select>
                </div>
                   <div class="col-sm-12 col-xl-3 m-b-30" id="hide-form">
                       <h4 class="sub-title"><br> </h4>
                       <div class=" col-xs-12">
                           <input type="submit" class="btn btn-success submit_btn_custom" value="Submit">
                       </div>
                   </div>
                    
                 <?= csrf_field() ?>
               </form>
        </div>
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">{{$schema_name}} Deductions</a>
                {{-- <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Summary</a> --}}
                
                </div>
            </nav>
            <br>
          <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                <div class="table-responsive dt-responsive">
                    <table id="dom-jqry" class="table table-striped table-bordered dataTable">
                        <thead>
                            <tr>
                                <th class="col-lg-1">#</th>
                                <th class="col-lg-2">Name</th>
                                <th class="col-lg-2">Category</th>
                                <th class="col-lg-1">Amount</th>
                                <th class="col-lg-1">Members<i class="fa fa-question-circle" title="This is the total number of users who are (or ware ) in this plan. It include active and in active users"></i></th>
                                <th class="col-lg-1">Percentage</th>
                                <th class="col-lg-1">Description</th>
                                  <th class="col-lg-1">Bank Account</th>
                                <th class="col-lg-1">Account Number</th>
                                <th class="col-lg-2">Action</th>
                            </tr>
                        </thead>
                                <!-- body start -->
                                <tbody>
                                    @if (isset($deductions) && !empty($deductions))
                                        <?php $i=1;?>
                                        @foreach ($deductions as $deduction)
                                        <tr>
                                            <td data-title="slno">
                                                <?php echo $i; ?>
                                            </td>
                                            <td data-title="name">
                                                <?php echo $deduction->name; ?>
                                            </td>
                                            <td data-title="name">
                                                <?php echo $deduction->category == 1 ? 'Fixed' : 'Monthly'; ?>
                                            </td>
                                            <td data-title="amount">
                                                <?php echo $deduction->amount; ?>
                                            </td>
                                             <td data-title="amount">
                                                 <?php echo  \DB::table($schema_name.'.user_deductions as a')->where('a.deduction_id',$deduction->id)->count(); ?>
                                            </td>
                                            <td data-title="percentage">
                                                <?php echo $deduction->is_percentage == null ? 'NONE' : $deduction->percent; ?>
                                            </td>
                                            <td data-title="description">
                                                <?php echo $deduction->description; ?>
                                            </td>
                                             <td data-title="description">
                                                <?php
                                                $bank_name = \DB::table($schema_name.'.bank_accounts as a')->where('a.id',$deduction->bank_account_id)->first(); 
                                                echo isset($bank_name)?$bank_name->name:'';
                                                ?>
                                            </td>
                                             <td data-title="description">
                                                <?php echo $deduction->account_number; ?>
                                            </td>
                                            <td data-title="action">
                                                {{-- <?php echo '<a  href="' . url("deduction/edit/$deduction->id") . ' " class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> ' .'edit' . ' </a>' ?> --}}
                                                <?php
                                                // echo (int) $deduction->predefined ==1 ? "": '<a  href="' . url("deduction/delete/$deduction->id") . ' " class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> ' .delete. ' </a>';
                                                $sub = $type == 1 ? 'subscribe' : 'monthlysubscribe';
                                                ?>
                                                <a href="<?=url('deduction/' . $sub . '/' . $deduction->id.'/'.$schema_name) ?>" class="btn btn-primary btn-xs mrg" ><i class="fa fa-users"></i> members</a>
                                            </td>
                                        </tr>
                                        <?php $i++;?>
                                        @endforeach
                                    @endif
                                </tbody>
                                <!-- body end -->
                    </table>
                </div> <!-- End datatable -->
             </div>
             <!-- summary-->
                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <h5> Summary </h5> 
                    <table class="table table-bordered">
                        <thead>
                          
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                    <div class="row">             
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    {{-- <h6>Transaction By Month</h6> --}}
                                    <div class="clearfix"></div>
                                </div>
                                  
                            </div>
                        </div>
                    </div>
                </div> <!-- End summary -->
        </div>
   </div>

@endsection