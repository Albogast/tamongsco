@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>

        <div class="page-body">
            <!--profile cover start-->
            <div class="row">
                <div class="col-lg-12">
                    <div class="cover-profile">
                        <div class="profile-bg-img"> 
                            <img class="profile-bg-img img-fluid" style="height: 280px;" src="<?=$root?>/assets/images/shulesoft.JPG" alt="bg-img">
                            <div class="card-block user-info">
                                <div class="col-md-12">
                                    <div class="media-left">
                                        <?php
                                        if (strpos($staff->photo, 'https:') !== false) {
                                            $src = $staff->photo;
                                        } elseif (is_file('storage/uploads/images/' . $staff->photo)) {
                                            $image = 'storage/uploads/images/' . $staff->photo;
                                            $src = url($image);
                                        } else {
                                            $image = 'storage/uploads/images/defualt.png';
                                            $src = url($image);
                                        }
            
                                        ?>
                                        <a href="#" class="profile-image">
                                            <img class="user-img img-radius" style="height: 150px;" src="<?php echo $src; ?>"  alt="ShuleSoft" title="{{$staff->name}}">
                                        </a>
                                    </div>
                                 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                                    <!--profile cover end-->
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <!-- tab header start -->
                                            <div class="tab-header card">
                                                <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist" id="mytab">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" data-toggle="tab" href="#personal" role="tab">Basic Info</a>
                                                        <div class="slide"></div>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" data-toggle="tab" href="#binfo" role="tab">
                                                            Sent sms</a>
                                                        <div class="slide"></div>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" data-toggle="tab" href="#payments" role="tab">
                                                            Activity</a>
                                                        <div class="slide"></div>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" data-toggle="tab" href="#contract" role="tab">Contract</a>
                                                        <div class="slide"></div>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div class="tab-content">
                                                <div class="tab-pane active" id="personal" role="tabpanel">
                                                    <div class="card">
                                                        <div class="card-header">
                                                            <h3 class="card-header-text">Personal Information</h3>
                                                        </div>
                                                        <div class="card-block">
                                                            <div class="view-info">
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="general-info">
                                                                            <div class="row">
                                                                                <div class="col-lg-12 col-xl-6">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table m-0">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <th scope="row">Full Name</th>
                                                                                                    <td><?= $staff->name ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Gender</th>
                                                                                                    <td><?= $staff->sex ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Birth Date</th>
                                                                                                    <td><?= customdate($staff->dob); ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Address</th>
                                                                                                    <td><?= $staff->address ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Joining Date</th>
                                                                                                    <td><?= customdate($staff->jod) ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Bank name</th>
                                                                                                    <td><?= $staff->bank_name ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Salary </th>
                                                                                                    <td><?= money($staff->salary) ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Town</th>
                                                                                                    <td><?= $staff->town ?></td>
                                                                                                </tr>
                                                                                              
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- end of table col-lg-6 -->
                                                                                <div class="col-lg-12 col-xl-6">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <th scope="row">Id number</th>
                                                                                                    <td><?= $staff->sid ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Username</th>
                                                                                                    <td><?= $staff->username ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Status</th>
                                                                                                    <td><?= $staff->status == '1'? 'Active' : 'Inactive' ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">National ID</th>
                                                                                                    <td><?= $staff->national_id ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Town</th>
                                                                                                    <td><?= $staff->town?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Bank Account Number</th>
                                                                                                    <td><?= $staff->bank_account_number ?></td>
                                                                                                </tr>

                                                                                                <tr>
                                                                                                    <th scope="row">Email</th>
                                                                                                    <td><?= $staff->email; ?></td>
                                                                                                </tr>
                                                                                            
                                                                                              
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- end of table col-lg-6 -->
                                                                            </div>
                                                                            <!-- end of row -->
                                                                        </div>
                                                                        <!-- end of general info -->
                                                                    </div>
                                                                    <!-- end of col-lg-12 -->
                                                                </div>
                                                                <!-- end of row -->
                                                            </div>                                                         
                                                        </div>
                                                    </div>
                                                </div>
                                                

                                                 <div class="tab-pane" id="binfo" role="tabpanel">
                                                     <div class="card">
                                                        <div class="card-block">
                                                            <div class="view-info">
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="general-info">
                                                                            <div class="row">
                                                                                <div class="col-lg-12 col-xl-12">
                                                                                    <div class="table-responsive dt-responsive">
                                                                                        <table id="" class="table table-striped table-bordered nowrap">
                                                                                             <?php
                                                                                                    if (isset($messages)) {
                                                                                                        foreach ($messages as $message) {
                                                                                                            if ($message->is_sent == 1) {
                                                                                                                ?>
                                                                                                                <li>
                                                                                                                    
                                                                                                                    <div class="message_date">
                                                                                                                        <h3 class="date text-info"><?= $message->is_sent == 1 ? 'sent' : 'received' ?></h3>
                                                                                                                        <p class="month"><?= timeAgo($message->created_at) ?></p>
                                                                                                                    </div>
                                                                                                                    <div class="message_wrapper">
                                                                                                                        <h5 class="heading"><?= user_name($message->user_id,$schema) ?></h5>
                                                                                                                        <blockquote class="message"><?= $message->body ?>
                                                                                                                        </blockquote>
                                                                                                                        <br>
                                                                                                                        <p class="url">
                                                                                                                            <span class="fs1 text-info" aria-hidden="true" data-icon=""></span>
                                                                                                                            <a href="#"></a>
                                                                                                                        </p>
                                                                                                                    </div>
                                                                                                                </li>
                                                    
                                                                                                            <?php } else { ?>
                                                                                                                <li>
                                                                                                                    <a style="text-decoration:none">
                                                                                                                        <h3 class="date text-info text-right"><br/>
                                                                                                                            <?= timeAgo($message->created_at) ?></h3>
                                                                                                                        <br/>
                                                                                                                        <div class="message_date">
                                                                                                                            <img src="<?= url("storage/uploads/images/defualt.png"); ?>" class="avatar" alt="Avatar">
                                                                                                                        </div>
                                                                                                                        <div class="message_wrapper text-right">
                                                                                                                            <h6 class="heading"></h6>
                                                                                                                            <blockquote class="message text-right"><?= $message->body ?></blockquote>
                                                                                                                            <br>
                                                    
                                                                                                                        </div>
                                                                                                                    </a>
                                                                                                                </li>  
                                                    
                                                                                                                <?php
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                    ?>
                                                                                                </ul>
                                                                                            
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                               
                                                                            </div> 
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                
                                                
                                            
                                                 <div class="tab-pane" id="payments" role="tabpanel">
                                                    <div class="row">
                                                        <div class="col-xl-12">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <!-- contact data table card start -->
                                                                    <div class="card">
                                                                        <div class="card-header">
                                                                            <h4 class="card-header-text">Activities</h4>
                                                                        </div>
                                                                        <div class="card-block contact-details">
                                                                            <div class="data_table_main table-responsive dt-responsive">
                                                                                <table id="simpletable" class="table  table-striped table-bordered nowrap">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>#</th>
                                                                                            <th>On Date</th>
                                                                                            <th>Page Visit Action</th>
                                                                                            <th>Device</th>
                                                                                            <th>Browser</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        <?php $i=1;if(isset($logs)) { ?>
                                                                                        <?php

                                                                                        foreach ($logs as $log) {
                                                                                            //check to identify action
                                                                                            $ar = explode('/', $log->url);
                                                                                            $controller = isset($ar[1]) ? $ar[1] : '';
                                                                                            $method = isset($ar[2]) ? $ar[2] : '';
                                                                                            if ($controller == 'SmsController')
                                                                                                continue;
                                                                                            ?>
                                                                                            <tr>
                                                                                                <td><?= $i;?></td>
                                                                                                <td><?= date('d M Y', strtotime($log->created_at)) . ' at ' . date('h:i A', strtotime($log->created_at)) ?></td>
                                                                                                <td><?php
                                                                                                    if (preg_match('/index/', $method) || preg_match('/view/', $method) || preg_match('/show/', $method)) {
                                                                                                        $action = 'View';
                                                                                                    } else if (preg_match('/add/', $method)) {
                                                                                                        $action = 'Add ';
                                                                                                    } else if (preg_match('/edit/', $method)) {
                                                                                                        $action = 'Edit';
                                                                                                    } else if (preg_match('/delete/', $method)) {
                                                                                                        $action = 'Delete';
                                                                                                    } else {
                                                                                                        $action = str_replace('_', ' ', $method);
                                                                                                    }
                                                                                                    echo $action . ' ' . $controller;
                                                                                                    ?></td>
                                                                                                <td><?= $log->platform ?></td>
                                                                                                <td class="hidden-phone"><?= $log->user_agent ?></td>
                                
                                                                                            </tr>
                                                                                            
                                                                                        <?php $i++;} ?>
                                                                                        <?php } ?>
                                                                                    </tbody>
                                                                               
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="tab-pane" id="contract" role="tabpanel">
                                                   <div class="row">

                                                    <div class="col-xl-12">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <!-- contact data table card start -->
                                                                <div class="card">
                                                                    <div class="card-header">
                                                                        <h4 class="card-header-text">Contract</h4>
                                                                    </div>
                                                                    <div class="card-block contact-details">
                                                                        <div class="data_table_main table-responsive dt-responsive">
                                                                            <?php if(count($contracts) > 0) { ?>
                                                                            <table id="simpletable" class="table  table-striped table-bordered nowrap">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>#</th>
                                                                                        <th>On Date</th>
                                                                                        <th>Contract status</th>
                                                                                        <th>Contract start</th>
                                                                                        <th>Contract End</th>
                                                                                        <th>Contract notify</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                  
                                                                                    <?php  $i=1; foreach ($contracts as $contract) {?>
                                                                                        <tr>
                                                                                            <td><?= $i;?></td>
                                                                                            <td><?= date('d M Y', strtotime($contract->created_at)) . ' at ' . date('h:i A', strtotime($contract->created_at)) ?></td>
                                                                                            <td><?= $contract->name ?></td>
                                                                                            <td><?= $contract->start_date ?></td>
                                                                                            <td><?= $contract->end_date ?></td>
                                                                                            <td><?= $contract->notify_date ?></td>
                                                                                        </tr>
                                                                                    <?php $i++; } ?>
                                                                                    <?php } else { ?>
                                                                                      <h4 class="text-center">Contract of <?= user_name($staff_id,$schema) ?> Not Defined</h4>
                                                                                    <?php } ?>
                                                                                </tbody>
                                                                           
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>

                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                          
                                        </div>
                                    </div>
                                </div>
    
@endsection


