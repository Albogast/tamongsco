@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>
<?php
$root = url('/') . '/public/';
define('SCHEMA', $schema);

function check_status($table, $where = null) {
$schema = SCHEMA;
if ($table == 'admin.vendors') {
    $report = \collect(DB::select('select created_at::date from ' . $table . '  ' . $where . ' order by created_at::date desc limit 1'))->first();
}elseif ($table == 'invoices') {
    $report = \collect(DB::select('select date::date as created_at from ' . $table . '  ' . $where . ' order by date::date desc limit 1'))->first();
} else {
    $report = \collect(DB::select('select created_at::date from ' . $schema . '.' . $table . '  ' . $where . ' order by created_at::date desc limit 1'))->first();
}
if (!empty($report)) {

    $echo = '<b class="label label-success">' . date('d M Y', strtotime($report->created_at)) . '</b>';
} else {

    $echo = '<b class="label label-warning">Not Defined</b>';
}
return $echo;
}
?>

                                <div class="page-body">
                                    <!--profile cover start-->
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="cover-profile">
                                                <div class="profile-bg-img"> 
                                                <div class="mapouter">
                                            <div class="gmap_canvas"><iframe width="100%" height="500"
                                                    id="gmap_canvas"
                                                    src="https://maps.google.com/maps?q=<?= $school->sname ?>&t=&z=13&ie=UTF8&iwloc=&output=embed"
                                                    frameborder="0" scrolling="no" marginheight="0"
                                                    marginwidth="0"></iframe><a
                                                    href="https://www.embedgooglemap.net/blog/nordvpn-coupon-code/">nordvpn
                                                    coupon</a></div>
                                            <style>
                                            .mapouter {
                                                position: relative;
                                                text-align: right;
                                                height: 300px;
                                                width: 100%;
                                            }

                                            .gmap_canvas {
                                                overflow: hidden;
                                                background: none !important;
                                                height: 300px;
                                                width: 100%;
                                            }
                                            </style>
                                                    <div class="card-block user-info">
                                                        <div class="col-md-12">
                                                            <div class="media-left">
                                                                <?php
                                                               
                                                                    $image = 'storage/uploads/images/defualt.png';
                                                                    $src = url($image);                                    
                                                                ?>
                                                                <a href="#" class="profile-image">
                                                                    <img class="user-img img-radius" style="height: 150px;" src="<?php echo $src; ?>"  alt="ShuleSoft" title="{{$student->name}}">
                                                                </a>
                                                            </div>
                                                         
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--profile cover end-->
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <!-- tab header start -->
                                            <div class="tab-header card">
                                                <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist" id="mytab">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" data-toggle="tab" href="#personal" role="tab">Student Info</a>
                                                        <div class="slide"></div>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" data-toggle="tab" href="#binfo" role="tab">
                                                            Parent info</a>
                                                        <div class="slide"></div>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" data-toggle="tab" href="#payments" role="tab">
                                                            Payment reports</a>
                                                        <div class="slide"></div>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" data-toggle="tab" href="#examreportd" role="tab">Exam reports</a>
                                                        <div class="slide"></div>
                                                    </li>

                                                </ul>
                                            </div>

                                            <div class="tab-content">
                                                <div class="tab-pane active" id="personal" role="tabpanel">
                                                    <div class="card">
                                                        <div class="card-header">
                                                            <h3 class="card-header-text">About student</h3>
                                                        </div>
                                                        <div class="card-block">
                                                            <div class="view-info">
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="general-info">
                                                                            <div class="row">
                                                                                <div class="col-lg-12 col-xl-6">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table m-0">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <th scope="row">Full Name</th>
                                                                                                    <td><?= $student->name ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Gender</th>
                                                                                                    <td><?= $student->sex ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Birth Date</th>
                                                                                                    <td><?= customdate($student->dob); ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Roll/RegNo Status</th>
                                                                                                    <td><?= $student->roll ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Joining Date</th>
                                                                                                    <td><?= customdate($student->create_date) ?></td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- end of table col-lg-6 -->
                                                                                <div class="col-lg-12 col-xl-6">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <th scope="row">Class</th>
                                                                                                    <td><?= classname($schema,$student->classesID) ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Section/Stream</th>
                                                                                                    <td><?= section_stream($schema,$student->classesID,$student->sectionID) ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Status</th>
                                                                                                    <td><?= $student->status == '1'? 'Active' : 'Inactive' ?></td>
                                                                                                </tr>
                                                                                              
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- end of table col-lg-6 -->
                                                                            </div>
                                                                            <!-- end of row -->
                                                                        </div>
                                                                        <!-- end of general info -->
                                                                    </div>
                                                                    <!-- end of col-lg-12 -->
                                                                </div>
                                                                <!-- end of row -->
                                                            </div>                                                         
                                                        </div>
                                                    </div>
                                                </div>
                                                

                                            <div class="tab-pane" id="binfo" role="tabpanel">
                                                    <!-- personal card start -->
                                                    <div class="card">
                                                        <div class="card-header">
                                                            <h5 class="card-header-text">About Parent</h5>
                                                            <a  type="button" href="<?= url('Users/prt_profile/' .$schema .'/'.$parent->parentID) ?>" class="btn btn-sm btn-primary waves-effect waves-light f-right">
                                                                 View profile  
                                                            </a>
                                                        </div>
                                                        <div class="card-block">
                                                            <div class="view-info">
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="general-info">
                                                                            <div class="row">
                                                                                <div class="col-lg-12 col-xl-6">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table m-0">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <th scope="row">Full Name</th>
                                                                                                    <td><?= $parent->name?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Phone</th>
                                                                                                    <td><?= $parent->phone?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Employer name</th>
                                                                                                    <td><?= $parent->employer?></td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- end of table col-lg-6 -->
                                                                                <div class="col-lg-12 col-xl-6">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <th scope="row">Email</th>
                                                                                                    <td><?= $parent->name?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Address</th>
                                                                                                    <td><?= $parent->address?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Username</th>
                                                                                                    <td><?= $parent->username?></td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div> 
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            
                                                <div class="tab-pane" id="payments" role="tabpanel">
                                                    <div class="row">
                                                        <div class="col-xl-12">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <!-- contact data table card start -->
                                                                    <div class="card">
                                                                        <div class="card-header">
                                                                            <h5 class="card-header-text">Payment records</h5>
                                                                        </div>
                                                                        <div class="card-block contact-details">  
                                                                        <?php $i = 1; foreach ($student_archive as $archive) { ?>
                                                                             <li role="" class="<?= $i == 1 ? 'active' : '' ?>"><a href="#content<?= $archive->id ?>">Class:
                                                                                <?= $archive->classes_name . ' (' . $archive->section_name . ')' ?>, Year:
                                                                                <?= $archive->year ?></a>
                                                                                </li>
                                                                               
                                                                            <div class="data_table_main table-responsive dt-responsive">
                                                                                <table class="table table-bordered no-footer">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th class="col-sm-1">Invoice number</th>
                                                                                            <th class="col-sm-2">Total amount</th>
                                                                                            <th class="col-sm-2">Total paid</th>
                                                                                            <th class="col-sm-2">Total unpaid</th>
                                                                                            <th class="col-sm-2">Advance payment</th>
                                                                                            <th class="col-sm-3">Previous Due Amount</th>
                                                                                            <th class="col-sm-3">Payment status</th>
                                                                                            <th class="col-sm-4">Action</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        <?php
                                                                                    $invoices = DB::table($schema.'.invoice_balances')->join($schema.'.invoices', $schema.'.invoices.id', $schema.'.invoice_balances.invoice_id')->where($schema.'.invoice_balances.student_id', $archive->student_id)->where($schema.'.invoice_balances.academic_year_id', $archive->academic_year_id)->select(DB::raw('coalesce(coalesce(sum(total_amount),0)-sum(discount_amount),0) as amount, coalesce(coalesce(sum(total_payment_invoice_fee_amount),0)+ coalesce(sum(total_advance_invoice_fee_amount)),0) as paid_amount, sum(balance) as balance,invoices.id, invoice_id,invoice_balances.student_id,reference'))->groupBy('invoice_id', 'invoice_balances.student_id', 'reference', 'invoices.id')->get();
                                                                                    foreach ($invoices as $invoice) {
                                                                                        $end_date =  DB::table($schema.'.academic_year')->where('id',$archive->academic_year_id)->first()->end_date;
                                                                                        $installment =  DB::table($schema.'.installments')->where('academic_year_id',$archive->academic_year_id)->orderby('start_date', 'asc');
                                                                                       ?>
                                                                                        <tr>
                                                                                            <td><?= $invoice->reference ?></td>
                                                                                            <td>
                                                                                                <?php
                                                                                                $total_amount = $invoice->amount;
                                                                                                echo money($total_amount);
                                                                                                ?>
                                                                                             </td>
                                                                                           
                                                                                            <td>
                                                                                                <?php
                                                                                                $paid_total_amount = $invoice->paid_amount;
                                                                                                echo money($paid_total_amount);
                                                                                                ?>
                                                                                            </td>
                                                                                            <td>
                                                                                            <?php
                                                                                            echo money($total_amount - $paid_total_amount);
                                                                                            ?>
                                                                                            </td>
                                                                                            <td>
                                                                                             <?php
                                                                                            $adv_amount = 0;
                                                                                            if (!empty($last_invoice)) {
                                                                                                if ($invoice->id != $last_invoice->id) {
                                                                                             } else {
                                                                                                 $advance_amount = \DB::table($schema.'.advance_payment_balance')->where($schema.'.student_id', $invoice->student_id)->select(DB::raw('SUM(reminder) as total_advance'))->first();
                                                                                                 if (!empty($advance_amount)) {
                                                                                                    $adv_amount = money($advance_amount->total_advance);
                                                                                                 }
                                                                                             }
                                                                                           }
                                                                                            echo $adv_amount;
                                                                                            ?>
                                                                                            </td>

                                                                                        <td>
                                                                                           <?php                               
                                                                                          $start_date = $installment->first()->start_date;
                                                                                          $previous_balance = collect(\DB::SELECT('SELECT SUM(balance) as last_balance from ' . $schema .'.invoice_balances where start_date < \'' . $start_date . '\' and student_id= ' . $invoice->student_id . ''))->first();
                                                                                          $amount_due = \collect(DB::select('select sum(amount-coalesce(due_paid_amount,0)) as total_due_amount from ' . $schema. '.dues_balance where student_id=' . $invoice->student_id . ''))->first();
                                                                                          ?>
                                                                                          <?php echo money($previous_balance->last_balance + $amount_due->total_due_amount);?>
                                                                                       </td>
                                                                
                                                                                    <td>
                                                                                        <?php
                                                                                        $status = 1;
                                                                                        $check_status = '';
                                                                                        $setstatus = '';
                                                                                        $btn_class = 'success';
                                                                                        if ($paid_total_amount == '0') {
                                                                                            $check_status = '0';
                                                                                            $status = 'Invoice not paid';
                                                                                            $btn_class = 'danger';
                                                                                        } elseif ($paid_total_amount > 0 && $paid_total_amount < $total_amount) {
                                                                                            $check_status = '2';
                                                                                            $status = 'Invoice partially paid';
                                                                                            $btn_class = 'warning';
                                                                                        } elseif ($paid_total_amount >= $total_amount) {
                                                                                            $check_status = '3';
                                                                                            $status = 'Invoice fully paid';
                                                                                            $btn_class = 'info';
                                                                                        }
                                                                
                                                                                            echo "<button class='btn btn-" . $btn_class . " btn-sm'>" . $status . "</button>";
                                                                                             ?>
                                                                                        </td>
                                                                
                                                                                        <td> </td>
                                                                                        </tr>
                                                                                    <?php } ?>

                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                            <?php } ?>

                                                                        </div>
                                                                        
                                                                    </div>
                                                                    <!-- contact data table card end -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                
                                                <!-- tab pane contact end -->
                                                <div class="tab-pane" id="examreportd" role="tabpanel">
                                                     <div class="row">
                                    
                                                     <div class="col-xl-12">
                                                       <div class="row">
                                                        <div class="col-sm-12">
                                                            <!-- contact data table card start -->
                                                            <div class="card">
                                                                <div class="card-header">
                                                                    <h5 class="card-header-text"></h5>
                                                                </div>
                                                                
                                                                <div class="card-block contact-details">
                                                            <?php $i = 1; foreach ($student_archive as $archive) { ?>
                                                                {{-- <h6 role="" class="<?= $i == 1 ? 'active' : '' ?>">
                                                                    <b class="<?= $i == 1 ? 'active' : '' ?>"> <a href="#navpills-<?= $archive->id ?>" data-toggle="tab" aria-expanded="<?= $i == 1 ? 'true' : 'false' ?>"><?= $archive->classes_name ?>-<?= $archive->year  ?></a></b>
                                                                   
                                                                </h6> --}}
                                                                <?php $i++; } ?>

                                                                <?php  $j = 1;
                                                                foreach ($student_archive as $archive) {
                                                                    $semisters = DB::table($schema.'.semester')->where('academic_year_id', $archive->academic_year_id)->get();
                                                                    $student_subject = DB::table($schema.'.subject_count')->where('student_id', $archive->student_id)->where('academic_year_id', $archive->academic_year_id)->count();
                                                                    ?>
                                                            <div id="navpills-<?= $archive->id ?>" class="tab-pane <?= $j == 1 ? 'active' : '' ?>">
                                                            
                                                         <?php $k = 1;if (!empty($semisters)) {
                                                            foreach ($semisters as $key => $semester) { ?>
                                                           <h6 class=""><?= $semester->name ?></h6>
                                                          <hr />

                                                    <ul style="margin-left:1em">
                                                    <div>
                                                      <div>
                                                        <div>
                                                         <?php
                                                          $exams = \DB::select('SELECT  a."examID", a.academic_year_id, a."classesID", b.date, b.exam from ' . $schema . '.student_exams a JOIN ' . $schema . '.exam b on a."examID"=b."examID"  WHERE a."student_id"=' . $archive->student_id . ' and b."examID" in (select exam_id from ' . $schema . '.exam_report) AND b.semester_id=' . $semester->id . ' order by b.date');
                                                            if (count($exams) > 0) { ?>
                                                            <h6>Single report</h6>
                                                             <br />

                                                            <div class="data_table_main table-responsive dt-responsive">
                                                                <table id="" class="table  table-striped table-bordered nowrap">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>#</th>
                                                                            <th>Exam name</th>
                                                                            <th>Exam date</th>
                                                                            <th>Exam Status</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php $f = 1;
                                                                           if (!empty($exams)) {
                                                                              foreach ($exams as $exam) { ?>
                                                                        <tr>
                                                                            <td><?= $f ?></td>
                                                                            <td><?= ucfirst($exam->exam) ?> </td>
                                                                            <td><?= date('d M Y', strtotime($exam->date)) ?></td>
                                                                            <td>
                                                                            <?php
                                                                            $marked_subjects = \collect(\DB::select('select count(distinct "subjectID") FROM ' . $schema . '.mark_info where "student_id"=' . $archive->student_id . ' and academic_year_id=' . $archive->academic_year_id . ' and "examID"=' . $exam->examID))->first();
                                                                            $diff = $student_subject - $marked_subjects->count;
                                                                            if ($diff == 0) {
                                                                                echo '<b class="badge badge-success">complete</b>';
                                                                            } else {
                                                                                echo $marked_subjects->count . ' out of ' . $student_subject;
                                                                            }
                                                                            ?>
                                                                            </td>
                                                                        </tr>
                                                                       <?php ?>
                                                                      <?php $f++;}
                                                                        }?>
                                                                    </tbody>       
                                                                </table>
                                                            </div>
                                                            <?php } ?>
                                                            <?php
                                                            $sql = 'SELECT a.* from ' . $schema . '.exam_report a where a.classes_id in (select "classesID" FROM ' . $schema . '.section where "sectionID"=' . $archive->section_id . ') and a.combined_exams !=\'0\'  and a.academic_year_id=' . $archive->academic_year_id . ' and a.semester_id=' . $semester->id . '';
                                                            $exams_report = \DB::select($sql);
                                                               if (count($exams_report) > 0) {  ?>
                                                              <h6><?= ("school report") ?></h6>
                                                              <div class="data_table_main table-responsive dt-responsive">
                                                                <table id="" class="table  table-striped table-bordered nowrap">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>#</th>
                                                                            <th>Exam date</th>
                                                                            <th>Exam date</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php $z = 1;
                                                                        foreach ($exams_report as $rep) {
                                                                        ?>
                                                                        <tr><td><?= $z?>
                                                                            <td><?= $rep->name == '' ? 'STUDENT ACADEMIC REPORT' : $rep->name ?> </td>
                                                                            <td><?= date('d M Y', strtotime($rep->created_at)) ?></td>
                                                                        </tr>
                                                                       <?php $z++; } ?>
                                                                    </tbody>       
                                                                </table>
                                                            </div>
                                                            <?php } ?>
                                                        </div>
                                                     </div>
                                                  </div>
                                                </ul>
                                            <?php $k++; } }  ?>
                                                </div>
                                         <?php $j++; } ?>        
                                            </div>
                                        </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>          
                                                
                      </div>
                                     
                   </div>
                </div>
             </div>
    
@endsection