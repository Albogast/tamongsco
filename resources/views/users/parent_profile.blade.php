
@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>

                          <div class="page-body">
                                    <!--profile cover start-->
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="cover-profile">
                                                <div class="profile-bg-img"> 
                                                    <img class="profile-bg-img img-fluid" style="height: 280px;" src="<?=$root?>/assets/images/shulesoft.JPG" alt="bg-img">
                                                    <div class="card-block user-info">
                                                      
                                                        <div class="col-md-12">
                                                            <div class="media-left">
                                                                <?php 
                                                                if (strpos($parent->photo, 'https:') !== false) {
                                                                    $src = $parent->photo;
                                                                } elseif (is_file('storage/uploads/images/' . $parent->photo)) {
                                                                    $image = 'storage/uploads/images/' . $parent->photo;
                                                                    $src = url($image);
                                                                } else {
                                                                    $image = 'storage/uploads/images/defualt.png';
                                                                    $src = url($image);
                                                                }
                                    
                                                                ?>
                                                                <a href="#" class="profile-image">
                                                                    <img class="user-img img-radius" style="height: 150px;" src="<?php echo $src; ?>"  alt="ShuleSoft" title="{{$parent->name}}">
                                                                </a>
                                                            </div>
                                                         
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--profile cover end-->
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <!-- tab header start -->
                                            <div class="tab-header card">
                                                <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist" id="mytab">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" data-toggle="tab" href="#personal" role="tab">Basic Info</a>
                                                        <div class="slide"></div>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" data-toggle="tab" href="#students" role="tab">
                                                           Students</a>
                                                        <div class="slide"></div>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" data-toggle="tab" href="#activities" role="tab">
                                                            Payments</a>
                                                        <div class="slide"></div>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" data-toggle="tab" href="#sms" role="tab">SMS</a>
                                                        <div class="slide"></div>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div class="tab-content">
                                                <div class="tab-pane active" id="personal" role="tabpanel">
                                                    <div class="card">
                                                        <div class="card-header">
                                                            <h3 class="card-header-text">Parent profile</h3>
                                                        </div>
                                                        <div class="card-block">
                                                            <div class="view-info">
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="general-info">
                                                                            <div class="row">
                                                                                <div class="col-lg-12 col-xl-6">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table table-responsive m-b-0">
                                                                                            <tr>
                                                                                                <th class="">Parent full Name
                                                                                                </th>
                                                                                                <td class=""><?= $parent->name ?></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <th class="">Relation</th>
                                                                                                <td class=""><?=$parent->relation?></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <th class="">Proffesion</th>
                                                                                                <td class=""><?= $parent->relation == 'Father' ? $parent->father_profession : $parent->mother_profession ?></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <th class="">Employer Name</th>
                                                                                                <td class=""><?=$parent->employer ?></td>
                                                                                            </tr>
                                                                                             <tr>
                                                                                                <th class="">Phone</th>
                                                                                                <td class=""><?=$parent->phone ?></td>
                                                                                               </tr>
                                                                                            
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- end of table col-lg-6 -->
                                                                                <div class="col-lg-12 col-xl-6">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <th class="">Email</th>
                                                                                                      <td class=""><?=$parent->email ?></td>
                                                                                                   </tr>
                                                                                                   <tr>
                                                                                                     <th class="">Address</th>
                                                                                                      <td class=""> <?=$parent->address ?> </td>
                                                                                                 </tr>
                                                                                                  <tr>
                                                                                                     <th class="">Preferred Language</th>
                                                                                                      <td class=""><?=$parent->language?> </td>
                                                                                                 </tr>
                                                                                                   <tr>
                                                                                                     <th class="">Sex</th>
                                                                                                      <td class=""> <?=$parent->sex?>  </td>
                                                                                                 </tr>
                                                                                              
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- end of table col-lg-6 -->
                                                                            </div>
                                                                            <!-- end of row -->
                                                                        </div>
                                                                        <!-- end of general info -->
                                                                    </div>
                                                                    <!-- end of col-lg-12 -->
                                                                </div>
                                                                <!-- end of row -->
                                                            </div>                                                         
                                                        </div>
                                                    </div>
                                                </div>


                                                 <div class="tab-pane" id="students" role="tabpanel">
                                                     <div class="card">
                                                        <div class="card-header">
                                                            <h4 class="card-header-text">Students belongs to </h5>
                                                        </div>
                                                        <div class="card-block">
                                                            <div class="view-info">
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="general-info">
                                                                            <div class="row">
                                                                                <div class="col-lg-12 col-xl-12">
                                                                                    <div class="table-responsive">
                                                                                        <?php if(isset($students)) { ?>
                                                                                        <table id="" class="table table-striped table-bordered nowrap">
                                                                                            <thead>
                                                                                                <tr>
                                                                                                    <th>#</th>
                                                                                                    <th>Photo</th>
                                                                                                    <th>Name</th>
                                                                                                    <th>Class </th>
                                                                                                    <th>Roll</th>
                                                                                                    <th>Action</th>
                                                                                                </tr>
                                                                                              </thead>
                                                                                             <tbody>
                                                                                               
                                                                                                <?php $i=1; foreach ($students as $student) { ?>
                                                                                                  <tr>
                                                                                                    <th><?=$i?></th>
                                                                                                    <th><?=$student->photo?></th>
                                                                                                    <th><?=$student->name?></th>
                                                                                                    <th><?=$student->classes?></th>
                                                                                                    <th><?=$student->roll?></th>
                                                                                                    <th>
                                                                                                        <a  type="button" href="<?= url('Users/std_profile/' .$schema .'/'.$student->student_id) ?>" class="btn btn-sm btn-primary waves-effect waves-light f-right">
                                                                                                            View   
                                                                                                       </a>
                                                                                                    </th>
                                                                                                  </tr>
                                                                                                <?php $i++;} ?>
                                                                                            </tbody>
                                                                                       </table>
                                                                                       <?php } ?>
                                                                                    </div>
                                                                             
                                                                            </div> 
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            
                                                <div class="tab-pane" id="activities" role="tabpanel">
                                                    <div class="row">
                                                        <div class="col-xl-12">
                                                            <div class="row">

                                                                <div class="col-sm-12">
                                                                    <!-- contact data table card start -->
                                                                    <div class="card">
                                                                        <div class="card-header">
                                                                            <h4 class="card-header-text">View All invoices</h5>
                                                                        </div>
                                                                        <div class="card-block contact-details">
                                                                           <?php $i = 1; foreach ($students as $stud) { ?>
                                                                            <h5> <?= $stud->name ?> </h5>
                                                                            <div class="data_table_main table-responsive dt-responsive">
                                                                                <table id="" class="table  table-striped table-bordered nowrap">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>#</th>
                                                                                            <th>Invoice No</th>
                                                                                            <th>Total amount </th>
                                                                                            <th>Total Paid</th>
                                                                                            <th>Unpaid</th>
                                                                                            <th>Payment status</th>
                                                                                            <th>Payment due</th>
                                                                                            <th>Action</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        <?php
                                                                                        $f = 1;
                                                                                        
                                                                                        $student_archives = DB::table($schema.'.student_archive')->where('student_id', $stud->student_id)->get();
                                                                                        foreach ($student_archives as $archive) {
                                
                                                                                            $invoices = \DB::table($schema.'.invoice_balances')->join($schema.'.invoices', 'invoices.id', 'invoice_balances.invoice_id')->where($schema.'.invoice_balances.student_id', $archive->student_id)->where($schema.'.invoice_balances.academic_year_id', $archive->academic_year_id)->select(DB::raw('coalesce(coalesce(sum(total_amount),0)-sum(discount_amount),0) as amount, coalesce(coalesce(sum(total_payment_invoice_fee_amount),0)+ coalesce(sum(total_advance_invoice_fee_amount)),0) as paid_amount, sum(balance) as balance,invoices.id, invoice_id,invoice_balances.student_id,reference'))->groupBy('invoice_id', 'invoice_balances.student_id', 'reference', 'invoices.id')->get();
                                                                                            foreach ($invoices as $invoice) {
                                                                                                ?>
                                                                                                <tr> <td><?= $i ?></td>
                                                                                                    <th scope="row"><?= $invoice->reference ?></th>
                                                                                                    <!--<td><?php // date('d M Y', strtotime($invoice->date))  ?></td>-->
                                                                                                    <td>  
                                                                                                        <?php
                                //                                                                    $fees = $invoice->invoicesFeesInstallments()->get();
                                //                                                                    $total_amount = 0;
                                //                                                                    foreach ($fees as $amount) {
                                //                                                                        $total_amount += $amount->amount;
                                //                                                                    }
                                                                                                        // echo money($total_amount);
                                                                                                        $total_amount = $invoice->amount;
                                                                                                        echo money($total_amount);
                                                                                                        ?></td>
                                                                                                       <td>
                                                                                                        <?php
                                //                                                                   
                                                                                                        $paid_total_amount = $invoice->paid_amount;
                                                                                                        echo money($paid_total_amount);
                                                                                                        ?>
                                                                                                      </td>
                                                                                                     <td>
                                                                                                        <?php
                                                                                                        echo money($total_amount - $paid_total_amount);
                                                                                                        ?>
                                                                                                     </td>
                                                                                                    <td> <?php
                                                                                                        $status = '1';
                                                                                                        $setstatus = '';
                                                                                                        $btn_class = 'success';
                                                                                                        if ($paid_total_amount == '0') {
                                                                                                            $check_status = '0';
                                                                                                            $status = 'invoice not paid';
                                                                                                            $btn_class = 'danger';
                                                                                                        } elseif ($paid_total_amount > 0 && $paid_total_amount < $total_amount) {
                                                                                                            $check_status = '2';
                                                                                                            $status = 'Invoice partially paid';
                                                                                                            $btn_class = 'warning';
                                                                                                        } elseif ($paid_total_amount >= $total_amount) {
                                                                                                            $check_status = '3';
                                                                                                            $status = 'invoice fully paid';
                                                                                                            $btn_class = 'info';
                                                                                                        }
                                
                                                                                                        echo "<button class='btn btn-" . $btn_class . " btn-sm'>" . $status . "</button>";
                                                                                                        ?>
                                                                                                      </td>
                                                                                                      <td>
                                                                                                        <?php 
                                                                                                            $payment = \DB::table($schema.'.invoices')->where('id',$invoice->id)->select('invoices.*')->first();
                                                                                                               $i_due_date = date('Y-m-d', strtotime($payment->due_date));
                                                                                                            if(is_null($payment->due_date)){  
                                                                                                                //Update due date by adding 30 days to a date
                                                                                                                $due_date =  date('Y-m-d', strtotime($payment->date. ' + 30 days'));  
                                                                                                                \DB::table($schema.'.invoices')->where('id',$invoice->id)->update(['invoices.due_date' => $due_date]);
                                                                                                                $payment = \DB::table($schema.'.invoices')->where('id',$invoice->id)->select('invoices.due_date')->first();
                                                                                                                $i_due_date = date('Y-m-d', strtotime($payment->due_date));
                                                                                                              }
                                                                                                            
                                                                                                            ?>
                                                                                                           
                                                                                                         <?= $i_due_date ?>                                                                                                        
                                                                                                    </td>
                                                                                                    <td> 
                                                                                                        {{-- <a  type="button" href="<?= url('invoices/show/' .$schema .'/'. $invoice->id .'/'. $archive->academic_year_id) ?>" class="btn btn-sm btn-primary waves-effect waves-light f-right">
                                                                                                            View   
                                                                                                       </a> --}}
                                                                                                    </td>
                                                                                                  </tr>
                                                                                                <?php
                                                                                                $f++;
                                                                                            }
                                                                                        }
                                                                                        ?>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                            <?php } ?>
                                                                        </div>
                                                                        
                                                                    </div>

                                                                </div>
                                                            </div>
                                              
                                                        </div>
                                                    </div>
                                                </div>


                                               
                                                <div class="tab-pane" id="sms" role="tabpanel">
                                                    <div class="row">
                                                              <div class="col-sm-12">
                                                                  <!-- contact data table card start -->
                                                                  <div class="card">
                                                                      <div class="card-header">
                                                                          <h4 class="card-header-text">SMS</h4>
                                                                      </div>
                                                                      <div class="card-block contact-details">
                                                                          <div class="data_table_main table-responsive dt-responsive">
                                                                            <table id="" class="table table-striped table-bordered nowrap">
                                                                                <?php
                                                                                       if (isset($messages)) {
                                                                                           foreach ($messages as $message) {
                                                                                               if ($message->is_sent == 1) {
                                                                                                   ?>
                                                                                                   <li>
                                                                                                    <?php
                                                                                                    $photo = array(
                                                                                                        "src" => url('storage/uploads/images/'),
                                                                                                        'width' => '',
                                                                                                        'height' => '',
                                                                                                        'class' => 'avatar',
                                                                                                        'style' => '',
                                                                                                        'alt' => 'ShuleSoft'
                                                                                                    );
                                                                                                    echo img($photo);
                                                                                                    ?>
                                                                                                       <div class="message_date">
                                                                                                           <h3 class="date text-info"><?= $message->is_sent == 1 ? 'sent' : 'received' ?></h3>
                                                                                                           <p class="month"><?= timeAgo($message->created_at) ?></p>
                                                                                                       </div>
                                                                                                       <div class="message_wrapper">
                                                                                                           <h5 class="heading"><?= $message->user_id != null ? parent_name($message->user_id,$schema) : '' ?></h5>
                                                                                                           <blockquote class="message"><?= $message->body ?>
                                                                                                           </blockquote>
                                                                                                           <br>
                                                                                                           <p class="url">
                                                                                                               <span class="fs1 text-info" aria-hidden="true" data-icon=""></span>
                                                                                                               <a href="#"></a>
                                                                                                           </p>
                                                                                                       </div>
                                                                                                   </li>
                                       
                                                                                               <?php } else { ?>
                                                                                                   <li>
                                                                                                       <a style="text-decoration:none">
                                                                                                           <h3 class="date text-info text-right"><br/>
                                                                                                               <?= timeAgo($message->created_at) ?></h3>
                                                                                                           <br/>
                                                                                                           <div class="message_date">
                                                                                                               <img src="<?= url("storage/uploads/images/defualt.png"); ?>" class="avatar" alt="Avatar">
                                                                                                           </div>
                                                                                                           <div class="message_wrapper text-right">
                                                                                                               <h6 class="heading"></h6>
                                                                                                               <blockquote class="message text-right"><?= $message->body ?></blockquote>
                                                                                                               <br>
                                       
                                                                                                           </div>
                                                                                                       </a>
                                                                                                   </li>  
                                       
                                                                                                   <?php
                                                                                               }
                                                                                           }
                                                                                       }
                                                                                       ?>
                                                                                   </ul>
                                                                               
                                                                           </table>
                                                                          </div>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                        </div>
                                                   </div>

                                          </div>
                                       </div>
                                    </div>
                                </div>
                         @endsection