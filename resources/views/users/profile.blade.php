@extends('layouts.app')
@section('content')
<?php
$root = url('/') . '/public/';
define('SCHEMA', $schema);

function check_status($table, $where = null) {
$schema = SCHEMA;
if ($table == 'admin.vendors') {
    $report = \collect(DB::select('select created_at::date from ' . $table . '  ' . $where . ' order by created_at::date desc limit 1'))->first();
}elseif ($table == 'invoices') {
    $report = \collect(DB::select('select date::date as created_at from ' . $table . '  ' . $where . ' order by date::date desc limit 1'))->first();
} else {
    $report = \collect(DB::select('select created_at::date from ' . $schema . '.' . $table . '  ' . $where . ' order by created_at::date desc limit 1'))->first();
}
if (!empty($report)) {

    $echo = '<b class="label label-success">' . date('d M Y', strtotime($report->created_at)) . '</b>';
} else {

    $echo = '<b class="label label-warning">Not Defined</b>';
}
return $echo;
}
?>

<!-- Page-body start -->
<div class="page-body">
    <div class="row">
        <!-- customar project  start -->
        <div class="col-sm-12">
            <div class="card">
                <div class="content social-timeline">
                    <div class="">
                        <!-- Row Starts -->
                        <div class="row">
                            <div class="col-md-12">
                                <!-- Social wallpaper start -->
                                <div class="social-wallpaper">
                                    <div class="mapouter">
                                        <div class="gmap_canvas"><iframe width="100%" height="500" id="gmap_canvas"
                                                src="https://maps.google.com/maps?q=<?= $school->sname ?>&t=&z=13&ie=UTF8&iwloc=&output=embed"
                                                frameborder="0" scrolling="no" marginheight="0"
                                                marginwidth="0"></iframe><a
                                                href="https://www.embedgooglemap.net/blog/nordvpn-coupon-code/">nordvpn
                                                coupon</a></div>
                                        <style>
                                        .mapouter {
                                            position: relative;
                                            text-align: right;
                                            height: 300px;
                                            width: 100%;
                                        }

                                        .gmap_canvas {
                                            overflow: hidden;
                                            background: none !important;
                                            height: 300px;
                                            width: 100%;
                                        }
                                        </style>
                                    </div>
                                    <div class="profile-hvr">
                                        <i class="icofont icofont-ui-edit p-r-10"></i>
                                        <i class="icofont icofont-ui-delete"></i>
                                    </div>
                                </div>
                                <!-- Social wallpaper end -->
                                <!-- Timeline button start -->
                                <div class="timeline-btn">

                                    <a href="#" class="btn btn-primary waves-effect waves-light">Send
                                        Message</a>
                                </div>
                                <!-- Timeline button end -->
                            </div>
                        </div>
                        <!-- Row end -->
                        <!-- Row Starts -->
                        <div class="row">

                            <div class="col-ld-12 col-sm-12 ">
                                <!-- Nav tabs -->
                                <div class="tab-header card">

                                    <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist" id="mytab">

                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#about" role="tab"
                                                aria-expanded="false">About</a>
                                            <div class="slide"></div>
                                        </li>

                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#photos" role="tab"
                                                aria-expanded="false"> Usage</a>
                                            <div class="slide"></div>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link " data-toggle="tab" href="#friends" role="tab"
                                                aria-expanded="true">Staff Members</a>
                                            <div class="slide"></div>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link " data-toggle="tab" href="#payments" role="tab"
                                                aria-expanded="true">Invoice</a>
                                            <div class="slide"></div>
                                        </li>

                                    </ul>
                                </div>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <!-- Timeline tab start -->

                                    <!-- About tab start -->
                                    <div class="tab-pane active" id="about" aria-expanded="true">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="card">
                                                    <div class="card-header">
                                                    <div class="row" style="font-weight: bold; text-align:center;">
                                                    <?php foreach($list_users as $list){ ?>
                                                     <div class="col-md-12 col-lg-3">
                                                        <div class="text-primary">
                                                            <?= $list->total ?>
                                                        </div>
                                                        <div><?= ucfirst($list->table) ?>s</div>
                                                    </div>
                                                    <?php } ?>
                                                   
                                                     </div>
                                                     <hr>
                                                    </div>
                                                    <div class="card-block">
                                                    
                                                        <div id="view-info" class="row">
                                                            <div class="col-lg-12 col-md-12">
                                                            <h5 class="card-header-text">Basic Information</h5>

                                                                <form>
                                                                    <table class="table m-b-0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <th>
                                                                                    School Name
                                                                                </th>
                                                                                <td>
                                                                                    <?= $school->sname ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th>
                                                                                    Location</th>
                                                                                <td>
                                                                                    <?= $school->address ?></td>
                                                                            </tr>
                                                                            <?php if ($is_client == 1) { ?>
                                                                            <tr>
                                                                                <th>
                                                                                    Date On boarded</th>
                                                                                <td>
                                                                                    <?= date('d M Y h:i', strtotime($school->created_at)) ?>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th>
                                                                                    Contact Details</th>
                                                                                <td>
                                                                                    <?= $school->phone ?></td>
                                                                            </tr>
                                                                          
                                                                            
                                                                            <?php } ?>
                                                                        </tbody>
                                                                    </table>
                                                                    <hr>
                                                                    <table class="table m-b-0">
                                                                        <tr>
                                                                        <th colspan="1">School Level</th>
                                                                        <th colspan="1">Result Format</th>
                                                                        <th colspan="1"> Students </th>
                                                                        </tr>
                                                                        <?php
                                                                            if (!empty($levels)) {
                                                                                foreach ($levels as $level) {
                                                                        ?>                                                                                        
                                                                        <tr>
                                                                                <td>
                                                                                    <?php echo $level->name; ?>
                                                                                </td>
                                                                                <td>
                                                                                    <?php echo $level->result_format; ?>
                                                                                </td>
                                                                                <td>55</td>
                                                                            </tr>
                                                                    <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                    </table>
                                                                </form>
                                                            </div>
                                       
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                          
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="implementation" aria-expanded="false">


                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Project Implementation Schedule</h5>
                                                <span>This part have to be followed effectively </span>
                                                <p align="right">
                                                    <a href="<?= url('customer/download/' . $client_id) ?>"
                                                        class="btn btn-warning btn-sx">Download Implementation
                                                        Plan</a>
                                                </p>
                                            </div>
                                            <div class="card-block table-border-style">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered dataTable">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Task</th>
                                                                <th>ShuleSoft Person </th>
                                                                <th><?= ucfirst($schema) ?> Person Allocated
                                                                </th>
                                                                <th>Start Date : Time</th>
                                                                <th>End Date : Time</th>
                                                                <th>Status</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- About tab end -->
                                    <!-- Photos tab start -->
                                    <div class="tab-pane" id="photos" aria-expanded="false">
                                        <div class="card">
                                            <div class="card-header">
                                                <div class="row text-center">
                                                    <div class="col-sm-4">
                                                        <div class="social-media">
                                                            <span>Weekly Logins Users</span>
                                                            <i class="icofont  icofont-ui-user"></i>
                                                            <h5 class="counter">2587</h5>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="social-media">
                                                            <span>Weekly Logins Parents</span>
                                                            <i class="icofont icofont-ui-user"></i>
                                                            <h5 class="counter">5987</h5>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="social-media">
                                                            <span>Weekly Logins Users</span>
                                                            <i class="icofont  icofont-ui-user"></i>
                                                            <h5 class="counter">58,158</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                       

                                        <div class="card-block">
                                        
                                        <div id="container_log"
                                                style="min-width: 80%;  height: 480px; margin: 0 auto">
                                            </div>
                                        <hr>

                                                <h5>Client Usage Stages</h5>
                                                <span>This part analyze customer system usage and provide a
                                                    guidance for account manager to guide properly a school to
                                                    reach the highest stage. </span>

                                            </div>
                                            <div class="card-block table-border-style">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Module</th>
                                                                <th>Status</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th scope="row" colspan="3">
                                                                    <div class="label-main">
                                                                        <label class="label label-primary">Stage
                                                                            1</label>
                                                                    </div>
                                                                </th>

                                                            </tr>
                                                            <tr>
                                                                <th scope="row">1</th>
                                                                <td>Basic Configuration</td>
                                                                <td>
                                                                    <?php
                                                                        //classlevel
                                                                        $levels = DB::table($schema . '.classlevel')->get();
                                                                        if (empty($levels)) {
                                                                            echo '<b class="label label-warning">Class Level Not Defined</b>';
                                                                        }
                                                                        /**
                                                                         * --Check if Academic Years defined
                                                                         */
                                                                        if (!empty($levels)) {
                                                                            foreach ($levels as $level) {

                                                                                $academic_year = DB::table($schema . '.academic_year')->where('class_level_id', $level->classlevel_id)->where('start_date', '<', date('Y-m-d'))->where('end_date', '>', date('Y-m-d'))->first();
                                                                                if (empty($academic_year)) {
                                                                                    echo '<b class="label label-warning">Academic Year Not Defined for ' . $level->name . ' (' . date('Y') . ')</b><br/>';
                                                                                }
                                                                            }
                                                                        } else {
                                                                            echo '<b class="label label-warning">Academic Year Not Defined</b><br/>';
                                                                        }
                                                                        /**
                                                                         *
                                                                         * Check if terms have been defined
                                                                         */
                                                                        if (!empty($levels)) {
                                                                            foreach ($levels as $level) {

                                                                                $academic_year = DB::table($schema . '.academic_year')->where('class_level_id', $level->classlevel_id)->where('start_date', '<', date('Y-m-d'))->where('end_date', '>', date('Y-m-d'))->first();
                                                                                if (empty($academic_year)) {
                                                                                    echo '<b class="label label-warning">No Terms Defined for ' . $level->name . ' (' . date('Y') . ')</b><br/>';
                                                                                } else {
                                                                                    //check terms for this defined year
                                                                                    $terms = DB::table($schema . '.semester')->where('academic_year_id', $academic_year->id)->where('start_date', '<', date('Y-m-d'))->where('end_date', '>', date('Y-m-d'))->count();

                                                                                    echo $terms == 0 ? '<b class="label label-warning">No Terms Defined for ' . $level->name . ' (' . date('Y') . ')</b><br/>' : '<label class="label label-success">' . $level->name . ' (' . $academic_year->name . ') at ' . date('d M Y', strtotime($academic_year->created_at)) . '</label>';
                                                                                }
                                                                            }
                                                                        } else {
                                                                            echo '<b class="label label-warning">Not Defined</b><br/>';
                                                                        }
                                                                        /**
                                                                         *
                                                                         * --check if stamp has been defined
                                                                         *
                                                                         */
                                                                        if (!empty($levels)) {
                                                                            foreach ($levels as $level) {
                                                                                if (strlen($level->stamp) < 3) {
                                                                                    echo '<b class="label label-warning">No Stamp for ' . $level->name . '</b><br/>';
                                                                                }
                                                                            }
                                                                        }
                                                                        ?>

                                                                </td>
                                                                <td>

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">2</th>
                                                                <td>Marking</td>
                                                                <td>
                                                                    <?= check_status('mark'); ?>
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">3</th>
                                                                <td>Exam Published</td>
                                                                <td><?= check_status('exam_report'); ?></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">4</th>
                                                                <td>Invoice Created</td>
                                                                <td> <?= check_status('invoices'); ?></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">5</th>
                                                                <td>Payments Received</td>
                                                                <td> <?= check_status('payments'); ?></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">6</th>
                                                                <td>SMS sents</td>
                                                                <td> <?= check_status('sms'); ?>
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">7</th>
                                                                <td>Expenses</td>
                                                                <td> <?= check_status('expense', ' WHERE refer_expense_id in (select id from ' . $schema . '.refer_expense where financial_category_id in (2,3)) '); ?>
                                                                    <br />
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row" colspan="3">
                                                                    <div class="label-main">
                                                                        <label class="label label-info">Stage
                                                                            2</label>
                                                                    </div>
                                                                </th>

                                                            </tr>
                                                            <tr>
                                                                <th scope="row">1</th>
                                                                <td>Payroll Usage</td>
                                                                <td> <?= check_status('salaries'); ?></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">2</th>
                                                                <td>Electronic Payments</td>
                                                                <td>
                                                                    Integration Date:
                                                                    <?= check_status('bank_accounts_integrations'); ?><br />
                                                                    Last Online Transaction Date:
                                                                    <?= check_status('payments', ' WHERE token is not null'); ?>
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">3</th>
                                                                <td>Inventory Usage</td>
                                                                <td>Vendors Registered:
                                                                    <?= check_status('admin.vendors', "WHERE schema_name='" . $schema . "'"); ?><br />
                                                                    Items
                                                                    Registered:<?= check_status('product_alert_quantity'); ?>

                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">4</th>
                                                                <td>Other Transactions</td>
                                                                <td>
                                                                    Revenue:
                                                                    <?= check_status('revenues', ' WHERE refer_expense_id in (select id from ' . $schema . '.refer_expense where financial_category_id=1) '); ?>
                                                                    <br />

                                                                    Capital :
                                                                    <?= check_status('revenues', ' WHERE refer_expense_id in (select id from ' . $schema . '.refer_expense where financial_category_id=7) '); ?><br />
                                                                    Fixed Assets:
                                                                    <?= check_status('expense', ' WHERE refer_expense_id in (select id from ' . $schema . '.refer_expense where financial_category_id=4) '); ?><br />
                                                                    Liabilities :
                                                                    <?= check_status('expense', ' WHERE refer_expense_id in (select id from ' . $schema . '.refer_expense where financial_category_id=6) '); ?><br />
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row" colspan="3">
                                                                    <div class="label-main">
                                                                        <label class="label label-success">Stage
                                                                            3</label>
                                                                    </div>
                                                                </th>

                                                            </tr>
                                                            <tr>
                                                                <th scope="row">1</th>
                                                                <td>Library Usage</td>
                                                                <td>
                                                                    Books Added:
                                                                    <?= check_status('book'); ?><br />
                                                                    Book Issue: <?= check_status('issue'); ?>

                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">1</th>
                                                                <td>Attendance Usage</td>
                                                                <td>
                                                                    Student:
                                                                    <?= check_status('sattendances'); ?>
                                                                    <br />
                                                                    Teacher: <?= check_status('tattendance'); ?>
                                                                    <br />
                                                                    Exam: <?= check_status('eattendance'); ?>
                                                                    <br />
                                                                    Teacher on Duty:
                                                                    <?= check_status('teacher_duties'); ?>
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">2</th>
                                                                <td>Routine Usage</td>
                                                                <td> <?= check_status('routine'); ?></td>
                                                                <td></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                            
                                            <script src="https://code.highcharts.com/highcharts.js">
                                            </script>
                                            <script src="https://code.highcharts.com/modules/data.js">
                                            </script>
                                            <script type="text/javascript">
                                            graph_disc = function() {

                                                Highcharts.chart('container_log', {
                                                    chart: {
                                                        type: 'column'
                                                    },
                                                    title: {
                                                        text: "Number of System User Login by Day."
                                                    },
                                                    subtitle: {
                                                        text: ''
                                                    },
                                                    xAxis: {
                                                        type: 'category'
                                                    },
                                                    yAxis: {
                                                        title: {
                                                            text: 'Number of Logins'
                                                        }

                                                    },
                                                    legend: {
                                                        enabled: false
                                                    },
                                                    plotOptions: {
                                                        series: {
                                                            borderWidth: 0,
                                                            dataLabels: {
                                                                enabled: true,
                                                                format: '{point.y:.1f}'
                                                            }
                                                        }
                                                    },
                                                    tooltip: {
                                                        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                                                        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
                                                    },
                                                    series: [{
                                                        name: 'Number of Logins',
                                                        colorByPoint: true,
                                                        data: [
                                                            <?php
                                                                            $setting = "'setting'";
                                                                        $logins = DB::select('select count(user_id), created_at::date as month from '.$schema.'.login_locations where user_id is not null AND  "table" != '.$setting.' and extract(year from created_at) = ' . date('Y') . '  group by created_at::date order by created_at::date desc limit 10');
                                                                        if (!empty($logins)) {
                                                                        foreach ($logins as $log) {
                                                                           // $dateObj = DateTime::createFromFormat('!m', $log->month);
                                                                           // $month = $dateObj->format('F');
                                                                            ?> {
                                                                name: '<?=$log->month."<br><b>".date("l", strtotime($log->month))."</b>" ?>',
                                                                y: <?php echo $log->count; ?>,
                                                                drilldown: ''
                                                            },
                                                            <?php
                                                                        }
                                                                        }
                                                                        ?>
                                                        ]
                                                    }]
                                                });

                                            }
                                            $(document).ready(graph_disc);
                                            </script>

                                            <hr>
                                           
                                    </div>

                                    <!-- Photos tab end -->
                                    <!-- Friends tab start -->
                                    <div class="tab-pane" id="friends" aria-expanded="false">
                                            <div class="card">
                                                <div class="card-header">
                                                    <h3>List of School Staffs</h3>
                                                </div>
                                                <div class="card-block">
                                                
                                                <table class="table dataTable">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Name</th>
                                                            <th>Phone</th>
                                                            <th>Email</th>
                                                            <th>Title</th>
                                                            <th>View</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                            $users = DB::table($schema . '.user')->where('status', 1)->get();
                                                            if (count_chars($users)) {
                                                                foreach ($users as $user) {
                                                                    ?>
                                                        <tr>
                                                            <td></td>
                                                            <td><?= $user->name ?></td>
                                                            <td><?= $user->phone ?></td>
                                                            <td><?= $user->email ?></td>
                                                            <td><?= $user->usertype ?></td>
                                                            <td><a href="<?= url('Users/staff_profile/'.$schema.'/' . $user->userID)?>"  class="btn btn-success btn-sm btn-out-dotted" data-original-title="View Student Report"><i class="fa fa-file"> </i> view</a></td>
                                                        </tr>
                                                        <?php
                                                                }
                                                            }
                                                            ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                               
                               
                                    <!-- Friends tab end -->
                                    <div class="tab-pane" id="payments" aria-expanded="false">

                                            <div class="card">
                                                <button type="button" class="btn btn-primary waves-effect"
                                                    data-toggle="modal" data-target="#standing-order-Modal">
                                                    Standing Order </button>
                                            </div>

                                            <div class="modal fade" id="standing-order-Modal" tabindex="-1"
                                                role="dialog" aria-hidden="true" style="z-index: 1050; display: none;">
                                                <div class="modal-dialog modal-lg" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">Add Standing Order</h4>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                                <span aria-hidden="true">×</span>
                                                            </button>
                                                        </div>
                                                        <form action="{{ url('Customer/addStandingOrder') }}"
                                                            method="post" enctype="multipart/form-data">
                                                            <div class="modal-body">

                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <strong> Branch name </strong>
                                                                            <select name="branch_id" required
                                                                                class="form-control select2">
                                                                                <?php
                                                                                        /*
                                                                                        $branches = \App\Models\PartnerBranch::orderBy('id','asc')->get();
                                                                                        if (!empty($branches)) {
                                                                                            foreach ($branches as $branch) {
                                                                                                ?>
                                                                                <option value="<?= $branch->id ?>">
                                                                                    <?= $branch->name ?>
                                                                                </option>
                                                                                <?php
                                                                                            }
                                                                                        } */
                                                                                        ?>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <strong> Contact person </strong>
                                                                            <select name="school_contact_id" required class="form-control select2">

                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <strong> Number of occurrence </strong>
                                                                            <input type="number" class="form-control" name="number_of_occurrence" required>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <strong> Basis </strong>
                                                                            <input type="text" class="form-control" placeholder="eg. Quarter" name="which_basis" required>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <strong> Total amount</strong>
                                                                            <input type="text" class="form-control" name="total_amount" required>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <strong> Amount for Every Occurrence</strong>
                                                                            <input type="text" class="form-control" name="occurance_amount" required>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <strong> Maturity Date</strong>
                                                                            <input type="date" class="form-control"
                                                                                name="maturity_date" required>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <strong> Standing order </strong>
                                                                            <input type="file" class="form-control"
                                                                                name="standing_order_file" required>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="modal-footer">
                                                                <button type="button"
                                                                    class="btn btn-default waves-effect "
                                                                    data-dismiss="modal">Close</button>
                                                                <button type="submit"
                                                                    class="btn btn-primary waves-effect waves-light ">Save
                                                                    changes</button>
                                                            </div>
                                                            <input type="hidden" value="<?= $client_id ?>"
                                                                name="client_id" />
                                                            <?= csrf_field() ?>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="card table-responsive">
                                                <table id="invoice_table"
                                                    class="table table-striped table-bordered nowrap dataTable">
                                                    <thead>
                                                        <tr>
                                                            <th>Client Name</th>
                                                            <th>Reference #</th>
                                                            <th>Amount</th>
                                                            <th>Paid Amount</th>
                                                            <th>Remained Amount</th>
                                                            <th>Due Date</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                                $total_amount = 0;
                                                                $total_paid = 0;
                                                                $total_unpaid = 0;
                                                                $i = 1;
                                                                foreach ($invoices as $invoice) {
                                                                $amount = $invoice->invoiceFees()->sum('amount');
                                                                $paid = $invoice->payments()->sum('amount');
                                                                $unpaid = $amount - $paid;
                                                                $total_paid += $paid;
                                                                $total_amount += $amount;
                                                                $total_unpaid += $unpaid;
                                                           ?>

                                                        <tr>
                                                            <td><?= $invoice->client->username ?></td>
                                                            <td><?= $invoice->reference ?></td>
                                                            <td><?= money($amount) ?></td>
                                                            <td><?= money($paid) ?></td>
                                                            <td><?= money($unpaid) ?></td>
                                                            <td><?= date('d M Y', strtotime($invoice->due_date)) ?>
                                                            </td>
                                                            <td>

                                                                <div class="dropdown-secondary dropdown f-right">
                                                                    <button
                                                                        class="btn btn-success btn-mini dropdown-toggle waves-effect waves-light"
                                                                        type="button" id="dropdown"
                                                                        data-toggle="dropdown" aria-haspopup="true"
                                                                        aria-expanded="false">Options</button>
                                                                    <div class="dropdown-menu"
                                                                        aria-labelledby="dropdown6"
                                                                        data-dropdown-in="fadeIn"
                                                                        data-dropdown-out="fadeOut"><a
                                                                            class="dropdown-item waves-light waves-effect"
                                                                            href="<?= url('account/invoiceView/' . $invoice->id) ?>"><span
                                                                                class="point-marker bg-danger"></span>View</a>
                                                                        <a class="dropdown-item waves-light waves-effect"
                                                                            href="<?= url('account/invoice/edit/' . $invoice->id) ?>"><span
                                                                                class="point-marker bg-warning"></span>Edit</a><a
                                                                            class="dropdown-item waves-light waves-effect"
                                                                            href="<?= url('account/invoice/delete/' . $invoice->id) ?>"><span
                                                                                class="point-marker bg-warning"></span>Delete</a>
                                                                        <?php if ((int) $unpaid > 0) { ?>
                                                                        <hr />
                                                                        <a class="dropdown-item waves-light waves-effect"
                                                                            href="<?= url('account/payment/' . $invoice->id) ?>"><span
                                                                                class="point-marker bg-warning"></span>Add
                                                                            Payments</a>
                                                                        <?php }  ?>
                                                                        <?php if((int) $unpaid >0){ ?>
                                                                        <a class="dropdown-item waves-light waves-effect"
                                                                            href="#" data-toggle="modal"
                                                                            data-target="#large-Modal"
                                                                            onclick="$('#invoice_id').val('<?=$invoice->id?>')"><span
                                                                                class="point-marker bg-warning"></span>Send
                                                                            Invoice</a>
                                                                        <?php }  ?>
                                                                        <?php if((int) $paid >0){ ?>
                                                                        <a class="dropdown-item waves-light waves-effect"
                                                                            href="<?= url('account/receipts/' . $invoice->id) ?>"
                                                                            target="_blank"><span
                                                                                class="point-marker bg-warning"></span>Receipt</a>
                                                                        <?php }
                                                                             ?>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <?php $i++; } ?>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td colspan="2">Total</td>
                                                            <td><?= money($total_amount) ?></td>
                                                            <td><?= money($total_paid) ?></td>
                                                            <td><?= money($total_unpaid) ?></td>
                                                            <td colspan="2"></td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                <!-- Row end -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Page-body end -->
</div>
</div>
<div class="card-block">
    <div class="modal fade" id="status-Modal" tabindex="-1" role="dialog" aria-hidden="true"
        style="z-index: 1050; display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Change Schools Status</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="<?= url('customer/schoolStatus') ?>" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="hidden" value="<?= $schema ?>" name="schema_name" />
                        </div>
                        <div class="form-group">
                            School <?= ucfirst($schema) ?> status
                            <select name="status" class="form-control select2">
                                <option value="">Select status</option>
                                <option value="1">Active Paid</option>
                                <option value="2">Active</option>
                                <option value="3">Resale</option>
                                <option value="4">Inactive</option>
                            </select>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light ">Save changes</button>
                    </div>
                    <?= csrf_field() ?>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="customer_contracts_model" tabindex="-1" role="dialog" style="z-index: 1050; display: none;"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Upload Contract</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p align='center'><span class="label label-danger">Once you upload a contract, you cannot EDIT</span>
                </p>
                <form action="<?= url('customer/contract/' . $client_id) ?>" method="POST"
                    enctype="multipart/form-data">



                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Contract Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="name" required="">
                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Agreement Type</label>
                        <div class="col-sm-10">
                            <select name="contract_type_id" class="form-control">

                                <?php
                            $ctypes = DB::table('admin.contracts_types')->get();
                            if (!empty($ctypes)) {
                                foreach ($ctypes as $ctype) {
                                    ?>
                                <option value="<?= $ctype->id ?>"><?= $ctype->name ?></option>
                                <?php
                                }
                            }
                            ?>

                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Contract Start Date</label>
                        <div class="col-sm-10">
                            <input type="date" class="form-control" value="" name="start_date" required="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Contract Start Date</label>
                        <div class="col-sm-10">
                            <input type="date" class="form-control" name="end_date" required="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Upload Document</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control" accept=".pdf" name="file" required="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Notes</label>
                        <div class="col-sm-10">
                            <textarea rows="5" cols="5" name="description" class="form-control"
                                placeholder="Any important details about this document"></textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label"></label>
                        <div class="col-sm-10">
                            <?= csrf_field() ?>
                            <button type="submit" class="btn btn-success" placeholder="Default textarea">Submit</button>
                        </div>
                    </div>
                </form>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
<?php $root = url('/') . '/public/' ?>
<?php
if (!empty($profile)) {
?>
<div class="modal fade" id="school_details" tabindex="-1" role="dialog" style="z-index: 1050; display: none;"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="card-block">

                    <div id="view-info" class="row">
                        <div class="col-lg-12 col-md-12">
                            <form action="<?= url('customer/contract/' . $client_id) ?>" method="POST"
                                enctype="multipart/form-data">
                                <table class="table m-b-0">
                                    <tbody>
                                        <tr>
                                            <th class="social-label b-none p-t-0">School Name
                                            </th>
                                            <td class="social-user-name b-none p-t-0 text-muted">
                                                <?= $profile->school->name ?></td>
                                        </tr>
                                        <tr>
                                            <th>Region</th>
                                            <td >
                                                <?= $profile->school->region ?></td>
                                        </tr>
                                        <tr>
                                            <th>District</th>
                                            <td >
                                                <?= $profile->school->district ?></td>
                                        </tr>
                                        <tr>
                                            <th>Ward</th>
                                            <td ><?= $profile->school->ward ?>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                        </div>
                    </div>
                    </form>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Submit</button>
                    </div>
                </div>
                <br />
            </div>
        </div>

    </div>
</div>
<?php } ?>
<script type="text/javascript">
function save_comment(id) {
    var content = $('#task_comment' + id).val();
    var task_id = $('#task_id' + id).val();
    $.ajax({
        type: 'POST',
        url: "<?= url('customer/taskComment/null') ?>",
        data: {
            content: content,
            task_id: task_id
        },
        dataType: "html",
        success: function(data) {
            $('input[type="text"],textarea').val('');
            $('.new_comment' + id).after(data);
        }
    });
}

notify = function(title, message, type) {
    new PNotify({
        title: title,
        text: message,
        type: type,
        hide: 'false',
        icon: 'icofont icofont-info-circle'
    });
}

allocate = function(a, role_id) {
    $.ajax({
        url: '<?= url('customer/allocate/null') ?>',
        data: {
            user_id: a,
            school_id: '<?= $school->school_id ?>',
            role_id: role_id,
            schema: '<?= $schema ?>'
        },
        dataType: 'html',
        success: function(data) {
            $('#supportl').html(data);
        }
    });
}

show_tabs = function(a) {
    $('.live_tabs').hide(function() {
        $('#' + a).show();
    });
}

$('#school_id').click(function() {
    var val = $(this).val();
    $.ajax({
        url: '<?= url('customer/search/null') ?>',
        data: {
            val: val,
            type: 'school',
            schema: '<?= $schema ?>'
        },
        dataType: 'html',
        success: function(data) {

            $('#search_result').html(data);
        }
    });
});

removeTag = function(a) {
    $.ajax({
        url: '<?= url('customer/removeTag') ?>/null',
        method: 'get',
        data: {
            id: a
        },
        success: function(data) {
            if (data == '1') {
                $('#removetag' + a).fadeOut();
            }
        }
    });
}

task_group = function() {
    $('.task_group').change(function() {
        var val = $(this).val();
        var task_id = $(this).attr('data-task-id');
        var data_attr = $('#task_user' + task_id).val();
        $.ajax({
            url: '<?= url('customer/getAvailableSlot') ?>/null',
            method: 'get',
            data: {
                start_date: val,
                user_id: data_attr
            },
            success: function(data) {
                $('#start_slot' + task_id).html(data);
            }
        });
    });

    $('.task_school_group').blur(function() {
        var val = $(this).text();
        var data_attr = $(this).attr('data-attr');
        var task_id = $(this).attr('task-id');
        // var date=$('#'+task_id).val();
        $.ajax({
            url: '<?= url('customer/editTrain') ?>/null',
            method: 'get',
            dataType: 'html',
            data: {
                task_id: task_id,
                value: val,
                attr: data_attr
            },
            success: function(data) {
                // $(this).after(data).addClass('label label-success');
                notify('Success', 'Success', 'success');
            }
        });
    });

    $('.slot').change(function() {
        var val = $(this).val();
        //var data_attr = $(this).attr('data-attr');
        var task_id = $(this).attr('data-id');
        var date = $('#' + task_id).val();
        $.ajax({
            url: '<?= url('customer/editTrain') ?>/null',
            method: 'get',
            dataType: 'json',
            data: {
                task_id: task_id,
                value: date,
                slot_id: val,
                attr: 'start_date'
            },
            success: function(data) {
                $('#task_end_date_id' + data.task_id).html(data.end_date);
                notify('Success', 'Success', 'success');
            }
        });
    });

    $('.task_allocated_id').change(function() {
        var task_allocated_id = $(this).val();
        var task_id = $(this).attr('task-id');
        $.ajax({
            url: '<?= url('customer/editTrain') ?>/null',
            method: 'get',
            data: {
                task_id: task_id,
                user_id: task_allocated_id
            },
            success: function(data) {
                notify('Success', data, 'success');
            }
        });
    });
}

$(document).ready(task_group);
</script>
@endsection