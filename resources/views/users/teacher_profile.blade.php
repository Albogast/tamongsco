@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>

                                <div class="page-body">
                                    <!--profile cover start-->
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="cover-profile">
                                                <div class="profile-bg-img"> 
                                                    <img class="profile-bg-img img-fluid" style="height: 280px;" src="<?=$root?>/assets/images/shulesoft.JPG" alt="bg-img">
                                                    <div class="card-block user-info">
                                                        <div class="col-md-12">
                                                            <div class="media-left">
                                                            <?php  isset($teacher->photo) ? $teacher->photo:'';
                                                                if (strpos($teacher->photo, 'https:') !== false) {
                                                                    $src = $teacher->photo;
                                                                } elseif (is_file('storage/uploads/images/' . $teacher->photo)) {
                                                                    $image = 'storage/uploads/images/' . $teacher->photo;
                                                                    $src = url($image);
                                                                } else {
                                                                    $image = 'storage/uploads/images/defualt.png';
                                                                    $src = url($image);
                                                                }
                                    
                                                                ?>
                                                                <a href="#" class="profile-image">
                                                                    <img class="user-img img-radius" style="height: 150px;" src="<?php echo $src; ?>"  alt="ShuleSoft" title="{{$teacher->name}}">
                                                                </a>
                                                            </div>
                                                         
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--profile cover end-->
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <!-- tab header start -->
                                            <div class="tab-header card">
                                                <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist" id="mytab">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" data-toggle="tab" href="#personal" role="tab">Basic Info</a>
                                                        <div class="slide"></div>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" data-toggle="tab" href="#messages" role="tab">
                                                            Sent sms</a>
                                                        <div class="slide"></div>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" data-toggle="tab" href="#activities" role="tab">
                                                            Performance</a>
                                                        <div class="slide"></div>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" data-toggle="tab" href="#allocations" role="tab">Subject allocations</a>
                                                        <div class="slide"></div>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div class="tab-content">
                                                <div class="tab-pane active" id="personal" role="tabpanel">
                                                    <div class="card">
                                                        <div class="card-header">
                                                            <h3 class="card-header-text">About teacher</h3>
                                                        </div>
                                                        <div class="card-block">
                                                            <div class="view-info">
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="general-info">
                                                                            <div class="row">
                                                                                <div class="col-lg-12 col-xl-6">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table m-0">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <th scope="row">Full Name</th>
                                                                                                    <td><?= $teacher->name ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Gender</th>
                                                                                                    <td><?= $teacher->sex ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Birth Date</th>
                                                                                                    <td><?= customdate($teacher->dob); ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Address</th>
                                                                                                    <td><?= $teacher->address ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Joining Date</th>
                                                                                                    <td><?= customdate($teacher->jod) ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Bank name</th>
                                                                                                    <td><?= $teacher->bank_name ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Designation</th>
                                                                                                    <td><?= $teacher->designation ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Phone</th>
                                                                                                    <td><?= $teacher->phone?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Joining Date</th>
                                                                                                    <td><?= customdate($teacher->jod) ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Religion</th>
                                                                                                    <td><?= isset($teacher->religion) ?$teacher->religion : '' ?></td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- end of table col-lg-6 -->
                                                                                <div class="col-lg-12 col-xl-6">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <th scope="row">Id number</th>
                                                                                                    <td><?= $teacher->id_number ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Username</th>
                                                                                                    <td><?= $teacher->username ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Status</th>
                                                                                                    <td><?= $teacher->status == '1'? 'Active' : 'Inactive' ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">National ID</th>
                                                                                                    <td><?= $teacher->national_id ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Town</th>
                                                                                                    <td><?= $teacher->location?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Bank Account Number</th>
                                                                                                    <td><?= $teacher->bank_account_number ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Physical condition</th>
                                                                                                    <td><?= $teacher->physical ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Health status</th>
                                                                                                    <td><?= $teacher->h_name ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th scope="row">Email</th>
                                                                                                    <td><?= $teacher->email; ?></td>
                                                                                                </tr>
                                                                                              
                                                                                                <tr>
                                                                                                    <th scope="row">Health insurance</th>
                                                                                                    <td><?= $teacher->insurance_name ?></td>
                                                                                                </tr>
                                                                                              
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- end of table col-lg-6 -->
                                                                            </div>
                                                                            <!-- end of row -->
                                                                        </div>
                                                                        <!-- end of general info -->
                                                                    </div>
                                                                    <!-- end of col-lg-12 -->
                                                                </div>
                                                                <!-- end of row -->
                                                            </div>                                                         
                                                        </div>
                                                    </div>
                                                </div>



                                                

                                                 <div class="tab-pane" id="messages" role="tabpanel">
                                                     <div class="card">
                                                        <div class="card-block">
                                                            <div class="view-info">
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="general-info">
                                                                            <div class="row">
                                                                                <div class="col-lg-12 col-xl-12">
                                                                                    <div class="table-responsive">
                                                                                        <table id="" class="table table-striped table-bordered nowrap">
                                                                                            <?php
                                                                                                   if (isset($messages)) { 
                                                                                                       foreach ($messages as $message) {
                                                                                                           if ($message->is_sent == 1) {
                                                                                                               ?>
                                                                                                               <li>
                                                                                                                   <div class="message_date">
                                                                                                                       <h3 class="date text-info"><?= $message->is_sent == 1 ? 'sent' : 'received' ?></h3>
                                                                                                                       <p class="month"><?= timeAgo($message->created_at) ?></p>
                                                                                                                   </div>
                                                                                                                   <div class="message_wrapper">
                                                                                                                       <h5 class="heading"><?= isset($message->user_id) ? teacher($message->user_id,$this_schema)->name : '' ?></h5>
                                                                                                                       <blockquote class="message"><?= $message->body ?>
                                                                                                                       </blockquote>
                                                                                                                       <br>
                                                                                                                       <p class="url">
                                                                                                                           <span class="fs1 text-info" aria-hidden="true" data-icon=""></span>
                                                                                                                           <a href="#"></a>
                                                                                                                       </p>
                                                                                                                   </div>
                                                                                                               </li>
                                                   
                                                                                                           <?php } else { ?>
                                                                                                               <li>
                                                                                                                   <a style="text-decoration:none">
                                                                                                                       <h3 class="date text-info text-right"><br/>
                                                                                                                           <?= timeAgo($message->created_at) ?></h3>
                                                                                                                       <br/>
                                                                                                                       <div class="message_date">
                                                                                                                          
                                                                                                                       </div>
                                                                                                                       <div class="message_wrapper text-right">
                                                                                                                           <h6 class="heading"></h6>
                                                                                                                           <blockquote class="message text-right"><?= $message->body ?></blockquote>
                                                                                                                           <br>
                                                   
                                                                                                                       </div>
                                                                                                                   </a>
                                                                                                               </li>  
                                                   
                                                                                                               <?php
                                                                                                           }
                                                                                                       }
                                                                                                   }
                                                                                                   ?>
                                                                                               </ul>
                                                                                           
                                                                                       </table>
                                                                                    </div>
                                                                             
                                                                            </div> 
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                                
                                                
                                            
                                                <div class="tab-pane" id="activities" role="tabpanel">
                                                    <div class="row">
                                                        <div class="col-xl-12">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <!-- contact data table card start -->
                                                                    <div class="card">
                                                                        <div class="card-header">
                                                                            <h5 class="card-header-text">Table of Examination Performance</h5>
                                                                        </div>
                                                                        <div class="card-block contact-details">
                                                                            <div class="data_table_main table-responsive dt-responsive">
                                                                                <table id="simpletable" class="table  table-striped table-bordered nowrap">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>#</th>
                                                                                            <th>Exam name</th>
                                                                                            <th>Subject name</th>
                                                                                            <th>Class name</th>
                                                                                            <th>Year</th>
                                                                                            <th>Max score</th>
                                                                                            <th>Min score</th>
                                                                                            <th>Average</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        <?php $i=1;if(isset($avg_perfomance)) { ?>
                                                                                        <?php foreach ($avg_perfomance as $avg) { ?>
                                                                                          <tr>
                                                                                            <th><?=$i?></th>
                                                                                            <th><?=$avg->exam?></th>
                                                                                            <th><?=$avg->subject_name?></th>
                                                                                            <th><?=$avg->classes?></th>
                                                                                            <th><?=$avg->academic_year?></th>
                                                                                            <th><?=to2decimal($avg->max)?></th>
                                                                                            <th><?=to2decimal($avg->min)?></th>
                                                                                            <th><?=to2decimal($avg->avg)?></th>
                                
                                                                                          </tr>
                                                                                        <?php $i++;} ?>
                                                                                        <?php } ?>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                               
                                                <div class="tab-pane" id="allocations" role="tabpanel">
                                                    <div class="row">
                                                              <div class="col-sm-12">
                                                                  <!-- contact data table card start -->
                                                                  <div class="card">
                                                                      <div class="card-header">
                                                                          <h4 class="card-header-text">Subjects allocation</h4>
                                                                      </div>
                                                                      <?php if(!empty($subjects_allocated)) { ?>
                                                                      <div class="card-block contact-details">
                                                                          <div class="data_table_main table-responsive dt-responsive">
                                                                              <table id="simpletable" class="table  table-striped table-bordered nowrap">
                                                                                  <thead>
                                                                                      <tr>
                                                                                          <th>#</th>
                                                                                          <th class="text-left">Subject Name</th>
                                                                                          <th class="text-left">Class name</th>
                                                                                          <th class="text-center">Academic Year </th>
                                                                                      </tr>
                                                                                  </thead>
                                                                                  <tbody>
                                                                                      
                                                                                      <?php $i=1; foreach ($subjects_allocated as $subject) { ?>
                                                                                        <tr>
                                                                                          <th class="text-left"><?= $i?></th>
                                                                                          <th class="text-left"><?=strtoupper($subject->subject) ?></th>
                                                                                          <th class="text-left"><?=strtoupper($subject->classes) ?></th>
                                                                                          <th class="text-center"><?= $subject->year?></th>
                                                                                        </tr>
                                                                                      <?php $i++;} ?>
                                                                                  </tbody>
                                                                              </table>
                                                                          </div>
                                                                      </div>
                                                                      <?php } ?>
                                                                  </div>
                                                              </div>
                                                           </div>
                                                       </div>

                                              </div>
                                         </div>
                                      </div>
                                  </div>
                         @endsection