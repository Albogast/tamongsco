@extends('layouts.app')

@section('content')
<!-- Page-body start -->
<?php $root = url('/') . '/public/' ?>

<div class="page-body">
    <!--profile cover start-->
    <div class="row">
            
    </div>
    <!--profile cover end-->
    <div class="row">
        <div class="col-lg-12">
            <!-- tab header start -->
         
            <!-- tab header end -->
            <!-- tab content start -->
            <div class="tab-content">
                <!-- tab panel personal start -->
                <div class="tab-pane active" id="personal" role="tabpanel">
                    <!-- personal card start -->
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">School Profile</h5>
                            
                        </div>
                        <div class="card-block">
                            <div class="view-info">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="general-info">
                                            <div class="row">
                                                <div class="col-lg-12 col-xl-6">
                                                    <div class="table-responsive">
                                                        <table class="table m-0">
                                                            <tbody>
                                                                <tr>
                                                                    <th scope="row">School Name</th>
                                                                    <td>{{ $school->name }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Type</th>
                                                                    <td>{{ ucfirst($school->type) }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Reg Number</th>
                                                                    <td>{{ $school->number }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Ownership</th>
                                                                    <td>{{ $school->ownership }}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <!-- end of table col-lg-6 -->
                                                <div class="col-lg-12 col-xl-6">
                                                    <div class="table-responsive">
                                                        <table class="table">
                                                            <tbody>
                                                                <tr>
                                                                    <th scope="row">Region</th>
                                                                    <td>{{ $school->region }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">District</th>
                                                                    <td>{{ $school->district }}</td>
                                                                </tr>
                                                               
                                                                <tr>
                                                                    <th scope="row">Ward</th>
                                                                    <td>{{ $school->ward }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Website</th>
                                                                    <td>{{ $school->ownership }}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <!-- end of table col-lg-6 -->
                                            </div>
                                            <!-- end of row -->
                                        </div>
                                        <!-- end of general info -->
                                    </div>
                                    <!-- end of col-lg-12 -->
                                </div>
                                <!-- end of row -->
                            </div>
                            <!-- end of view-info -->
                            
                        </div>
                        <!-- end of card-block -->
                    </div>
                    <div class="row">
                      
        <div class="col-md-12">
            <div class="card table-card">
                <div class="card-header">
                    <h5>School Staffs</h5>
                    <div class="card-header-right">
                        <ul class="list-unstyled card-option">
                            <li><i class="feather icon-maximize full-card"></i></li>
                            <li><i class="feather icon-minus minimize-card"></i></li>
                            <li><i class="feather icon-trash-2 close-card"></i></li>
                        </ul>
                    </div>
                </div>
                <div class="card-block">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                <th>S/N</th>
                                    <th> Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Title</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i=1; ?>
                            @foreach ($clients as $value)
                                <tr>
                                    <td><?=$i++?></td>
                                    <td>{{ $value->name }} </td>
                                    <td>{{ $value->email }} </td>
                                    <td>{{ $value->phone }} </td>
                                    <td>{{ $value->title }} </td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- latest activity end -->
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="card-header-text">Google Map Location</h5>
                                    <div class="card-header-right">
                                        <ul class="list-unstyled card-option">
                                            <li><i class="feather icon-maximize full-card"></i></li>
                                            <li><i class="feather icon-minus minimize-card"></i></li>
                                            <li><i class="feather icon-trash-2 close-card"></i></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <iframe src="http://maps.google.com/maps?q=<?=$school->latitude.','.$school->langitude?>&z=15&output=embed"  style="height:400px;width: 100%;"></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    <!-- personal card end-->
                </div>
            </div>
<!-- Page-body end -->
@endsection
