@extends('layouts.app')

@section('content')
<?php $root = url('/public/'); ?>
<!-- Page-body start -->
<div class="page-body">
    <div class="row">
        <!-- customar project  start -->
        <div class="col-xl-12 col-md-12" >
        <div class="card">
            <h3 align="center">Tanzania Association of Managers and Owners of Non-Government Schools and Colleges </h3>
        </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card">
                <div class="card-block">
                    <div class="row align-items-center m-l-0">
                        <div class="col-auto">
                            <i class="feather icon-users f-30 text-c-pink"></i>
                        </div>
                        <div class="col-auto">
                            <a href="{{ url('users') }}">
                                <h6 class="text-muted m-b-10"> School Contacts </h6>
                                <h2 class="m-b-0"> {{ $our_schools }}</h2>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card">
                <div class="card-block">
                    <div class="row align-items-center m-l-0">
                        <div class="col-auto">
                            <i class="feather icon-layers f-30 text-c-pink"></i>
                        </div>
                        <div class="col-auto">
                            <a href="{{ url('users') }}">
                                <h6 class="text-muted m-b-10"> TAMONGSCO Schools </h6>
                                <h2 class="m-b-0"> {{ $our_schools }}</h2>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card">
                <div class="card-block">
                    <div class="row align-items-center m-l-0">
                        <div class="col-auto">
                        <i class="feather icon-sliders f-30 text-c-pink"></i>
                        </div>
                        <div class="col-auto">
                            <a href="{{ url('users') }}">
                                <h6 class="text-muted m-b-10"> All Private Schools </h6>
                                <h2 class="m-b-0"> {{ $schools }}</h2>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-3 col-md-6">
            <div class="card">
                <div class="card-block">
                    <div class="row align-items-center m-l-0">
                        <div class="col-auto">
                            <i class="feather icon-message-square f-30 text-c-pink"></i>
                        </div>
                        <div class="col-auto">
                            <a href="{{ url('users') }}">
                                <h6 class="text-muted m-b-10"> Total Messages </h6>
                                <h2 class="m-b-0"> {{ $users }}</h2>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- user start -->
        <div class="col-xl-6 col-md-12">
            <div class="card table-card">
                <div class="card-header">
                <h5>Number of Schools per Regions</h5>
                </div>
                <div class="card-block">
                    <div class="table-responsive">
                        <table class="table table-hover table-borderless">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Region</th>
                                    <th>Schools</th>
                                    <th>Contacts</th>
                                    <th class="text-center">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $i = 1;
                            ?>
                                @if (count($regions ) > 0)
                                @foreach ($regions as $this_schema)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $this_schema->name }}</td>
                                    <td>{{ $this_schema->schools()->count() }}</td>
                                    <td>{{  \App\Models\Member::whereIn('school_id', $this_schema->schools()->get(['id']))->count() }}</td>
                                    
                                    <td class="text-center"> <a
                                            href="{{ url('dashboard/school/'.$this_schema->id) }}"
                                            class="btn btn-info btn-sm btn-out-dotted"> View </a> </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                            <tfoot>
                          
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- user start -->
        <div class="col-xl-6 col-md-12">
            <div class="card table-card">
                <div class="card-header">
                    <h5>Number of Schools per Regions</h5>
                </div>
                <div class="card-block">
                    <div class="table-responsive">
                        <table class="table table-hover table-borderless">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Region</th>
                                    <th>Schools</th>
                                    <th>Contacts</th>
                                    <th class="text-center"> Profile </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $total_male = 0;
                                $total_female = 0;
                            ?>
                                @if (count($all_regions) > 0)
                                @foreach ($all_regions as $region)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $region->name }}</td>
                                    <td>{{ $region->schools()->count() }}</td>
                                    <td>{{  \App\Models\Member::whereIn('school_id', $region->schools()->get(['id']))->count() }}</td>
                                    
                                    <td class="text-center"> <a
                                            href="{{ url('dashboard/school/'.$region->id) }}"
                                            class="btn btn-info btn-sm btn-out-dotted"> View </a> </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                            <tfoot>
                          
                            </tfoot>
                        </table>

            </div>
        </div>
        <!-- user end -->


        </div>
        </div>
        <!-- user end -->



        <div class="col-md-12 col-lg-12">
            <div class="card">
                <div class="card-block text-center">
                    <!-- <i class="feather icon-mail text-c-lite-green d-block f-40"></i> -->
                    <h4 class="m-t-5"><span class="text-c-lite-green">© {{ date('Y') }} INETS Company Limited. All rights reserved.</span> </h4>
                    <!-- <p class="m-b-5">Your main list is growing</p> -->
                </div>
            </div>
        </div>
       
        <!-- subscribe end -->

        <!-- order  end -->
    </div>
</div>
<!-- Page-body end -->

@endsection
