@extends('layouts.app')

@section('content')
<?php $root = url('/public/'); ?>
<link rel="stylesheet" href="<?=$root?>/bower_components/select2/css/select2.min.css">

<!-- Page body start -->
<div class="page-body">
    <div class="row">
        <div class="col-sm-12">
            <!-- Job application card start -->
            <div class="card">
                <div class="card-header">
                    <h5>Add New School Contact Staff</h5>
                    <!-- <span>Explain what is Happening</span> -->
                </div>
                <div class="card-block">
                    <form id="main" method="post" action="" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Staff Name</label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control form-control-danger" id="name" name="name" placeholder="Enter fullname" required="">
                                <span class="messages"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Staff Phone</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="phone" name="phone" placeholder="e.g. +255743000109" required="">
                                <span class="messages"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Staff Email</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" placeholder="email@domain.com" id="email" name="email" required="">
                                <span class="messages"></span>
                            </div>
                        </div>
                        <!-- <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Staff Address</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" placeholder="P.O Box 111, Dodoma, Tanzania" id="email" name="address" required="">
                                <span class="messages"></span>
                            </div>
                        </div> -->
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Select Staff Role</label>
                            <div class="col-sm-10">
                            <select name="title" id="part_id" class="form-control">
                                    <option value="Director">Director/Owner</option>
                                    <option value="Manager">School Manager</option>
                                    <option value="Head teacher">Head Teacher</option>
                                    <option value="Second Master/Mistress">Second Master/Mistress</option>
                                    <option value="Academic master">Academic Master</option>
                                    <option value="Teacher">Normal Teacher</option>
                                    <option value="Accountant">Accountant</option>
                                    <option value="Other Staff">Other Non Teaching Staff</option>
                            </select>
                                <span class="messages"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Select Member Region </label>
                            <div class="col-sm-10">
                            <select name="" id="region_id" class="form-control select2">
                                <option value="">Select Here...</option>
                                @foreach ($regions as $class)
                                    <option value="{{ $class->id }}"> {{ $class->name }} </option>
                                @endforeach
                               
                            </select>
                                <span class="messages"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Select School</label>
                            <div class="col-sm-10">
                            <select name="school_id" id="school_id" class="form-control select2">
                            </select>
                                <span class="messages"></span>
                            </div>
                        </div>
                        

                        <div class="form-group row">
                            <label class="col-sm-2"></label>
                            <div class="col-sm-5">
                                <button type="submit" class="btn btn-primary m-b-0 ">Submit</button>
                            </div>
                            <div class="col-sm-5">
                            <a data-toggle="modal" data-target="#uploads" class="btn btn-primary btn-out-dotted f-right"> <i class="ti-user"></i> Upload By Excel </a>
                                
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Job application card end -->
    </div>
</div>
</div>
<!-- Page body end -->
  <!-- Required Jquery -->
  <script type="text/javascript" src="<?=$root?>/bower_components/jquery/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?=$root?>/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<script>
$('#region_id').change(function(event) {
        var id = $(this).val();
        if (id === '0') {
            $('#school_id').val(0);
        } else {
            $.ajax({
                type: 'POST',
                url: "<?= url('dashboard/loadSchool') ?>",
                data: "_token=" + "{{ csrf_token() }}" + "&id=" + id,
                dataType: "html",
                success: function(data) {
                    $('#school_id').html(data);
                }
            });
        }
    });
</script>

<div class="modal hide fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" id="uploads">
                <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title text-center">
                          Upload School Staffs by Excel
                      </h4>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>
                    <form action="<?=url('users/uploadMembers')?>" method="POST"  enctype="multipart/form-data">
                    <div class="modal-body">
                    
                <div class="col-xs-12 col-sm-12 col-md-12">
                  <div class="form-group">
                  <input type="hidden" class="form-control" value="<?=Auth::User()->id?>" name="user_id">
                  </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                  <div class="form-group">
                    <strong>Add Details About This Message:</strong>
                    <input type="file" name="call_file" id="" class="form-control" >
                  </div>
                </div>
                    </div>
                    <div class="modal-footer">
                    <button type="submit" class="btn btn-primary waves-effect waves-light "> <i class="ti-comments"> </i> Send</button>
                    </div>
                    <?= csrf_field() ?>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        
@endsection
