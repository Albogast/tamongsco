@extends('layouts.app')

@section('content')

<!-- Page-body start -->
<div class="page-body">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-block">
                    <h4>Add New School Details Here. Fill all Details Properly</h4>
                    @if (sizeof($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <form method="post" action="" enctype='multipart/form-data'>
                        {{ csrf_field() }}
                        <div class="card-block">
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <strong>School Name:</strong>
                                    <input type="text" class="form-control" name="name" required>
                                </div>
                                <div class="col-md-6">
                                    <strong>Type:</strong>
                                    <select name="type" id="type" class="form-control">
                                    <option value="secondary">Secondary School</option>
                                    <option value="college">College</option>
                                    <option value="primary">Primary School</option>
                            </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">

                                    <div class="col-md-6">
                                        <strong>Ownership:</strong>
                                        <select name="ownership" id="part_id" class="form-control">
                                            <option value="Non Government">Non Government</option>
                                            <option value="Government">Government</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <strong>Reg No *</strong>
                                        <input type="text" class="form-control" placeholder="" name="number" required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6">
                                    <strong>Region*</strong>
                                    <select id="schema" name="region" class="form-control form-control-primary">
                                    <option value="">Select One Region</option>
                                            @if (count($regions) > 0)
                                            @foreach ($regions as $schema)
                                                <option value="{{ strtoupper($schema->name) }}"> {{ strtoupper($schema->name) }}</option>
                                            @endforeach
                                            @endif
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <strong>District*</strong>
                                    <input type="text" class="form-control" placeholder="" name="district" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6">
                                    <strong>Ward:</strong>
                                    <input type="text" class="form-control" name="ward">
                                </div>
                                <div class="col-md-6">
                                    <strong>Students:</strong>
                                    <input type="number" class="form-control" placeholder="0" name="student">

                                </div>
                               
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-primary">
                                            &emsp; Submit &emsp;
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                </div>

                </form>

            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>

<script>
    $(".select2").select2({
        theme: "bootstrap",
        dropdownAutoWidth: false,
        allowClear: false,
        debug: true
    });

</script>

@endsection
