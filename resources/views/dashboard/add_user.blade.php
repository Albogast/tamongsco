@extends('layouts.app')

@section('content')
<?php $root = url('/public/'); ?>
<link rel="stylesheet" href="<?=$root?>/bower_components/select2/css/select2.min.css">

<!-- Page body start -->
<div class="page-body">
    <div class="row">
        <div class="col-sm-12">
            <!-- Job application card start -->
            <div class="card">
                <div class="card-header">
                    <h5>Add New Portal User</h5>
                    <!-- <span>Explain what is Happening</span> -->
                </div>
                <div class="card-block">
                    <form id="main" method="post" action="" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Staff Name</label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control form-control-danger" id="name" name="name" placeholder="Enter fullname" required="">
                                <span class="messages"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Staff Phone</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="name" name="phone" placeholder="e.g. +255743000109" required="">
                                <span class="messages"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Staff Email</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" placeholder="email@domain.com" id="email" name="email" required="">
                                <span class="messages"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Staff Address</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" placeholder="P.O Box 111, Dodoma, Tanzania" id="email" name="address" required="">
                                <span class="messages"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Select Staff Role</label>
                            <div class="col-sm-10">
                            <select name="role_id" id="part_id" class="form-control">
                            @foreach ($roles as $class)
                                    <option value="{{ $class->id }}"> {{ $class->name }}</option>
                                @endforeach
                                                             
                            </select>
                                <span class="messages"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Select Nationality</label>
                            <div class="col-sm-10">
                            <select name="national_id" id="part_id" class="form-control select2">
                                @foreach ($countries as $class)
                                    <option value="{{ $class->id }}"> {{ $class->country }} ({{ $class->country_code }})</option>
                                @endforeach
                               
                            </select>
                                <span class="messages"></span>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Staff Photo</label>
                            <div class="col-sm-10">
                                <input type="file" class="form-control" id="email" name="photo">
                                <span class="messages"></span>
                            </div>
                        </div>
                        <!-- <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Your Message</label>
                            <div class="col-sm-10">
                                <textarea name="body" class="form-control" placeholder="Write here.."
                                    cols="30" rows="10"></textarea>
                            </div>
                        </div> -->

                        <div class="form-group row">
                            <label class="col-sm-2"></label>
                            <div class="col-sm-5">
                                <button type="submit" class="btn btn-primary m-b-0 ">Submit</button>
                            </div>
                            <!-- <div class="col-sm-5">
                                <a href="{{ url('Dashboard/help/view') }}" class="btn btn-success btn-out-dotted"
                                    style="float: right;">View Issues</a>
                            </div> -->
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Job application card end -->
    </div>
</div>
</div>
<!-- Page body end -->

@endsection
