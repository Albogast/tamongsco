@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<!-- Page-body start -->
<div class="page-body">
    <div class="card">
        <!-- Email-card start -->
        <div class="card-block email-card">
            <div class="row">
                <div class="col-lg-12 col-xl-3">
                    <div class="user-head row">
                        <div class="user-face">
                            <h3 class="text-white">Send Message</h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-xl-9">
                    <div class="mail-box-head row">
                        <div class="col-md-12">
                            <a href="{{ url('users/viewMessage') }}" class="btn btn-info" style="float: right;">
                                <b>View Sent Message</b> </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- Right-side section start -->
                <div class="col-lg-12 col-xl-12">
                    <div class="mail-body">
                        <div class="mail-body">

                            <div class="mail-body-content">
                                <form id="main" method="post" action="">
                                    @csrf
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Select Module</label>
                                        <div class="col-sm-10">
                                            <select class="form-control js-example-basic-single">
                                                <option value="quick-sms">Quick SMS</option>
                                                <option value="whatsapp">WhatsApp</option>
                                                <option value="telegram">Telegram</option>
                                                <option value="phone-sms">Phone SMS</option>
                                                <option value="email">Email</option>
                                            </select>
                                            <span class="messages"></span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Select Regions</label>
                                        <div class="col-sm-10">
                                            <select name="region_id[]" id="part_id" class="form-control js-example-basic-single" multiple="">
                                                <option value="1000">All Regions</option>
                                                @foreach ($parts as $class)
                                                <option value="{{ $class->id }}"> {{ $class->name }}</option>
                                                @endforeach

                                            </select>
                                            <span class="messages"></span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Your Message</label>
                                        <div class="col-sm-10">
                                            <textarea name="body" class="form-control form-control-info"
                                                placeholder="Write here.." cols="30" rows="10"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-2"></label>
                                        <div class="col-sm-5">
                                            <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                                        </div>
                                        <div class="col-sm-5">

                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Right-side section end -->
            </div>
        </div>
    </div>
</div>
<!-- Page-body end -->
</div>
</div>
<!-- Main-body end -->
<script>
    // In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>
@endsection
