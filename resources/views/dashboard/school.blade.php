@extends('layouts.app')

@section('content')
<!-- Page-body start -->
<?php $root = url('/') . '/public/' ?>

<div class="page-body">
<div class="card">

    <!--profile cover end-->
    <div class="row">
    <div class="col-sm-6 col-xl-6">
    <div class="card-header">
                            <h5 class="card-header-text">Select Region</h5>

                    <form style="" class="form-horizontal" role="form" method="post">
                      <select id="schema" class="form-control form-control-primary">
                        <option value="opt1">Select One Region</option>
                                  @if (count($regions) > 0)
                                  @foreach ($regions as $schema)
                                      <option value="{{ $schema->id }}"> {{ $schema->name }}</option>
                                  @endforeach
                                  @endif
                        </select>
    
                  <?= csrf_field() ?>
              </form>
              </div>
              </div>
              <div class="col-sm-6 col-xl-6">
              <div class="card-header">

                <a href="{{ url('dashboard/addSchool') }}" style="float: right;" class="btn btn-primary"> <i class="icon-edit"></i> Add School</a>
                </div>
                </div>
                </div>
                       
                                    <div class="card-block contact-details">
                                        <?=!empty($region) ? '<h2>List of ' . count($schools).' Schools in  '.$region->name.'</h2><hr>' : '' ?>
                                        <div class="data_table_main table-responsive dt-responsive">
                                            <table id="simpletable" class="table  table-striped table-bordered nowrap">
                                                <thead>
                                                    <tr>
                                                        <th>S/N</th>
                                                        <th> School Name</th>
                                                        <th>Type</th>
                                                        <th>Reg No</th>
                                                        <th>Ownership</th>
                                                        <?=empty($region) ? '<th>Region</th>' : ''; ?>
                                                        <th>District</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $i=1; ?>
                                                    @foreach ($schools as $value)
                                                    <tr>
                                                        <td><?=$i++?></td>
                                                        <td>{{ $value->name }} </td>
                                                        <td>{{ ucfirst($value->type) }} </td>
                                                        <td>{{ $value->number }} </td>
                                                        <td>{{ $value->ownership }} </td>
                                                        <?=empty($region) ? '<td>'. $value->region .' </td>' : ''; ?>
                                                        <td>{{ $value->district }} </td>
                                                        <td>    
                                                                <a href="{{ url('dashboard/usage/'.$value->id) }}"> <i class="icofont icofont-eye-alt"></i>View</a>
                                                                <a href="#!"> <i class="icofont icofont-edit"></i>Edit</a>
                                                                <!-- <a href="#!"> <i class="icofont icofont-ui-delete"></i>Delete</a> -->
                                                          <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-cog"> </i> Add</a>
                                                              <!-- <div
                                                                class="dropdown-menu dropdown-menu-right b-none contact-menu">

                                                                <a class="dropdown-item" href="#!"> Ward - {{ $value->ward }}</a>
                                                                <a class="dropdown-item" href="#!"> <i class="icofont icofont-edit"></i>Edit</a>
                                                                <a class="dropdown-item" href="#!"> <i class="icofont icofont-ui-delete"></i>Delete</a>
                                                                <a class="dropdown-item" href="#!"> <i class="icofont icofont-eye-alt"></i>View</a>
                                                                <!-- <a class="dropdown-item" href="#!"> <i class="icofont icofont-tasks-alt"></i>Project</a>
                                                                <a class="dropdown-item" href="#!"> <i class="icofont icofont-ui-note"></i>Notes</a>
                                                                <a class="dropdown-item" href="#!"> <i class="icofont icofont-eye-alt"></i>Activity</a>
                                                                <a class="dropdown-item" href="#!"> <i class="icofont icofont-badge"></i>Schedule</a>
                                                            </div> -->
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>

                                            <div class="card-block">
                            <div class="view-info">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="general-info">
                                                <h3> Summary of Schools Per District</h3>
                                                <hr>
                                                    <div class="table-responsive">
                                                        <table class="table">
                                                            <tbody>
                                                                <?php $i = 1 ?>
                                                                @foreach ($districts as $district)
                                                                <tr>
                                                                    <th scope="row">{{ $i++ }}</th>
                                                                    <th scope="row">District</th>
                                                                    <th>{{ $district->district }}</th>
                                                                    <th>{{ $district->total }}</th>
                                                                    <?=empty($region) ? '<td>'. $value->region .' </td>' : ''; ?>

                                                                    <th><?php if(!empty($region)){ ?> <a href="{{ url('Dashboard/school/'.$region->id.'/'.$district->district) }}"> <i class="icofont icofont-ui"></i>View</a> <?php } ?></th>
                                                                </tr>
                                                                @endforeach
                                                                
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <!-- end of table col-lg-6 -->
                                            </div>
                                            <!-- end of row -->
                                        <!-- end of general info -->
                                    </div>
                                    <!-- end of col-lg-12 -->
                                </div>
                                <!-- end of row -->
                            </div>
                            <!-- end of view-info -->

                                        </div>
                                    </div>
                                    <!-- latest activity end -->
                                </div>
                                <!-- personal card end-->
                            </div>
                            <!-- tab content end -->
                        </div>
                    </div>
                </div>
                <!-- Page-body end -->
                <script type="text/javascript">
     
       $('#schema').change(function (event) {
            var schema = $(this).val();
            if (schema === '0') {
            } else {
                window.location.href = "<?= url('Dashboard/school') ?>/" + schema;
            }
        });


    </script>

                @endsection
