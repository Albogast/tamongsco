@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>

<!-- Page-body start -->
<div class="page-body">
    <div class="card">
        <!-- Email-card start -->
        <div class="card-block email-card">
            <div class="row">
                <div class="col-lg-12 col-xl-3">
                    <div class="user-head row">
                        <div class="user-face">
                            <h3 class="text-white">Send Message</h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-xl-9">
                    <div class="mail-box-head row">
                        <div class="col-md-12">
                            <a href="{{ url('users/message') }}" class="btn btn-primary" style="float: right;">
                                <b class="fa fa-comments"> Sent Message</b> </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- Right-side section start -->
                <div class="col-lg-12 col-xl-12">
                <div class="bg-white p-relative">
                          
                          <div class="card-block user-box">
                              <div class="p-b-20">
                                  <span class="f-14"><a href="#">Messages (<?=isset($messages) ? count($messages) : '0' ?>)</a></span>
                                  <!-- <span class="f-right">see all comments</span> -->
                              </div>
                             
                              <?php
                              if (isset($messages)) {
                                  foreach ($messages as $message) {
                                          ?>
                              <div class="media">
                                  <a class="media-left" href="#">
                                      <img class="media-object img-radius m-r-20"
                                          src="<?=$root?>\assets\images\avatar-2.jpg" alt="Generic placeholder image">
                                  </a>
                                  <div class="media-body b-b-theme social-client-description">
                                      <div class="chat-header"><span class="text-muted"> <?=date("M d, Y", strtotime($message->created_at)) ?></span> 
                                      <?= timeAgo($message->created_at) ?></div>
                                      <p class="text-muted"><?= $message->body ?></p>
                                  </div>
                              </div>
                              <?php } } ?>

                    </div>
                </div>
                <!-- Right-side section end -->
            </div>
        </div>
    </div>
</div>
<!-- Page-body end -->
</div>
</div>
<!-- Main-body end -->

@endsection
