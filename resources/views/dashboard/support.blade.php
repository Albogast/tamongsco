@extends('layouts.app')

@section('content')
<?php $root = url('/public/'); ?>
<!-- Page-body start -->
<div class="page-body">
    <div class="row">

        <!-- Server Side Processing table start -->
        <div class="col-sm-12">
            <!-- Basic Inputs Validation start -->
         
                <?php if(empty($set)){ ?>
                    <div class="card">
                <div class="card-header">
                    <h5>Submit Any Issue Here</h5>
                    <span>Explain what is Happening</span>

                </div>
                <div class="card-block">
                    <form id="main" method="post" action="">
                    @csrf
                    <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Select School</label>
                            <div class="col-sm-10">
                            <select name="schema_name" id="schema_name" class="form-control form-control-danger">
                                <option value="opt1">Select One School</option>
                                @if (count(load_schemas()) > 0)
                                @foreach (load_schemas() as $schema)
                                    <option value="{{ $schema->username }}"> {{ $schema->username }}</option>
                                @endforeach
                                @endif
                               
                            </select>
                                <span class="messages"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Select Module</label>
                            <div class="col-sm-10">
                            <select name="part_id" id="part_id" class="form-control form-control-info">
                                <option value="opt1">Select</option>
                                @foreach ($parts as $class)
                                    <option value="{{ $class->id }}"> {{ $class->name }}</option>
                                @endforeach
                               
                            </select>
                                <span class="messages"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Select Module Part</label>
                            <div class="col-sm-10">
                            <select name="subpart_id" id="subpart_id" class="form-control form-control-primary">
                               
                            </select>
                                <span class="messages"></span>
                            </div>
                        </div>
                     
                        <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Your Message</label>
                            <div class="col-sm-10">
                            <textarea name="body"  class="form-control form-control-info" placeholder="Write here.." cols="30" rows="10"></textarea>
                            </div>
                        </div>
                     
                        <div class="form-group row">
                            <label class="col-sm-2"></label>
                            <div class="col-sm-5">
                                <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                            </div>
                            <div class="col-sm-5">
                                <a href="{{ url('Dashboard/help/view') }}" class="btn btn-success btn-out-dotted" style="float: right;">View Issues</a>
                            </div>
                        </div>
                    </form>
               <?php }else{ ?>
                <div class="card">
                <div class="card-header">
                    <h5>List of Submitted Issues</h5>
                </div>
                <div class="card-block">
                <hr>
                <div class="table-responsive dt-responsive">
                    <table id="dom-jqry" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>School</th>   
                                <th>Type</th>   
                                <th>Task</th>                                                 
                                <th>Comment</th>
                                <th>Supported By</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $i = 1; foreach ($parts as $value) {    ?>
                            <tr>
                                <td><?= $i++ ?></td>
                                <td><?= ucfirst($value->client->username) ?></td>
                                <td><?= $value->part->name ?></td>
                                <td><?= $value->body ?></td>
                                <td><?= $value->comment ?></td>
                                <td><?= $value->to_user_id == null ?  'null' : $value->toUser->firstname .' '. $value->toUser->lastname ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
            </div>
                <a href="{{ url('Dashboard/help') }}" class="btn btn-primary"> Submit Issue </a>
            <?php } ?>
            </div>

            </div>
</div>
  <!-- Required Jquery -->
  <script type="text/javascript" src="<?=$root?>/bower_components/jquery/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?=$root?>/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<script>    $('#part_id').change(function(event) {
        var id = $(this).val();
        if (id === '0') {
            $('#subpart_id').val(0);
        } else {
            $.ajax({
                type: 'POST',
                url: "<?= url('dashboard/all_parts') ?>",
                data: "_token=" + "{{ csrf_token() }}" + "&id=" + id,
                dataType: "html",
                success: function(data) {
                    $('#subpart_id').html(data);
                }
            });
        }
    });
</script>
</script>
@endsection
   