@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>

<div class="page-body">
    <!--profile cover start-->
    <div class="row">
        <div class="col-lg-12">
            <div class="cover-profile">
                <div class="profile-bg-img">
                    <img class="profile-bg-img img-fluid" style="height: 280px;"
                        src="<?=$root?>/assets/images/shulesoft.JPG" alt="bg-img">
                    <div class="card-block user-info">
                        <div class="col-md-12">
                            <div class="media-left">
                               
                                <a href="#" class="profile-image">
                                    <img class="user-img img-radius" style="height: 150px;" src="<?php echo ''; ?>"
                                        alt="ShuleSoft" title="{{$teacher->name}}">
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--profile cover end-->
    <div class="row">
        <div class="col-lg-12">
            <!-- tab header start -->
            <div class="tab-header card">
                <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist" id="mytab">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#personal" role="tab">Basic Info</a>
                        <div class="slide"></div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#messages" role="tab">
                            Sent sms</a>
                        <div class="slide"></div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#activities" role="tab">
                            School Profile</a>
                        <div class="slide"></div>
                    </li>

                </ul>
            </div>

            <div class="tab-content">
                <div class="tab-pane active" id="personal" role="tabpanel">
                    <div class="card">
                        <div class="card-header">
                        <h3 class="card-header-text">About teacher</h3>
                        <h3 class="card-header-text right">About teacher</h3>
                        </div>
                        <div class="card-block">
                            <div class="view-info">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="general-info">
                                            <div class="row">
                                                <div class="col-lg-12 col-xl-6">
                                                    <div class="table-responsive">
                                                        <table class="table m-0">
                                                            <tbody>
                                                                <tr>
                                                                    <th scope="row">Full Name</th>
                                                                    <td><?= $teacher->name ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Added Date</th>
                                                                    <td><?= customdate($teacher->created_at); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Address</th>
                                                                    <td><?= $teacher->email ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Phone</th>
                                                                    <td><?= $teacher->phone?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Title</th>
                                                                    <td><?= $teacher->title?></td>
                                                                </tr>
                                                                
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <!-- end of table col-lg-6 -->
                                                <div class="col-lg-12 col-xl-6">
                                                    <div class="table-responsive">
                                                        <table class="table">
                                                            <tbody>
                                                                <tr>
                                                                    <th scope="row">School Name</th>
                                                                    <td><?= $teacher->school->name ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Type</th>
                                                                    <td><?= $teacher->school->type ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Ownership</th>
                                                                    <td><?= $teacher->school->ownership ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Region</th>
                                                                    <td><?= $teacher->school->region ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">District</th>
                                                                    <td><?= $teacher->school->district?></td>
                                                                </tr>
                                                               
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <!-- end of table col-lg-6 -->
                                            </div>
                                            <!-- end of row -->
                                        </div>
                                        <!-- end of general info -->
                                    </div>
                                    <!-- end of col-lg-12 -->
                                </div>
                                <!-- end of row -->
                            </div>
                        </div>
                    </div>
                </div>





                <div class="tab-pane" id="messages" role="tabpanel">
                <div>
                        <div class="bg-white p-relative">
                          
                            <div class="card-block user-box">
                                <div class="p-b-20">
                                    <span class="f-14"><a href="#">Messages (<?=isset($messages) ? count($messages) : '0' ?>)</a></span>
                                    <!-- <span class="f-right">see all comments</span> -->
                                </div>
                               
                                <?php
                                if (isset($messages)) { 
                                    foreach ($messages as $message) {
                                            ?>
                                <div class="media">
                                    <a class="media-left" href="#">
                                        <img class="media-object img-radius m-r-20"
                                            src="<?=$root?>\assets\images\avatar-2.jpg" alt="Generic placeholder image">
                                    </a>
                                    <div class="media-body b-b-theme social-client-description">
                                        <div class="chat-header"><span class="text-muted"> <?=date("M d, Y", strtotime($message->created_at)) ?></span> 
                                        <?= timeAgo($message->created_at) ?></div>
                                        <p class="text-muted"><?= $message->body ?></p>
                                    </div>
                                </div>
                                <?php } } ?>

                                <!-- <div class="media">
                                    <a class="media-left" href="#">
                                        <img class="media-object img-radius m-r-20" src="<?=$root?>\assets\images\user.png" alt="Generic placeholder image">
                                    </a>
                                    <div class="media-body">
                                        <form class="">
                                            <div class="">
                                                <textarea class="f-13 form-control msg-send" rows="3" cols="10"
                                                    required="" placeholder="Write something....."></textarea>
                                                <div class="text-right m-t-20"><a href="#"
                                                        class="btn btn-primary waves-effect waves-light">Post</a></div>
                                            </div>
                                        </form>
                                    </div>
                                </div> -->
                            </div>
                        </div>

                    </div>
                </div>



                <div class="tab-pane" id="activities" role="tabpanel">
                <div class="card">
                        <div class="card-header">
                            <h3 class="card-header-text">About teacher</h3>
                            <h3 class="card-header-text right">About teacher</h3>
                        </div>
                        <div class="card-block">
                    <div class="row">
                        <div class="col-xl-12">
                                    <div class="row">
                                    <div class="col-lg-12">
                                        <div class="general-info">
                                            <div class="row">
                                                <div class="col-lg-12 col-xl-6">
                                                    <div class="table-responsive">
                                                        <table class="table m-0">
                                                            <tbody>
                                                                <tr>
                                                                    <th scope="row">Full Name</th>
                                                                    <td>{{ $teacher->school->name }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Gender</th>
                                                                    <td>{{ $teacher->school->type }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Your Role</th>
                                                                    <td>{{ $teacher->school->ownership }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Location</th>
                                                                    <td>{{ $teacher->school->number }}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <!-- end of table col-lg-6 -->
                                                <div class="col-lg-12 col-xl-6">
                                                    <div class="table-responsive">
                                                        <table class="table">
                                                            <tbody>
                                                                <tr>
                                                                    <th scope="row">Region</th>
                                                                    <td>{{ $teacher->school->region }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">District</th>
                                                                    <td>{{ $teacher->school->district }}</td>
                                                                </tr>
                                                               
                                                                <tr>
                                                                    <th scope="row">Ward</th>
                                                                    <td>{{ $teacher->school->ward }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Website</th>
                                                                    <td><a href="#!">www.demo.com</a></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <!-- end of table col-lg-6 -->
                                            </div>
                                            <!-- end of row -->
                                        </div>
                                        <!-- end of general info -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>
@endsection
