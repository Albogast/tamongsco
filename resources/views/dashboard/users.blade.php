@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>
   
<div class="page-body">
    <!-- Server Side Processing table start -->
    <div class="card">
        <div class="card-block">

            <div class="row">
                <div class="col-sm-6 col-xl-6 m-b-30">
                    <form style="" class="form-horizontal" role="form" method="post">
                    <select id="schema" class="form-control form-control-primary">
                        <option value="opt1">Select One Region</option>
                                  @if (count($regions) > 0)
                                  @foreach ($regions as $schema)
                                      <option value="{{ $schema->id }}"> {{ $schema->name }}</option>
                                  @endforeach
                                  @endif
                        </select>
    
                  <?= csrf_field() ?>
              </form>
              </div>
              <div class="col-sm-6 col-xl-6 m-b-30">
                  <a href="{{ url('users/addMember') }}" style="float: right;" class="btn btn-primary"> <i class="icon-edit"></i> Add Member</a>
                </div>

              </div>

             <?php if( isset($male) || isset($female) || isset($all))   {   ?>
              <div class="">
                <div class="table-responsive dt-responsive">
                     <table id="" class="table table-striped table-bordered nowrap">
                       <thead>
                           <tr>
                            <th>School name</th>
                            <th>Men</th>
                            <th>Women</th>
                            <th>Total </th>
                           </tr>
                         </thead>
                         <tbody>
                             <tr>
                               <th><?= isset($school_name) ? $school_name : ''  ?></th>
                               <th><?=$male ?></th>
                               <th><?=$female ?></th>
                               <th><?=$all?></th>
                             </tr>
                         </tbody>
                   </table>
                  </div>
                </div>
             <?php  } ?>

                <div class="table-responsive dt-responsive">
                    <?php if( isset($teachers) && (!empty($teachers))) { ?>
                    <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Title</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>School</th>
                                <th>Type</th>
                                <th>Region</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; foreach ($teachers as $value) { ?>
                                <tr>
                                    <td><?= $i ?></td>
                                    
                                    <td><?= $value->name ?></td>
                                    <td><?= $value->title ?></td>
                                    <td><?= $value->email ?></td>
                                    <td><?= $value->phone ?></td>
                                    <td><?= $value->school->name ?></td>
                                    <td><?= ucfirst($value->school->type) ?></td>
                                    <td><?= $value->school->region ?></td>
                                    <td>
                                    <a type="button" class="btn btn-primary btn-sm btn-out-dotted" 
                                        href="<?= url('Users/teacherprofile/'.$value->id) ?>">
                                        View
                                    </a>
                                    <a type="button" class="btn btn-secondary btn-sm btn-out-dotted" 
                                        href="<?= url('Users/deletemember/'.$value->id) ?>">
                                        Delete
                                    </a>
                                    </td>
                                </tr>
                        <?php $i++; } ?>
                         
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Title</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>School</th>
                                <th>Type</th>
                                <th>Region</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </tfoot>
                    </table>
                    <?php } ?>
                </div>
        </div>
    </div>
    <!-- Server Side Processing table end -->
</div>


<script type="text/javascript">
     
       $('#schema').change(function (event) {
            var schema = $(this).val();
            if (schema === '0') {
            } else {
                window.location.href = "<?= url('Users/members') ?>/" + schema;
            }
        });


    </script>

@endsection