@extends('layouts.app')
@section('content')

<!-- Sidebar inner chat end-->
    <!-- Page-body start -->
    <div class="page-body">
      <div class="row">
        <div class="col-sm-12">
          <!-- Ajax data source (Arrays) table start -->
          <div class="card tab-card">
            <div class="card-block">
            <span> List of Defined Events
          <a class="btn btn-success btn-sm f-right" href="<?= url('Dashboard/addEvent') ?>"> Add New Event </a>
        </span> <hr>
              <div class="steamline">
                <div class="card-block">

                  <div class="table-responsive dt-responsive">
                  <table id="simpletable" class="table  table-striped table-bordered nowrap">
                      <thead>
                        <tr>
                          <th>Id </th>
                          <th>Title</th>
                          <th>Date</th>
                          <th>Start</th>
                          <th>End</th>
                          <th>Location</th>
                          <th>Attendance</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php 
                      if(sizeof($events) > 0){ 
                        $i = 1;
                        foreach($events as $event){
                          ?>
                      <tr>
                          <td><?=$i++?> </td>
                          <td><a href="{{ url('Dashboard/events/'.$event->id) }}"><?=substr($event->title, 0, 60)?></a></td>
                          <td><?=ucfirst($event->event_date)?></td>
                          <td><?=$event->start_time?></td>
                          <td><?=$event->end_time?></td>
                          <td><?=$event->location?></td>
                          <td><?=$event->members()->count() ?></td>

                          <td>
                          <a class="btn btn-info btn-sm" href="{{ url('Dashboard/events/'.$event->id.'/1') }}">Show</a>
                          <a class="btn btn-warning btn-sm" href="{{ url('Dashboard/DeleteMedia/'.$event->id) }}">Delete</a>
                          </td>
                        </tr>
                        <?php } } ?>
                      </tbody>

                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
