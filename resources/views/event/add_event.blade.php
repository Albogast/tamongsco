@extends('layouts.app')

@section('content')

        <!-- Page-body start -->
        <div class="page-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-block">
                        <h4>Add Workshop Details Here. Fill all Details Properly</h4>
                            @if (sizeof($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <form method="post" action="" enctype='multipart/form-data'>
                                {{ csrf_field() }}
                                <div class="card-block">
                                        <div class="form-group row">
                                            <div class="col-md-12">
                                            <strong>Event Title:</strong>
                                            <input type="text" class="form-control"  name="title" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">

                                                <div class="col-md-6">
                                                    <strong>Event Date:</strong>
                                                    <input type="date" class="form-control" placeholder="Date" name="event_date" required>
                                                </div>
                                                <div class="col-md-6">

                                                    <strong>Event Place *</strong>
                                                    <input type="text" class="form-control" placeholder="location" name="location" required>

                                                </div>
                                            </div>
                                    </div>

                                        <div class="form-group row">
                                                <div class="col-md-6">
                                                    <strong>Start Time*</strong>
                                                    <input type="time" class="form-control" placeholder="Deadline" name="start_time" required>
                                                </div>
                                                <div class="col-md-6">
                                                    <strong>End Time*</strong>
                                                    <input type="time" class="form-control" placeholder="Time" name="end_time" required>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <strong>Attach Event/Press Document:</strong>
                                                    <input type="file" class="form-control"  name="attach">
                                                </div>
                                            </div>
                                        <div class="form-group">
                                            <strong>Workshop Meeting Link:</strong>
                                            <input type="text" class="form-control"  name="meeting_link">

                                        </div>
                                        <div class="form-group">
                                            <strong>More Details About this Workshop:</strong>
                                            <textarea name="note" required rows="4" placeholder="Write More details Here .." class="form-control"> </textarea>
                                        </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <button type="submit" class="btn btn-primary">
                                                        &emsp; Submit &emsp;
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    $(".select2").select2({
        theme: "bootstrap",
        dropdownAutoWidth: false,
        allowClear: false,
        debug: true
    });
</script>

@endsection
