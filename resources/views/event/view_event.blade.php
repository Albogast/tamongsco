@extends('layouts.app')
@section('content')
<?php $root = url('/') . '/public/' ?>


    <?php
    $medias = \App\Models\EventAttendee::where('event_id', $event->id)->get();
    ?>
    <!-- Page-body start -->
    <div class="page-body">
      <div class="row">
        <div class="col-lg-12">
          <!-- tab panel personal start -->
          <div class="tab-pane active" id="personal" role="tabpanel">
            <!-- personal card start -->
            <div class="card">
              <div class="card-header">
                <h5 class="card-header-text">Title: {{ $event->title }}</h5>
                <a data-toggle="modal" data-target="#uploads" class="btn btn-primary f-right"> <i class="ti-user"></i> Upload Users </a>

                <table class="table m-0">
                  <tbody>
                    <tr>
                      <th scope="row">Event Date</th>
                      <th>Start Time</th>
                      <th> End Time</td>
                      <th> Place</td>
                      <th> Registers</td>
                        </tr>
                        <tr>
                          <td><?=ucfirst($event->event_date)?></td>
                          <td><?=$event->start_time?></td>
                          <td><?=$event->end_time?></td>
                          <td><?=$event->location?></td>
                          <td>Total - <?=sizeof($medias)?></td>
                          <tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="card-block">
                        <div class="view-info">

                          <h4 class="card-header-text">Event Registered Attendees</h4>
                          <a data-toggle="modal" data-target="#sendMessage" class="btn btn-secondary btn-sm  f-right"> <i class="ti-comments"></i> Send Message </a>

                            <div class="col-lg-12 col-xl-12">

                            <div class="table-responsive dt-responsive ">
                              <table class="table table-bordered dataTable">
                                <thead>
                                  <tr>
                                    <th>Icon </th>
                                    <th>Name</th>
                                    <th>Position</th>
                                    <th>School Name</th>
                                    <th>phone</th>
                                    <th>Email</th>
                                   <th>Joined At</th> 
                                   <th>Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php if (sizeof($medias) > 0) {
                                    $i = 1;
                                    foreach ($medias as $media) {
                                      $school = (int)($media->school_id) > 0 ? $media->school->name : $media->school_id;
                                      ?>
                                      <tr>
                                        <td><strong> <i class="ti-user"> </i> </strong></td>
                                        <td><?=$media->name?></td>
                                        <td><?=$media->position?></td>
                                        <td> <?=$school?></td>
                                        <td><?=$media->phone?></td>
                                        <td><?=$media->email?></td>
                                        <td><?=$media->created_at?></td> 
                                        <td> <a class="btn btn-danger btn-sm" href="{{ url('dashboard/deleteUser/'.$media->id) }}">Delete</a></td>
                                      </tr>
                                    <?php } } ?>
                                  </tbody>
                                </table>
                              </div>

                          </div>
                        </div>
                        <hr>
                                <div class="col-lg-12">
                                      <h5 class="card-header-text">More About this Event</h5>
                                    </div>
                                    <div class="card-block user-desc">
                                      <div class="view-desc">
                                        <?= $event->note ?>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- personal card end-->
              </div>
               
              <div class="modal hide fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" id="uploads">
                <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title text-center">
                    <b>Add New Members</b> <br> <hr>
                    Import Event  from a CSV file. In Excel, add all required column of  Event attendee, and save the file in a CSV format. Click A CSV file, then drag and drop your .csv file, or click choose file to browse files on your computer. Then click <b>Submit. <br>  <br></b>
                    <a href="<?=url('public/users.csv')?>" class="right"> <u><b>#Download Sample</b></u> </a>
                    </h4>
                </div>
                <form id="add-form" action="{{ url('users/uploadEvents') }}" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                <?= csrf_field() ?>
                <div class="form-group">
                    <label>Attach File Name <i class="fa fa-file"></i></label>
                    <div class="input-group">
                    <input type="file" class="form-control" placeholder="Enter group name..." name="call_file" required>
                    </div>
                </div>
                <!-- </div> -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                </div>
                </form>
                    </div>
                 </div>
                 </div>
                 </div>

               
              <div class="modal hide fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" id="sendMessage">
                <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title text-center">
                        <?=$event->title?>
                      </h4>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>
                    <form action="" method="POST">
                    <div class="modal-body">
                    
                <div class="col-xs-12 col-sm-12 col-md-12">
                  <div class="form-group">
                  <input type="hidden" class="form-control" value="<?=$event->id?>" name="event">
                  </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                  <div class="form-group">
                    <strong>Add Details About This Message:</strong>
                    <textarea name="message" rows="6" id="content_part" placeholder="Write More details Here .." class="form-control"> </textarea>
                  </div>
                </div>
                
                <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                        <strong>  Select Mode of this Message Below.</strong> 
                          <hr>
                   
                          &nbsp;  &nbsp; &nbsp;<input type="checkbox" name="sms" value='1'>  Send SMS  &nbsp; &nbsp;  &nbsp; &nbsp;  &nbsp; &nbsp;
                      <input type="checkbox" name="email" value="1" >  Send Email 

                    </div>
                </div>
                    </div>
                    <div class="modal-footer">
                    <button type="submit" class="btn btn-primary waves-effect waves-light "> <i class="ti-comments"> </i> Send</button>
                    </div>
                    <?= csrf_field() ?>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        
</div>
</div>
      @endsection
