@extends('layouts.app')
@section('content')

    <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="ghttps://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="ghttps://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
<div class="page-body">
<div class="row">
 <div class="col-sm-12">
  <div class="card">
    <div class="card-block">


        <div class="card-block">
            <h4 class="sub-title">Job Details</h4>
            <form method="post">
                <div class="form-group row">
                    <div class="col-sm-8 m-b-5">
                        <input type="text" class="form-control" name="name_of_the_job" placeholder="Name of the job" required>
                    </div>
                    <div class="col-sm-4">
                        <input type="date" class="form-control"  name="deadline_date" placeholder="Deadline date" required>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <input type="text" name="applicationaddress" class="form-control" placeholder="Enter application address" required>
                    </div>
                </div>

               <h4 class="sub-title">Job description</h4>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <textarea rows="5" cols="5" id="summernote" class="form-control" name="job_descriptions" placeholder="Enter job descriptions" required></textarea>
                    </div>
                </div>


                <h4 class="sub-title">Responsibilities</h4>
                <div class="form-group row">
                    <div class="col-sm-12 m-b-5">
                        <textarea rows="5" cols="5" id="summernote1" class="form-control" name="responsibilities" placeholder="Enter job responsibilities" required></textarea>
                    </div>
                </div> 

                <h4 class="sub-title">REQUIREMENTS and education level</h4>
                <div class="form-group row">
                    <div class="col-sm-12">
                      <textarea rows="5" cols="5" id="summernote2" class="form-control" name="requirements" placeholder="Enter job requirements" required></textarea>
                    </div>
                </div>

                <h4 class="sub-title"> DESIRED SKILLS AND EXPERIENCE</h4>
                <div class="form-group row">
                    <div class="col-sm-12">
                      <textarea rows="5" cols="5"  id="summernote3" class="form-control" name="skills_and_experience" required placeholder="Enter skills and experience"></textarea>
                    </div>
                </div>

                   <h4 class="sub-title"> Other details</h4>
                    <div class="form-group row">
                    <div class="col-sm-12">
                      <textarea rows="5" id="summernote4" cols="5" class="form-control" name="other_details" required placeholder="Enter other details"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <input type="submit" class="btn btn-sm btn-success" value="<?=("Submit") ?>" >
                </div>
                <?= csrf_field() ?>
            </form>
     
        </div>
    </div>
  </div>
 </div>
</div>
</div>

<script>
 $(document).ready(function() {
  $('#summernote').summernote();
  $('#summernote1').summernote();
  $('#summernote2').summernote();
  $('#summernote3').summernote();
  $('#summernote4').summernote();
});

</script>
@endsection