<?php $root = url('/public/'); ?>

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Application form</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="#">
    <meta name="keywords"
        content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <link rel="icon" href="<?=$root?>/images/shulesoft_logo.png" type="image/png">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="<?=$root?>/bower_components/bootstrap/css/bootstrap.min.css">
    <!-- feather Awesome -->
    <link rel="stylesheet" type="text/css" href="<?=$root?>/assets/icon/feather/css/feather.css">
    <link rel="stylesheet" type="text/css" href="<?=$root?>/assets/icon/icofont/css/icofont.css">

    <link rel="stylesheet" type="text/css" href="<?=$root?>/assets/icon/themify-icons/themify-icons.css">

    <!-- Sweet alert -->
    <link rel="stylesheet" type="text/css" href="<?=$root?>/bower_components/sweetalert/css/sweetalert.css">

    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="<?=$root?>/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$root?>/assets/css/jquery.mCustomScrollbar.css">

    <!-- accounting styles -->
    <link rel="stylesheet" type="text/css" href="<?=$root?>/assets/css/account_custom.css">

    <!-- select 2 -->
    <link rel="stylesheet" type="text/css" href="<?=$root?>/bower_components/select2/css/select2.min.css">

    <!-- end accounting styles -->
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>  --}}
      
    <script type="text/javascript" src="<?=$root?>/bower_components/jquery/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?=$root?>/bower_components/select2/js/select2.full.min.js"></script>
</head>
<body>




<!-- Pre-loader end -->
<div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">

        <div class="pcoded-main-container">
            <div class="pcoded-wrappere">
     
                    <div class="pcoded-inner-content">
                        <!-- Main-body start -->
                        <div class="main-body">
                            <div class="page-wrapper">
                                <!-- Page-header start -->
                                
                                
                                <div class="page-header">
                                    <div class="row align-items-end">
                                        <div class="col-lg-10">
                                            <div class="page-header-title">
                                                <div class="d-inline">
                                                    <h4>Job application form</h4>
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>
                                </div> 

                                <!-- Page-header end -->
                                    <!-- Page body start -->
                                    <div class="page-body">
                                        <div class="row">
                                            <div class="col-lg-10 col-xl-12">
                                                

                                                 <div class="card">
                                                   
                                                    <div class="card-block">
                                                        <h4 class="sub-title">Personal Details</h4>
                                                        <form method="POST" enctype="multipart/form-data" action="<?= url('recruiments/application') ?>">
                                                            <div class="form-group row">
                                                                <div class="col-sm-4 m-b-5">
                                                                    <input type="text" class="form-control" name="applicant_name" placeholder="Applicant full name" required>
                                                                </div>

                                                                 <div class="col-sm-4">
                                                                      <select class="col-sm-12 form-control" name="gender">
                                                                        <option>Applicant gender</option>
                                                                        <option value="MALE">MALE</option>
                                                                        <option value="FEMALE">FEMALE</option>
                                                                    </select>
                                                                </div>

                                                                  <div class="col-sm-4">
                                                                    <input type="text" class="form-control" name="address" placeholder="Applicant address">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <div class="col-sm-6">
                                                                    <input type="email" class="form-control" name="email" placeholder="Enter email address">
                                                                </div>

                                                                  <div class="col-sm-6">
                                                                    <input type="text" class="form-control" name="phone" placeholder="Mobile number">
                                                                </div>
                                                            </div>
                                                          
                                                            <div class="form-group row">
                                                                <div class="col-sm-12">
                                                                    <textarea rows="2" cols="5" class="form-control"  name="about_applicant" placeholder="Short description about you"></textarea>
                                                                </div>
                                                            </div>
                                                
                                                           <h4 class="sub-title">Your Education</h4>
                                                            <div class="form-group row">
                                                                <div class="col-sm-6 m-b-5">
                                                                     <select class="col-sm-12 form-control" name="education_level">
                                                                        <option>Choose education level</option>
                                                                        <option value="Certificate">Certificate</option>
                                                                        <option value="Diploma">Diploma</option>
                                                                        <option value="Degree">Degree</option>
                                                                        <option value="Masters">Masters</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <input type="text" class="form-control"  name="specialization" placeholder="Specialization">
                                                                </div>
                                                            </div>
                                                        
                                                        
                                                           <h4 class="sub-title">Your Experince</h4>
                                                            <div class="form-group row">
                                                                <div class="col-sm-12">
                                                                    <textarea rows="2" cols="5" class="form-control"  name="experience" placeholder="Write short description about your experience"></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <div class="col-sm-6 m-b-5">
                                                                    <label class="col-lable">Upload CV and Application letter in one document</label>
                                                                    <input type="file" name="cv_letter" class="form-control">
                                                                </div>
                                                            </div>

                                                           <h4 class="sub-title">Other Information</h4>
                                                            <div class="form-group row">
                                                                <div class="col-sm-12">
                                                                    <select class="form-control" name="find_us">
                                                                        <option>Where did you find us?</option>
                                                                        <option>Google Plus</option>
                                                                        <option>Linkedin</option>
                                                                        <option>Google Awards</option>
                                                                        <option>Other sources</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                          
                                                    
                                                        <h4 class="sub-title">Additional Information</h4>
                                                        <div class="form-group row">
                                                            <div class="col-sm-12">
                                                                <textarea rows="3" cols="5" class="form-control" placeholder="Any queries .. ??" name="others"></textarea>
                                                            </div>
                                                        </div>
                                                    
                                                
                                                    <div class="card-footer">
                                                        <div class="row">
                                                            <div class="col-sm-12 text-right">
                                                                <input type="hidden" name="job_id" value="<?= $job_id ?>">
                                                                <button type="submit" class="btn btn-primary m-r-10">Submit Details</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                   <?= csrf_field() ?>

                                             </form>
                                                 </div>
                                                
                                                </div>

                                            
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Page body end -->
                                </div>
                            </div>
                          
                        </div>
                    </div>
                
            </div>
        </div>
    </div>

    
    <script type="text/javascript" src="<?=$root?>/bower_components/jquery/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?=$root?>/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?=$root?>/bower_components/popper.js/js/popper.min.js"></script>
    <script type="text/javascript" src="<?=$root?>/bower_components/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?=$root?>/assets/pages/widget/excanvas.js"></script>

    <!-- select 2 -->
    <script type="text/javascript" src="<?=$root?>/bower_components/select2/js/select2.full.min.js"></script>
     <!-- Multiselect js -->
     <script type="text/javascript" src="<?=$root?>/bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js"></script>
    <script type="text/javascript" src="<?=$root?>/bower_components/multiselect/js/jquery.multi-select.js"></script>
    <script type="text/javascript" src="<?=$root?>/assets/js/jquery.quicksearch.js"></script>
    <!-- Custom js -->
    <script type="text/javascript" src="<?=$root?>/assets/pages/advance-elements/select2-custom.js"></script>

    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="<?=$root?>/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
    <script type="text/javascript" src="<?=$root?>/bower_components/sweetalert/js/sweetalert.min.js"></script>

    <!-- modernizr js -->
    <script type="text/javascript" src="<?=$root?>/bower_components/modernizr/js/modernizr.js"></script>
    <script type="text/javascript" src="<?=$root?>/assets/js/SmoothScroll.js"></script>
    <script src="<?=$root?>/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="<?=$root?>/assets/js/jquery.mousewheel.min.js"></script>
    <!-- Chart js -->
    <script type="text/javascript" src="<?=$root?>/bower_components/chart.js/js/Chart.js"></script>
    <!-- gauge js -->
    <script src="<?=$root?>/assets/pages/widget/amchart/amcharts.js"></script>
    <script src="<?=$root?>/assets/pages/widget/amchart/serial.js"></script>
    <script src="<?=$root?>/assets/pages/widget/amchart/light.js"></script>
    <!-- Custom js -->
    <!-- <script type="text/javascript" src="<?=$root?>/assets/pages/widget/custom-widget1.js"></script> -->
    <script src="<?=$root?>/assets/pages/data-table/js/data-table-custom.js"></script>

    <script type="text/javascript" src="<?=$root?>/assets/js/SmoothScroll.js"></script>
    <script src="<?=$root?>/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="<?=$root?>/assets/js/jquery.mousewheel.min.js"></script>
    <script src="<?=$root?>/assets/js/pcoded.min.js"></script>
    <script src="<?=$root?>/assets/js/vartical-layout.min.js"></script>
    <script type="text/javascript" src="<?=$root?>/assets/js/script.js"></script>
</body>

</html>
