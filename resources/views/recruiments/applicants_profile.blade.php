@extends('layouts.app')
@section('content')

<div class="page-body">
<div class="row">
 <div class="col-sm-12">
        
    
    <div class="card">
        <div class="card-header">
        <h4 class="sub-title  f-14">Applicant Personal Details</h4>
            <h4 class="text-muted f-14 m-b-10"><?= $applicant->name ?></h4>
            <h5 class="sub-title text-muted"> Email:&nbsp;<?= $applicant->email ?> &nbsp;&nbsp;&nbsp;&nbsp; Phone number:&nbsp;<?= $applicant->phone_number ?>  &nbsp;&nbsp;&nbsp;&nbsp; Gender:&nbsp;<?= $applicant->gender ?></h5>
        </div>
        <div class="card-block">
            <h4 class="sub-title">About applicant</h4>
            <p><?= $applicant->about_applicant ?></p>
        
            <h4 class="sub-title">Education :</h4>
            <h5 class="sub-title text-muted"> education level:  &nbsp;<?= $applicant->education ?> &nbsp;&nbsp;&nbsp;&nbsp; Specialization:&nbsp;<?= $applicant->specialization ?></h5>

    
            <h4 class="sub-title">Experience :</h4>
            <ul class="job-details-list">
                <li><?= $applicant->experience ?></li>
            </ul>

            <h4 class="sub-title">Applicant file</h4>
            <p><?= $applicant->applicant_file ?>
                <a href="#" class="btn btn-sm btn-info">view </a>
            </p>
        
        </div>
    </div>


</div>
</div>
</div>

@endsection