@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>
   
<div class="page-body">
    <!-- Server Side Processing table start -->
    <div class="card">
        <div class="card-block">

        <form style="" class="form-horizontal" role="form" method="post">
            <div class="row">
                <div class="col-sm-12 col-xl-4 m-b-30">
              <h5 class="sub-title">
                   <?php if(isset($schema_name))  { ?>
                        <?= school($schema_name)->name ?> 
                        <?php } else { ?>
                          School
                    <?php } ?>
                </h5>
                      <select id="schema" class="form-control form-control-primary">
                        <option value="opt1">Select One School</option>
                                  @if (count(load_schemas()) > 0)
                                  @foreach (load_schemas() as $schema)
                                      <option value="{{ $schema->username }}"> {{ school($schema->username)->name }}</option>
                                  @endforeach
                                  @endif
                        </select>
                  </div>
               
                  <?php if(isset($schema_name)) { ?>
                   <div class="col-sm-12 col-xl-4 m-b-30 float-right" id="hide-form1">
                      <h4 class="sub-title">&nbsp;</h4>
                           <h5 class="page-header">
                               <a class="btn btn-success" href="<?php echo url('SMS/addtemplate'.'/'.$schema_name) ?>">
                                <?= ('Add template')?>
                           </a>
                      </h5>
                  </div>
                  <?php } ?>
                  
                 {{-- <div class="col-sm-12 col-xl-4 m-b-30" id="hide-form">
                      <h4 class="sub-title">Academic Year</h4>
                      <select name="select" id="academic_year_id" class="form-control form-control-primary">
                         
                      </select>
                  </div> --}}
                  
                  <?= csrf_field() ?>
              </form>
            </div>

    
                <div class="table-responsive dt-responsive">
                    <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Template</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php  if(isset($mailandsmstemplates) && count($mailandsmstemplates) > 0 ) { ?>
                                <?php $i = 1; foreach ($mailandsmstemplates as $value) { ?>
                                    <tr>
                                        <td><?= $i ?></td>
                                        <td>
                                          <?php 
                                            if(strlen($value->name) > 25)
                                                echo substr($value->name, 0, 25)."...";
                                            else 
                                                echo substr($value->name, 0, 25);
                                           ?>
                                        </td>
                                        <td> <?php echo ucfirst($value->type); ?></td>
                                        <td> <?php 
                                            if(strlen($value->template) > 25)
                                                echo substr($value->template, 0, 25)."...";
                                            else 
                                                echo substr($value->template, 0, 25);
                                        ?></td>
                                        <td>
                                          <?php echo btn_view('sms/templateview/'.$schema_name . '/'. $value->id, ('view')) ?>
                                        
                                        <?php if($value->status==1){
                                        echo btn_delete('sms/templatedelete/'.$schema_name .'/' .$value->id, ('delete')); }?>
                                        </td>
                                    </tr>
                              <?php $i++; } ?>
                              <?php } ?>
                        </tbody>
                        <tfoot>
                             <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Template</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
        
    
        </div>
    </div>
    <!-- Server Side Processing table end -->
</div>


<script type="text/javascript">
        $('#schema').change(function (event) {
        var schema_name = $(this).val();
        if (schema_name === '0') {
        } else {
            window.location.href = "<?= url('SMS/template') ?>/" + schema_name.replace(/\s+/g, '');
        }
    });
</script>

@endsection