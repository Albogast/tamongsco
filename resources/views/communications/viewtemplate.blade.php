@extends('layouts.app')
@section('content')
<?php $root = url('/public/'); ?>
   
<div class="page-body">
    <!-- Server Side Processing table start -->
    <div class="card">
        <h4 class="m-3"> <?= $schema_name ?> Email/Sms template </h4>
        <div class="card-block">
          <div class="row">
            <div class="col-sm-12">
                <?php echo $mailandsmstemplate->template; ?><br><br>
            </div>
        </div>
    
        </div>
    </div>
    <!-- Server Side Processing table end -->
</div>


@endsection