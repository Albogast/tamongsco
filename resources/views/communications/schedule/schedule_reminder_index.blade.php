@extends('layouts.app')
@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
<?php $root = url('/public/'); ?>

<div class="page-body">
    <!-- Server Side Processing table start -->
    <div class="card">
        <div class="card-block">

            
        <div class="row">
            <div class="col-sm-8">
                <h4 class="sub-title">SCHEDULING REMINDERS</h4>
                
                <form role="form" method="post" >
                 
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <label> Title </label>
                             <input type="text" class="form-control" name="title" value="<?= old('title') ?>" id="title" placeholder="Reminder Title">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <label>Templates </label>
                               <select id="template_id" class="form-control form-control-primary select2">
                                 <option value="opt1">Select template</option>
                                @if(count($templates) > 0)
                                @foreach ($templates as $value)
                                    <option value="{{ $value->id }}"> {{ $value->name }}</option>
                                @endforeach
                                @endif
                             </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <label>Message </label>
                            <textarea class="form-control" style="resize:vertical" id="smsbox" name="message" required ><?= old('sms message') ?></textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6">
                            <label>Reminder To </label>
                              <select id='usertype_id' name="usertype_id" class="form-control form-control-primary select2">
                                 <option value="opt1"></option>
                                    @foreach ($usertypes as $user)
                                      <option value="{{ $user->id }}"> {{ $user->name }}</option>
                                   @endforeach
                        
                             </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6">
                            <label>Select users here </label>
                                <select name="user_id[]" id="user_id" class="select2_multiple form-control form-control-primary">
                               </select>
                        </div>
                    </div>

                    <div class="form-group row">
                           <label class="control-label col-md-3">
                            Schedule Type
                          </label>
                        <div class="col-sm-6">
                            <input type="radio" class=" radio-inline repeat" name="is_repeated" value="1" id="repeated_yes"> Repeated,
                            <input type="radio" class=" radio-inline repeat" name="is_repeated" value="0" id="repeated_no"> One time
                        </div>
                    </div>

                    <div class="form-group" id="repeated" style="">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                            Schedule Days
                        </label>
                        <div class="col-sm-6">
                            <?php
                            $days = ['Monday' => 'Monday', 'Tuesday' => 'Tuesday', 'Wednesday' => 'Wednesday', 'Thursday' => 'Thursday', 'Friday' => 'Friday', 'Saturday' => 'Saturday', 'Sunday' => 'Sunday'];
                            echo form_dropdown("days[]", $days, old("days"), "id='days' class='select2_multiple form-control' multiple='multiple'");
                            ?>
                        </div>
                    </div>
                    
                    <div class="form-group" id="last_date" style="display: none" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                            Last Schedule Date
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control calendar" name="last_date" value="<?= old('date') ?>" id="date" placeholder="Reminder Date">
                        </div>
                    </div> 

                     <div class="form-group row">
                        <div class="col-sm-6">
                             <label for="">Time</label>
                           <input type="time" class="form-control" id="schedule_time" name="time" value="" autocomplete="off" required>
                        </div>
                     </div>

                      <div class="form-group row">
                        <div class="col-sm-12">
                            <input type="hidden" class="form-control form-control-lg" value="<?= $schema_name?>" name="schema_name">
                            <input type="submit" class="btn btn-success" value="Submit" >
                        </div>
                    </div>
                     <?= csrf_field() ?>

                </form>
            </div>
          
        </div>

        </div>
    </div>
   </div>

 <script type="text/javascript">
    //  $('#schedule_time').timepicker();

    $('#usertype_id').change(function (event) {
        var usertype_id = $(this).val();
      //  alert(usertype_id);
        if (usertype_id === '0') {
        } else {
            $.ajax({
                type: 'POST',
                  headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "<?= url('sms/CallUsers') ?>",
                data: {usertype_id: usertype_id, schema_name:'<?php echo $schema_name ?>'},
                dataType: "html",
                success: function (data) {

                    $('#user_id').html(data);
                }
            });
        }
    });
   </script>

@endsection
